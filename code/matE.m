function AE = matE( E, ndim, muInv, epsilon, bloch, dx, dy, dz  ) 
% AE = matE( E, muInv, epsilon, bc, dx, dy, dz  ) 
% implements matrix-vector multiply 
% for the electric field FDFD.
% size(E) = size(muInv) = size(epsilon) = [ Nx, Ny, Nz, 3]
% bloch is a struct that contains bloch factors 

% convert from 1-D array to [Nx, Ny, Nz, 3] shape
E = reshape( E, ndim ); 

H =  -1i *  muInv .* curlE( padE( E, bloch ), dx, dy, dz ); 
AE = curlH( padH( H, bloch ), dx, dy, dz ) + 1i * epsilon .* E; 

% convert the matrix-vector product to 1-D array
AE = AE(:); 

end


