function AH = matH( H, ndim, mu, epsInv, bloch, dx, dy, dz ) 
% AH = matH( H, mu, epsInv, bloch, dx, dy, dz )
% implements matrix-vector multiplication 
% for the electric field FDFD equation
% size(H) = size(mu) = size(epsInv) = [ Nx, Ny, Nz, 3]
% bloch is a struct that contains the bloch factors 

% convert from 1-D array to [Nx, Ny, Nz, 3] shape
H = reshape( H, ndim ); 

E  = 1i * epsInv .* curlH( padH( H, bloch), dx, dy, dz );
AH =  curlE( padE( E, bloch), dx, dy, dz ) - 1i * mu .* H;

% convert the matrix-vector product to 1-D array
AH = AH(:); 

end


