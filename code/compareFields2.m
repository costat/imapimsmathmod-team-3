function compareFields2
e = load('FDFDmatrix_E.mat');
h = load('FDFDmatrix_H.mat');
eh = load('FDFDmatrix_EH.mat');

xyz = 'xyz';
x = squeeze( e.x(:,1,1) ) + e.dx/2; 
y = squeeze( e.y(1,:,1) ) + e.dy/2; 
z = squeeze( e.z(1,1,:) ) + e.dz/2;

% make the coordinate normalization distance/wavelength
x = x/(2*pi); 
y = y/(2*pi); 
z = z/(2*pi); 

% plot Epsilon_zz
figure(1)
for iv=1:3
    subplot(1,3,iv)
    myimage( x, z,real( squeeze( e.epsilon(:,round(e.Ny/2),:,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    title(sprintf('\\epsilon_{%s%s}', xyz(iv), xyz(iv) ))
end
print -djpeg100 epsilon.jpg
print -depsc2 epsilon.eps

z = z(~e.isPML); 

% plot and compare E fields
emax = norm(e.E(:),inf);
for iv=1:3
    
    figure(iv+10)
    
    subplot(1,3,1)
    myimage( x, z, real( squeeze( eh.E(:,round(e.Ny/2),~eh.isPML,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    caxis([-1,1]*emax)
    title(sprintf('FDFD-EH E_%s\n', xyz(iv)))
    
    subplot(1,3,2)
    myimage( x, z, real( squeeze( e.E(:,round(e.Ny/2),~e.isPML,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    caxis([-1,1]*emax)
    dif = e.E( :,:,~e.isPML,:) - eh.E( :,:,~eh.isPML,:);
    sum = e.E( :,:,~e.isPML,:) + eh.E( :,:,~eh.isPML,:);
    title(sprintf('FDFD-E E_%s \n rel dif = %.3f', xyz(iv),   2 * norm( dif(:) ) / norm( sum(:) )  ))
    
    subplot(1,3,3)
    myimage( x, z, real( squeeze( h.E(:,round(e.Ny/2),~h.isPML,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    caxis([-1,1]*emax)
    dif = h.E( :,:,~h.isPML,:) - eh.E( :,:,~eh.isPML,:);
    sum = h.E( :,:,~h.isPML,:) + eh.E( :,:,~eh.isPML,:);
    title(sprintf('FDFD-H E_%s \n rel dif = %.3f', xyz(iv),   2 * norm( dif(:) ) / norm( sum(:) )  ))
    
    print( sprintf('smallProb_E%s.jpg', xyz(iv) ), '-djpeg100' );
    print( sprintf('smallProb_E%s.eps', xyz(iv) ), '-depsc2' );
    
end

% plot and compare H fields
tmp = e.H( :,:,~h.isPML,:);
hmax = norm( tmp(:), inf );
for iv=1:3
    
    figure(iv+20)
    subplot(1,3,1)
    myimage( x, z, real( squeeze( eh.H(:,round(e.Ny/2),~eh.isPML,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    caxis([-1,1]*hmax)
    title(sprintf('FDFD-EH H_%s\n', xyz(iv) ))
    
    subplot(1,3,2)
    myimage( x, z, real( squeeze( e.H(:,round(e.Ny/2),~e.isPML,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    caxis([-1,1]*hmax)
    dif = e.H( :,:,~h.isPML,:) - eh.H( :,:,~eh.isPML,:);
    sum = e.H( :,:,~h.isPML,:) + eh.H( :,:,~eh.isPML,:);
    title(sprintf('FDFD-E H_%s \n rel dif = %.3f', xyz(iv),   2 * norm( dif(:) ) / norm( sum(:) )  ))
    
    subplot(1,3,3)
    myimage( x, z, real( squeeze( h.H(:,round(h.Ny/2),~h.isPML,iv) ) )' );
    polyg(e);
    xlabel('X / \lambda');
    ylabel('Z / \lambda');
    caxis([-1,1]*hmax)
    dif = h.H( :,:,~h.isPML,:) - eh.H( :,:,~eh.isPML,:);
    sum = h.H( :,:,~h.isPML,:) + eh.H( :,:,~eh.isPML,:);
    title(sprintf('FDFD-H H_%s \n rel dif = %.3f', xyz(iv),   2 * norm( dif(:) ) / norm( sum(:) )  ))
    
    print( sprintf('smallProb_H%s.jpg', xyz(iv) ), '-djpeg100' );
    print( sprintf('smallProb_H%s.eps', xyz(iv) ), '-depsc2' );
end



end

function polyg(e)
hold on; 
ax=axis; 
v=[
    0                e.zInterface
    e.Vrec(1,1)      e.zInterface
    e.Vrec(1,1)      e.zInterface-e.etchDepth
    e.Vrec(1,2)      e.zInterface-e.etchDepth
    e.Vrec(1,2)      e.zInterface
    e.Px             e.zInterface
    ] / (2*pi);
    plot( v(:,1), v(:,2), 'k' )
hold off; 
end