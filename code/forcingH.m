function Hf = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz )
% Hf = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz )
% Calculates the RHS of the H-field FDFD equation

Jf = -1i * epsDelta .* Ebg; 
Mf = +1i * muDelta  .* Hbg; 

E  = -1i * epsInv .* Jf;
Hf = Mf - curlE( padE( E, bloch), dx, dy, dz ); 

Hf = Hf(:); 

end
