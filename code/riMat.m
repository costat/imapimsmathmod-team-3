function Ax = riMat( x, muInv, epsInv, bloch, dx, dy, dz  )  

E = x(:,:,:,:,1);
H = x(:,:,:,:,2);

% E = (-i eps)^(-1) * [ curl H - Jind ]
% H = ( i mu )^(-1) * [ curl E - Mind ]

Ax(:,:,:,:,1) =  1i *  epsInv .* curlH( padH( H, bloch ), dx, dy, dz ); % -J/(-i eps ) 
Ax(:,:,:,:,2) = -1i *  muInv  .* curlE( padE( E, bloch ), dx, dy, dz ); % -M/(i mu )

end