function Ax = matEH( x, ndim, mu, epsilon, bloch, dx, dy, dz  ) 
% Ax = matEH( x, ndim, mu, epsilon, bloch, dx, dy, dz  ) 

x = reshape( x, ndim ); 
E = x(:,:,:,:,1);
H = x(:,:,:,:,2);

Ax(:,:,:,:,1) = -(1i *  mu )      .* H + curlE( padE( E, bloch ), dx, dy, dz ); 
Ax(:,:,:,:,2) =  (1i *  epsilon ) .* E + curlH( padH( H, bloch ), dx, dy, dz );

% convert the matrix-vector product to 1-D array
Ax = Ax(:); 

end