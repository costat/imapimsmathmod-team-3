function [epsilon, mu, epsInv, muInv, epsDelta, muDelta, isPML, nzPMLbot, nzPMLtop, isEtched ]....
    = materialParametersPML( epsilonPlate, muPlate, epsilonTop, muTop, ...
    Px, Py, Lv, Lg, D, Vrec, Lt, Lb, m, kappa_max, Rb, Rt, Nx, Ny, Nz, gridOffset, usePML)
%
%
%
%
% INPUTS 
%   Defining the glass plate, etchings, and vaccum layer
%
%        epsilonPlate: relative permittivity of glass
%        muPlate: relative permeability of glass
%        Px: length of plate (one period) in x-direction
%        Py: length of plate (one period) in y-direction
%        Lv: length of vaccum layer in z-direction
%        Lg: length of glass in z-direction
%        D: depth of etchings in z-direction, aligned with cell faces
%        Vrec: array of vertices defining rectangles that are aligned
%              with cell faces. Size of Vrec is 2 x 2 x Nrec, where 
%              Nrec is the number of rectangles. For the j-th 
%              rectangle, we have
%
%              Vrec(:, :, j) = [ x1, x2]    
%                              [ y1, y2]
%
%              corresponding to
%
%                 (x1, y2) _______________________ (x2, y2)
%                         |                       |
%                         |                       |
%                         |                       |
%                         |_______________________|
%                 (x1, y1)                         (x2, y1)
%
%
% 
%   Defining the PML
%
%        Lt: length of top PML
%        Lb: length of bottom PML (Q: do we need??)
%        m: polynomial degree used to define kappa and sigma
%        kappa_max
%        Rb, Rt: desired reflection errors
%       
%   Defining material cell grid and Yee sell grid 
%      
%        Nx: number of unit cells in x-direction
%        Ny: number of unit cells in y-direction
%        Nz: number of unit cells in z-direction
%        gridOffset: factor by which Yee grid is offset
%

eta=sqrt(muPlate/epsilonPlate);           % glass impedance 

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(Lb+Lg+Lv+Lt)/Nz;

Nb=Lb/dz;                      % thickness (number of cells)
Ng=Lg/dz;
Nv=Lv/dz;
Nt=Lt/dz;
Nbg=Nb+Ng;
Nbgv=Nbg+Nv;
Nd=D/dz;

% Arrays of material parameters 
Nrec=size(Vrec,3);                    % number of etched rectangles
epsilon=ones(Nx,Ny,Nz,3) * epsilonTop;
mu=ones(Nx,Ny,Nz,3) * muTop;            
epsilon(:,:,1:Nbg,:)=epsilonPlate;    % glass
mu(:,:,1:Nbg,:)=muPlate;
epsilon0=epsilon;                     % background
mu0=mu;
isEtched = false( Nx, Ny ); 
for rr=1:Nrec    
   % etch vacuum              
   etchx = ( Vrec(1,1,rr)/dx + 1 ) : ( Vrec(1,2,rr)/dx );
   etchy = ( Vrec(2,1,rr)/dy + 1 ) : ( Vrec(2,2,rr)/dy );
   isEtched( etchx, etchy ) = true; 
   epsilon(uint8(etchx),uint8(etchy),(Nbg-Nd+1):Nbg,:)=epsilonTop; 
   mu(uint8(etchx),uint8(etchy),(Nbg-Nd+1):Nbg,:)=muTop;
end

epsilon = materialAvg( epsilon ); 

% fill PML material parameters
% top
  
  if (usePML == 1)
    sigma_max=-(m+1)*log(Rt)/(2*eta*Lt);
    sigma=@(z) (z./Lt).^m.*sigma_max;
    kappa=@(z) 1+(kappa_max-1).*(z./Lt).^m;
    % Ex, Ey, and Hz are vertically aligned with cell centers
    z=(Lb+Lg+Lv+dz/2):dz:(Lb+Lg+Lv+Lt-dz/2);   % PML material cell centers
    %zyee=z+dz/4;                              % shift to Yee cell centers
    zyee=z+dz*gridOffset;
    s=kappa(zyee-z(1)+dz/2)+1i.*sigma(zyee-z(1)+dz/2)./epsilonTop;
    S=permute( repmat(s.',[1,Nx,Ny]) , [2,3,1]);
    epsilon(:,:,(Nbgv+1):Nz,1) = epsilonTop.*S;
    epsilon(:,:,(Nbgv+1):Nz,2) = epsilonTop.*S;
    mu(:,:,(Nbgv+1):Nz,3) = muTop.*S.^(-1);
    % Ez, Hx, and Hy are vertically aligned with cell faces
    zyee=zyee-dz/2;
    %s=kappa(zyee-zyee(1)+dz/4)+1i.*sigma(zyee-zyee(1)+dz/4)./epsilonTop;
    s=kappa(zyee-zyee(1)+dz*gridOffset)+1i.*sigma(zyee-zyee(1)+dz*gridOffset)./epsilonTop;
    S=permute( repmat(s.',[1,Nx,Ny]) , [2,3,1]);
    epsilon(:,:,(Nbgv+1):Nz,3) = epsilonTop.*S.^(-1);
    mu(:,:,(Nbgv+1):Nz,1) = muTop.*S;
    mu(:,:,(Nbgv+1):Nz,2) = muTop.*S;
    % bot
    sigma_max=-(m+1)*log(Rb)/(2*eta*Lb);
    sigma=@(z) (z./Lb).^m.*sigma_max;
    kappa=@(z) 1+(kappa_max-1).*(z./Lb).^m;
    % Ex, Ey, and Hz are vertically aligned with cell centers
    z=(dz/2):dz:(Lb-dz/2);                     % PML material cell centers
    %zyee=z+dz/4;                              % shift to Yee cell centers
    zyee=z+dz*gridOffset;
    %s=kappa(zyee(Nb)-zyee+dz/4)+1i.*sigma(zyee(Nb)-zyee+dz/4)./epsilonPlate;
    s=kappa(zyee(Nb)-zyee+dz*gridOffset)+1i.*sigma(zyee(Nb)-zyee+dz*gridOffset)./epsilonPlate;
    S=permute( repmat(s.',[1,Nx,Ny]) , [2,3,1]);
    epsilon(:,:,1:Nb,1) = epsilonPlate.*S;
    epsilon(:,:,1:Nb,2) = epsilonPlate.*S;
    mu(:,:,1:Nb,3) = muPlate.*S.^(-1);
    % Ez, Hx, and Hy are vertically aligned with cell faces
    zyee=zyee-dz/2;
    s=kappa(z(Nb)-zyee+dz)+1i.*sigma(z(Nb)-zyee+dz)./epsilonPlate;
    S=permute( repmat(s.',[1,Nx,Ny]) , [2,3,1]);
    epsilon(:,:,1:Nb,3) = epsilonPlate.*S.^(-1);
    mu(:,:,1:Nb,1) = muPlate.*S;
    mu(:,:,1:Nb,2) = muPlate.*S;
end   

nzPMLbot = Nb; 
nzPMLtop = Nbgv+1;
isPML = false(Nz,1);
isPML( 1 : nzPMLbot ) = true; 
isPML( nzPMLtop : Nz ) = true; 

epsDelta = epsilon - epsilon0; 
epsDelta(:,:,isPML,:) = 0; 
 
muDelta = mu - mu0; 
muDelta(:,:,isPML,:) = 0; 

muInv = 1 ./ mu; 
epsInv = 1 ./ epsilon;

end
