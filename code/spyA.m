clear all; close all; 

load FDFDmatrix_E.mat
figure(1)
spy(A); 
title('FDFD E-FIELD EQUATION')
print -djpeg100 spy_FDFD_E.jpg
print -depsc2   spy_FDFD_E.eps

clear; 
figure(2)
load FDFDmatrix_H.mat
spy(A); 
title('FDFD H-FIELD EQUATION')
print -djpeg100 spy_FDFD_H.jpg
print -depsc2   spy_FDFD_H.eps

clear; 
figure(3)
load FDFDmatrix_EH.mat
spy(A); 
N = size(A)/2; 
hold on
ax = axis; 
plot( [ ax(1:2)', ax([2,2])'/2 ], [ ax([4,4])'/2, ax(3:4)' ], 'k--')
hold off
title('FDFD [E,H]-FIELD EQUATION')
print -djpeg100 spy_FDFD_EH.jpg
print -depsc2   spy_FDFD_EH.eps

