function Ep = padE( E, bloch )

% E is padded at the bottom of x, y, z axes
n = size(E);
Ep = zeros( [ n(1:3)+1, n(4) ] );
Ep( 2:end, 2:end, 2:end, : ) = E;

% Periodic boundary condition for x
for p = 1:3
    Ep( 1, :, :, p )= Ep( end, :, :, p ) * bloch.xm;
end

% Periodic boundary condition for y
for p = 1:3
    Ep( :, 1, :, p )= Ep( :, end, :, p ) * bloch.ym;
end

end
