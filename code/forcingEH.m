function f = forcingEH( Ebg, Hbg, epsDelta, muDelta )
% Ef = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz )
% Calculates the RHS for the E-field FDFD equation. 

% induced magnetic and electric currents: 
f(:,:,:,:,1) = +1i * muDelta  .* Hbg; % Mind
f(:,:,:,:,2) = -1i * epsDelta .* Ebg; % Jind

f = f(:); 

end
