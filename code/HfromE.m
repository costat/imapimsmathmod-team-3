function H = HfromE( E, muInv, bloch, dx, dy, dz  ) 
% H = HfromE( E, muInv, bloch, dx, dy, dz  ) 
% size(E) = size(muInv) = [ Nx, Ny, Nz, 3]
% bloch is a struct that contains bloch factors 

H =  -1i *  muInv .* curlE( padE( E, bloch ), dx, dy, dz ); 

end


