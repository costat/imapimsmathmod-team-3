function f = riForcing( Ebg, Hbg, epsDelta, muDelta, epsInv, muInv )
% f = riRhs( Ebg, Hbg, epsDelta, muDelta, epsInv, muInv )
% forcing term 

% induced magnetic and electric currents:
% Jind = -1i * epsDelta .* Ebg; 
% Mind = +1i * muDelta  .* Hbg;

f(:,:,:,:,1) = - epsInv .* epsDelta .* Ebg; % -i/eps Jind
f(:,:,:,:,2) = - muInv .* muDelta  .* Hbg; %  i/mu  Mind

end
