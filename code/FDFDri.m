function FDFDri

% Set input parameters
maxit = 50; 
flag=5;

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag);

src = sourceParameters( epsilonPlate, muPlate );

zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta ]...
    = materialParameters( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset);

[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

bloch = blochFactors( src.kinc, Px, Py );

ndim = [Nx,Ny,Nz,3,2];
b = riForcing( Ebg, Hbg, epsDelta, muDelta, epsInv, muInv );
kmax = 0.8;
kmask = riProjSetup( kmax, dx, dy, dz, ndim );

x = b; 
for it = 1:maxit
    x = riMat( x, muInv, epsInv, bloch, dx, dy, dz  ) + b;
    x = riProj( x, kmask );
    if it>1
        disp( norm( x(:) - xold(:) ) )
    end
    xold = x;         
end

Esca = x(:,:,:,:,1);
Hsca = x(:,:,:,:,2);
E = Esca + Ebg;
H = Hsca + Hbg;

% store the solution
save FDFDri.mat

end

