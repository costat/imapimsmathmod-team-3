function Hp = padH( H, bloch )

% H is padded at the top of x, y, z axes
n = size(H);
Hp = zeros( [ n(1:3)+1, n(4) ] );
Hp( 1:end-1, 1:end-1, 1:end-1, : ) = H;

% Periodic boundary condition for x
for p = 1:3
    Hp( end, :, :, p )= Hp( 1, :, :, p ) * bloch.xp;
end

% Periodic boundary condition for y
for p = 1:3
    Hp( :, end, :, p )= Hp( :, 1, :, p ) * bloch.yp;
end

end
