function kmask = riProjSetup( kmax, dx, dy, dz, ndim )

dkx = 2 * pi / ( ndim(1) * dx ) ; 
dky = 2 * pi / ( ndim(2) * dy ) ;
dkz = 2 * pi / ( ndim(3) * dz ) ;

kx0 = fftindex( ndim(1), 0 ) * dkx; 
ky0 = fftindex( ndim(2), 0 ) * dky;
kz0 = fftindex( ndim(3), 0 ) * dkz;

[kx,ky,kz] = meshgrid( kx0, ky0, kz0 ); 

kx = permute( kx, [2,1,3] ); 
ky = permute( ky, [2,1,3] ); 
kz = permute( kz, [2,1,3] ); 

ksq = kx.^2 + ky.^2 + kz.^2; 

kmask = ( ksq < kmax^2 ); 
kmask = ifftshift( kmask ); 

end