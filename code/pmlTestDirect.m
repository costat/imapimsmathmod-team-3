function [E, H, Etrue, Htrue] = pmlTestDirect( equationType, usePML )
% FDFDmatrix( equationType )
% equationType = 'E' or 'H' or 'EH'
% output is saved to FDFDmatrix_E.mat or FDFDmatrix_H.mat

% Set input parameters

flag=4;

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag);



src = sourceParametersPML( epsilonPlate, muPlate );

zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta, isPML, nzPMLbot, nzPMLtop, isEtched ]...
    = materialParametersPML( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset, usePML);


[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

bloch = blochFactors( src.kinc, Px, Py );


% solve the scattered fields

switch upper( equationType )
    case 'E'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz );
        
        % form the matrix:
        N = prod(ndim);
        A = sparse(N,N);
        tic;
        for jj = 1:N
            u = zeros(N,1);
            u(jj) = 1;
            A(:,jj) = matE( u, ndim, muInv, epsilon, bloch, dx, dy, dz );
        end
        toc
        A = sparse(A);
        condA = condest(A);
        fprintf('condition number of (Ae) = %g\n', condA );
        
        Esca = A \ rhs;
        Esca = reshape( Esca, ndim );
        E = Esca + Ebg;
        H = HfromE( E, muInv, bloch, dx, dy, dz );
    case 'H'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz );
        
        % form the matrix:
        N = prod(ndim);
        A = sparse(N,N);
        for jj = 1:N
            u = zeros(N,1);
            u(jj) = 1;
            A(:,jj) = matH( u, ndim, mu, epsInv, bloch, dx, dy, dz );
        end
        A = sparse(A);
        condA = condest(A);
        fprintf('condition number of (Ae) = %g\n', condA );
        
        Hsca = A \ rhs;
        Hsca = reshape( Hsca, ndim );
        H = Hsca + Hbg;
        E = EfromH( H, epsInv, bloch, dx, dy, dz );
        
    case 'EH'
        ndim = [Nx,Ny,Nz,3,2];
        rhs = forcingEH( Ebg, Hbg, epsDelta, muDelta );
        
        % form the matrix:
        N = prod(ndim);
        A = sparse(N,N);
        for jj = 1:N
            u = zeros(N,1);
            u(jj) = 1;
            A(:,jj) = matEH( u, ndim, mu, epsilon, bloch, dx, dy, dz  );
        end
        A = sparse(A);
        condA = condest(A);
        fprintf('condition number of (A) = %g\n', condA );
        
        u = A \ rhs;
        
        u = reshape( u, ndim );
        Esca = u(:,:,:,:,1);
        Hsca = u(:,:,:,:,2);
        E = Esca + Ebg;
        H = Hsca + Hbg;
        
end

%gmres(A,rhs,[],[],100);

% True solution


[Etrue,Htrue] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface-etchDepth, gridOffset );


E0 = Etrue(:,:,~isPML,:);
err = E(:,:,~isPML,:)-E0;
x0 = ( x(:,1,1) + dx/2 )/(2*pi);
z0 = ( squeeze( z(1,1,:) ) + dz/2 )/(2*pi);
zPML = ( [ nzPMLbot; nzPMLtop-1 ] * dz )/(2*pi);

% Plot errors
figure
for iv = 2
    
    deltaE = E(:,floor(ndim(2)/2),:,iv)-Etrue(:,floor(ndim(2)/2),:,iv);
    imagesc( x0, z0, squeeze( real( deltaE ))' )
    set(gca,'Ydir','Normal');
    colorbar
    ax = axis;
    hold on
    plot( ax(1:2)' * [1,1], [1;1] * zPML', 'k' );
    hold off
    err2norm = norm( err(:), 2 ) / norm(E0(:), 2);
    errInfnorm = norm( err(:), inf );
    if usePML
        title( sprintf('WITH PML ||ERR||_2 = %.3f ||ERR||_{inf} = %.3f', err2norm, errInfnorm))
        text( mean(ax(1:2)), zPML(1)/2, 'PML')
        text( mean(ax(1:2)), 0.5 * zPML(2)+ 0.5 * dx * (Nz+1/2)/(2*pi), 'PML')
    else
        title( sprintf('WITHOUT PML ||ERR||_2 = %.3f ||ERR||_{inf} = %.3f', err2norm, errInfnorm))
    end
    xlabel('X / \lambda')
    ylabel('Z / \lambda')
    caxis([-4,4])
end
print(sprintf('pmlDirect_tot_PML%d_%s.jpg', usePML, equationType), '-djpeg100'); 
print(sprintf('pmlDirect_tot_PML%d_%s.eps', usePML, equationType), '-depsc2'); 

% Plot scattered fields 
figure
for iv = 2
    
    EscaSlice = squeeze( Esca(:,floor(ndim(2)/2),:,iv) );
    imagesc( x0, z0, real( EscaSlice )' )
    set(gca,'Ydir','Normal');
    colorbar
    ax = axis;
    hold on
    plot( ax(1:2)' * [1,1], [1;1] * zPML', 'k' );
    plot( ax(1:2)' * [1,1], [1;1] * [ zInterface, zInterface-etchDepth ]/(2*pi), 'k' );
    hold off
    if usePML
        title( 'Re(E^{sca}_y) WITH PML')
        text( mean(ax(1:2)), zPML(1)/2, 'PML')
        text( mean(ax(1:2)), 0.5 * zPML(2)+ 0.5 * dx * (Nz+1/2)/(2*pi), 'PML')
    else
        title(  'Re(E^{sca}_y) WITHOUT PML')
    end
    xlabel('X / \lambda')
    ylabel('Z / \lambda')
%    caxis([-4,4])
end


print(sprintf('pmlDirect_sca_PML%d_%s.jpg', usePML, equationType), '-djpeg100'); 
print(sprintf('pmlDirect_sca_PML%d_%s.eps', usePML, equationType), '-depsc2'); 

% store the solution
save( sprintf('pmlDirect_PML%d_%s.mat', usePML, equationType))



% PML movie

wt = (0: 1/16 : 2-1/16) * 2*pi;
figure(11)
mov = avifile(sprintf('pmlDirect_PML%d_%s.avi', usePML, equationType));
for iwt = 1:length(wt)

    imagesc( x0, z0, real( EscaSlice * exp( -1i * wt(iwt) ) )' )
    set(gca,'Ydir','Normal');
    colorbar
    ax = axis;
    hold on
    plot( ax(1:2)' * [1,1], [1;1] * zPML', 'k' );
    plot( ax(1:2)' * [1,1], [1;1] * [ zInterface, zInterface-etchDepth ]/(2*pi), 'k' );
    hold off
    if usePML
        title( 'Re(E^{sca}_y) WITH PML')
        text( mean(ax(1:2)), zPML(1)/2, 'PML')
        text( mean(ax(1:2)), 0.5 * zPML(2)+ 0.5 * dx * (Nz+1/2)/(2*pi), 'PML')
    else
        title(  'Re(E^{sca}_y) WITHOUT PML')
    end
    xlabel('X / \lambda')
    ylabel('Z / \lambda')

    drawnow
    F = getframe(gcf);
    mov = addframe(mov,F);
end
mov = close(mov);


end

