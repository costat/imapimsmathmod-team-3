function backgdtest

gridOffset = 0; 
hx = .1;
hy = .1;
hz = .1;

clear E0;

L = 100;
M = 100;
N = 100;

zInterface = N/2 * hz;

[X,Y,Z] = meshgrid(0:hx:hx*(L-1), 0:hy:hy*(M-1), 0:hz:hz*(N-1));

X = permute(X,[2,1,3]);
Y = permute(Y,[2,1,3]);
Z = permute(Z,[2,1,3]);
%Z = Z-zInterface; 

epsilon1 = 1.56;

epsilon2 = 1;

mu1 = 1;

mu2 = 1;

kinc = [0.2;0;1];

kinc = kinc / norm(kinc) * sqrt(mu1*epsilon1);

Einc = [0;1;0];

[E0,H0] = backgd(kinc, Einc, X,Y,Z, epsilon1, epsilon2, mu1, mu2, zInterface, gridOffset ) ;



e = squeeze(E0(:,M/2,:,2));
h = squeeze(H0(:,M/2,:,1));
figure(1)

subplot(2,2,1)
imagesc( X(:,1,1), squeeze(Z(1,1,:)), real(e)')
axis image; set(gca,'ydir','normal')
title('Ey  s-pol')

subplot(2,2,2)
imagesc( X(:,1,1), squeeze(Z(1,1,:)), real(h)')
axis image; set(gca,'ydir','normal')
title('Hx  s-pol')


Einc = [1;0;-0.2];

[E0,H0] = backgd(kinc, Einc, X,Y,Z, epsilon1, epsilon2, mu1, mu2, zInterface*0, gridOffset ) ;



e = squeeze(E0(:,M/2,:,1));
h = squeeze(H0(:,M/2,:,2));

subplot(2,2,3)
imagesc( X(:,1,1), squeeze(Z(1,1,:)), real(e)')
axis image; set(gca,'ydir','normal')
title('Ex  p-pol')

subplot(2,2,4)
imagesc( X(:,1,1), squeeze(Z(1,1,:)), real(h)')
axis image; set(gca,'ydir','normal')
title('Hy  p-pol')

end

