function src = sourceParameters( epsilonPlate, muPlate )
% src = sourceParameters( epsilonPlate, muPlate )
% prepares parameters of a plane wave 
% that is incident on the glass - vacuum interface
% from the glass side.
% epsilonPlate: relative electric permittivity of the plate
% muPlate: relative magnetic permeability of the plate
% src: incident plane wave struct

src.theta0 = pi/6; % angle of incidence in vacuum
src.phi0   = pi/6;  % azimuth angle  

% normalized wave vector in vacuum
% we define this to be sure that the wave 
% will not be evanescent in vacuum and that 
% it will propagate in a known direction 
% outside the glass plate. 
src.kinc0  = [ ...
    sin( src.theta0 ) * cos( src.phi0 ); 
    sin( src.theta0 ) * sin( src.phi0 ); 
    cos( src.theta0 );
    ]; 

% normalized wave vector in the glass plate
src.kinc(1:2,1) = src.kinc0(1:2,1); 
src.kinc(3,1) = sqrt( epsilonPlate * muPlate - sum( src.kinc(1:2).^2 ) ); 

src.polarizationType = 's'; 

switch lower( src.polarizationType )
    case 's'
        if src.theta0 ~= 0
            zhat = [0;0;1];
            src.Einc = cross( zhat, src.kinc );
            src.Einc = src.Einc / norm( src.Einc );
        else
            src.Einc = [0;1;0];
        end
    case 'p'
        if src.theta0 ~= 0
            zhat = [0;0;1];
            src.Einc = cross( cross( zhat, src.kinc ), src.kinc);
            src.Einc = src.Einc / norm( src.Einc );
        else
            src.Einc = [1;0;0];
        end
end
src.Hinc = cross( src.kinc, src.Einc ) / muPlate;


end