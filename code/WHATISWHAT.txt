=========  BASIC FDFD CODES  ========================================

matE.m    Calculates the vector-matrix product for the 2nd order E-field equation
matH.m    Calculates the vector-matrix product for the 2nd order H-field equation
matEH.m   calculates the vector-matrix product for the 1st order [E,H] equation

forcingE.m  calculates the RHS for the 2nd order E-field equation
forcingH.m  calculates the RHS for the 2nd order E-field equation
forcingEH.m calculates the RHS for the 1st order [E,H] equation

FDFDtest1.m   applies matE or matH to the exact homogeneous solution from backgd.  The result should be 0.
FDFDmain2.m   solves the matrix equation using gmres calling either matE, matH, or matEH 
FDFDmatrix.m  generates and stores the matrix using matE or matH or matEH. Solves x=A\rhs.  reports cond(A)

sourceParameters.m    specifies wavevector and Einc and Hinc for an incident plane wave. 
setInputs.m           user enters material and geometry parameters including the etched pattern.
materialParameters.m  sets up 3D arrays of epsilon and mu 
materialAvg.m         modifies the 3D epsilon array.  averages the reciprocal of epsilon_xx at the 
                      interfaces that are perpendicular to the x-axis. Same for y, and z.  
                      Assumes material boundaries line up with Yee grid cell boundaries. 


curlE.m   pads the E field array by one from the bottom and calculates curl(E) at the H nodes
curlH.m   pads the H field array by one from the top and calculates curl(H) at the E field nodes

padE.m  pads the E field array by one from the bottom. 
padH.m  pads the H field array by one from the top. 

blochFactors.m  calculates Bloch-peridicity factors for later use. 

HfromE.m  calculate E field from the H field array
EfromH.m  calculate H field from the E field array

backgd.m       exact solution for reflection and transmission of a plane wave at a single planer interface. 
backgdtest.m   exercises bckgd.m and visualizes the fields

=========  CODES TO VISUALIZE FDFD SOLUTION ========================================

compareFields2.m  visualize and compare the solutions of the second order E-field, H-field equations 
                  and the first order [E,H] field equ. 

arrow.m  makes a movie of time-dependent E-field vectors

=========  CODES TO TEST OR EXPLAIN PML ========================================

pmlExact  Exact solution of plane wave incident on a uniaxial PML half-space.
            Displays still images and movie

plotPML           Main that calls pmlTestDirect
pmlTestDirect     Direct FDFD solution, with and without PML, compared to exact solution.
pmlTestIterative  Iterative FDFD solution, with and without PML, compared to exact solution.
materialParametersPML   Same as materialParameters.m exect this one is only used by 
                        pmlTestDirect and pmlTestIterative.
sourceParametersPML     same as sourceParameters.m but this one is only used by 
                        pmlTestDirect and pmlTestIterative.


=========  DOMAIN DECOMPOSITION ========================================

FDFDmainDD
matEHDD
forcingEHDD
matrialParametersDD
setInputsDD
FDFDmatrixDD
bgForce_DD
dd_driver



