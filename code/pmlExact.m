function [E0,H0] = pmlExact( kinc, Einc, x, y, z,  zInterface )
% [E0,H0] = pmlExact( kinc, Einc, x, y, z,  zInterface )
% Plane wave is incident from medium 1 on to PML (medium 2)
% Medium 1 is lossless. 
% without loss of generality, we redefine:
% (x,y,z) <- (x,y,z) * k1
% H <- H * sqrt( mu1 / epsilon1 )
% Then curl E = i H; curl H = -i E in medium 1.
% epsilon2 = mu2 = diag( [ S, S, 1/S ] )
% in medium 2.  
% We do not treat the TE and TM cases separately
% because the problem is invariant under the transformation 
% E <- -H
% H <- E
% mu <- epsilon
% epsilon <- mu
% Without loss of generality, we choose the x axis to be
% in the plane of incidence

theta0 = pi/4; 

i2pi = 2i * pi; 
Lx = 4; 
Lz = 4; 
zInterface = Lz/2; 
dx = 0.05;
dz = dx; 
x0 = ( 0 : dx : Lx ); 
z0 = ( 0 : dz : Lz ); 
[x,z] = meshgrid( x0, z0 ); 

kx1 = sin( theta0 ); 
kz1 = cos( theta0 ); 

S = 1 + i; 

% Transmitted wave vector
kx2 = kx1; % phase matching 
kz2 = S * kz1; % PML

R = ( 1 - kz2 / ( S * kz1 ) ) / ( 1 + kz2 / ( S * kz1 ) ); 
T = 1+R; 

e1 = exp( i2pi * kz1 * zInterface ); 

Ey = exp( i2pi * ( kx1 * x + kz1 * z ) ) .* ( z<= zInterface ) + ...
    + e1 * R * exp( i2pi * ( kx1 * x - kz1 * (z-zInterface) ) ) .* ( z<= zInterface ) + ...
    + e1 * T * exp( i2pi * ( kx2 * x + kz2 * (z-zInterface) ) ) .* ( z > zInterface ); 

Hx =-kz1 * exp( i2pi * ( kx1 * x + kz1 * z ) ) .* ( z<= zInterface ) + ...
    +kz1 * e1 * R * exp( i2pi * ( kx1 * x - kz1 * (z-zInterface) ) ) .* ( z<= zInterface ) + ...
  -kz2/S * e1 * T * exp( i2pi * ( kx2 * x + kz2 * (z-zInterface) ) ) .* ( z > zInterface ); 

Hz = kx1 * exp( i2pi * ( kx1 * x + kz1 * z ) ) .* ( z<= zInterface ) + ...
    +kx1 * e1 * R * exp( i2pi * ( kx1 * x - kz1 * (z-zInterface) ) ) .* ( z<= zInterface ) + ...
  +kx2*S * e1 * T * exp( i2pi * ( kx2 * x + kz2 * (z-zInterface) ) ) .* ( z > zInterface ); 

figure(1)
myimage( x0, z0, real( Ey ) ) 
hold on
ax = axis; 
plot( ax(1:2), [1,1] * zInterface, 'k', 'linewidth', 2 )
hold off
xlabel( ' X / \lambda_{ 1} ' )
ylabel( ' Z / \lambda_{ 1} ' )
title( sprintf( 'REAL( Ey )   S=%g+%gi', real(S), imag(S) )) 
text( mean(ax(1:2)), mean( [zInterface, Lz] ), 'PML' )
print('pmlExact_Ey.jpg', '-djpeg100'); 
print('pmlExact_Ey.eps', '-depsc2');

figure(2)
myimage( x0, z0, real( Hx ) )
hold on
ax = axis; 
plot( ax(1:2), [1,1] * zInterface, 'k', 'linewidth', 2 )
hold off
xlabel( ' X / \lambda_{ 1} ' )
ylabel( ' Z / \lambda_{ 1} ' )
title( sprintf( 'REAL( Hx )   S=%g+%gi', real(S), imag(S) ))
text( mean(ax(1:2)), mean( [zInterface, Lz] ), 'PML' )
print('pmlExact_Hx.jpg', '-djpeg100'); 
print('pmlExact_Hx.eps', '-depsc2');

figure(3)
myimage( x0, z0, real( Hz ) )
hold on
ax = axis; 
plot( ax(1:2), [1,1] * zInterface, 'k', 'linewidth', 2 )
hold off
xlabel( ' X / \lambda_{ 1} ' )
ylabel( ' Z / \lambda_{ 1} ' )
title( sprintf( 'REAL( Hz )   S=%g+%gi', real(S), imag(S) ))
text( mean(ax(1:2)), mean( [zInterface, Lz] ), 'PML' )
print('pmlExact_Hz.jpg', '-djpeg100'); 
print('pmlExact_Hz.eps', '-depsc2');

% PML movie

wt = (0: 1/16 : 2-1/16) * 2*pi;
figure(11)
mov = avifile('pmlExact_Ey.avi');
for iwt = 1:length(wt)
    
    myimage( x0, z0, real( Ey * exp( -1i * wt(iwt) ) ) )
    hold on
    ax = axis;
    plot( ax(1:2), [1,1] * zInterface, 'k', 'linewidth', 2 )
    hold off
    xlabel( ' X / \lambda_{ 1} ' )
    ylabel( ' Z / \lambda_{ 1} ' )
    title( sprintf( 'REAL( Ey )   S=%g+%gi', real(S), imag(S) ))
    text( mean(ax(1:2)), mean( [zInterface, Lz] ), 'PML' )
    
    drawnow
    F = getframe(gcf);
    mov = addframe(mov,F);
end
mov = close(mov);

end
%--------------------------------
function myimage( x,y,u)
imagesc( x, y, u ); 
set(gca,'ydir','normal')
axis image
colorbar
end


