function [E, H] = FDFDmainDD(matrix, rhs, tol, subdomain, forcingFlag, bc, solverType)
% Solve problems on two subdomains. Domain decomposition performed
% at the plate surface.
%
% INPUTS
% matrix:
% rhs:
% tol: gmres tolerance
% subdomain: specifies plate (0) or top (1) 
% forcingFlag: no forcing (0) or forcing (1)
% bc: boundary condition 
% solverType: 'iterative' or 'direct' 
%
% NOTES - For now, forcingFlag/equationType will always be 1/E, 0/H. 
%       - Boundary conditions in driver_dd.m are for E or H only, 
%         depending on forcingFlag. However, curls will be taken in
%         driver_dd.m in order to pass
%		- For DD, equationType is 'EH'
%
%

equationType='EH';

% Set input parameters

flag=3; % domain decomposition

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputsDD(flag, subdomain);

src = sourceParameters( epsilonPlate, muPlate );


zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta ]...
    = materialParameters( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset);

%[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
%    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );


bloch = blochFactors( src.kinc, Px, Py );


% solve the scattered fields


% gmres parameters:
restart = [];
%tol = 1e-3;
maxit = 100;

if forcingFlag == 0
%	rhs = zeros(Nx, Ny, Nz, 3);
else if forcingFlag == 1
%	rhs  = forcingEH( Ebg, Hbg, epsDelta, muDelta);
end
            
% make sure the function runs
ndim = [Nx,Ny,Nz,3,2];

matEHDD( rhs, ndim, mu, epsilon, bloch, dx, dy, dz, bc, subdomain, forcingFlag ); 
        
x = gmres( @(x)matEHDD( x, ndim, mu, epsilon, bloch, dx, dy, dz, bc, subdomain, forcingFlag  ), ...
    rhs, restart, tol, maxit);
x = reshape( x, ndim );
Esca = x(:,:,:,:,1);
Hsca = x(:,:,:,:,2);
%E = Esca + Ebg;
%H = Hsca + Hbg;


% store the solution
save FDFD.mat

end

