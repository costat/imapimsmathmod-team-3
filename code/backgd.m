function [E0,H0] = backgd( kinc, Einc, x, y, z, epsilon1, epsilon2, mu1, mu2, zInterface,gridOffset )
% [E0,H0] = backgd( kinc, Einc, x, y, z, epsilon1, epsilon2, mu1, mu2, zInterface )
% Calculates the fields in the background materials
% The background is two half-spaces with an interface
% at the z = zinterface plane.  The wave is incident from the lower medium.
% where the relative material properties are epsilon1 and mu1.
% The wave is transmitted into the upper medium where the relative material
% parameters are epsilon2 and mu2.

tol = 10^(-14);

assert( abs(kinc'*Einc) < tol*norm(kinc)*norm(Einc) );
assert( abs( norm(kinc)^2 - epsilon1 * mu1 ) < tol*norm(kinc)^2 );

zhat = [0;0;1];

if norm(cross(zhat,kinc)) == 0
    EsInc = Einc;
    EpInc = [0;0;0];
else
    nhat = cross(zhat,kinc)/norm(cross(zhat,kinc));
    EsInc = (Einc'*nhat)*nhat;
    EpInc = Einc - EsInc;
end


Hinc =  cross(kinc,Einc) / mu1;
HpInc = cross(kinc,EpInc) / mu1;

% Transmitted wave vector

kTra(1:2,1) = kinc(1:2);
kTra(3,1) = sqrt( epsilon2 * mu2 - sum( kTra(1:2).^2 ) );

if imag(kTra(3))<0
    kTra(3) = -kTra(3);
end

% Transmission and reflection coefficients
Rp = ( epsilon2*kinc(3) - epsilon1*kTra(3) ) / ( epsilon2*kinc(3) + epsilon1*kTra(3) );
Rs = ( mu2*kinc(3) - mu1*kTra(3) ) / ( mu2*kinc(3) + mu1*kTra(3) );
Tp = 1 + Rp;
Ts = 1 + Rs;

% Reflected waves
kref(1:2,1) =  kinc(1:2);
kref(3,1)   = -kinc(3);
EsRef  = Rs * EsInc;
HpRef  = Rp * HpInc;

HsRef  = cross(kref,EsRef) / mu1;
EpRef  = -cross(kref,HpRef) / epsilon1;


% transmitted waves

EsTra = Ts * EsInc;
HpTra = Tp * HpInc;

HsTra  = cross(kTra,EsTra) / mu2;
EpTra  = -cross(kTra,HpTra) / epsilon2;
 
dx=abs(x(2,1,1)-x(1,1,1));
dy=abs(y(1,2,1)-y(1,1,1));
dz=abs(z(1,1,2)-z(1,1,1));
dp=[dx,dy,dz];

% ei = exp( 1i * ( kinc(1) * (x+dx*gridOffset) + kinc(2) * (y+dy*gridOffset) + kinc(3) * (z-zInterface+dz*gridOffset) ) );
% er = exp( 1i * ( kref(1) * (x+dx*gridOffset) + kref(2) * (y+dy*gridOffset) + kref(3) * (z-zInterface+dz*gridOffset) ) );
% et = exp( 1i * ( kTra(1) * (x+dx*gridOffset) + kTra(2) * (y+dy*gridOffset) + kTra(3) * (z-zInterface+dz*gridOffset) ) );

ei = exp( 1i * ( kinc(1) * (x+dx*gridOffset) + kinc(2) * (y+dy*gridOffset) + kinc(3) * (z+dz*gridOffset) ) );
er = exp( 1i * ( kref(1) * (x+dx*gridOffset) + kref(2) * (y+dy*gridOffset) + kref(3) * (z+dz*gridOffset) + (kinc(3)-kref(3)) * zInterface ) );
et = exp( 1i * ( kTra(1) * (x+dx*gridOffset) + kTra(2) * (y+dy*gridOffset) + kTra(3) * (z+dz*gridOffset) + (kinc(3)-kTra(3)) * zInterface ) );

    
E0(:,:,:,3) = ( Einc(3)  *  ei * exp(1i*kinc(1)*dp(1)/2+1i*kinc(2)*dp(2)/2) ...
    + ( EsRef(3) + EpRef(3) ) * er * exp(1i*kref(1)*dp(1)/2+1i*kref(2)*dp(2)/2) ) .* ( (z+dz*gridOffset)<=zInterface) ...
    + ( EsTra(3) + EpTra(3) ) * et * exp(1i*kTra(1)*dp(1)/2+1i*kTra(2)*dp(2)/2) .* ( (z+dz*gridOffset)>zInterface);
  
H0(:,:,:,3) = ( Hinc(3)  *  ei * exp(1i*kinc(3)*dp(3)/2) ...
    + ( HsRef(3) + HpRef(3) ) * er * exp(1i*kref(3)*dp(3)/2) ) .* ( (z+dz*gridOffset+dz/2)<=zInterface) ...
    + ( HsTra(3) + HpTra(3) ) * et * exp(1i*kTra(3)*dp(3)/2) .* ( (z+dz*gridOffset+dz/2)>zInterface);

E0(:,:,:,2) = ( Einc(2)  *  ei * exp(1i*kinc(1)*dp(1)/2+1i*kinc(3)*dp(3)/2) ...
    + ( EsRef(2) + EpRef(2) ) * er * exp(1i*kref(1)*dp(1)/2+1i*kref(3)*dp(3)/2) ) .* ( (z+dz*gridOffset+dz/2)<=zInterface) ...
    + ( EsTra(2) + EpTra(2) ) * et * exp(1i*kTra(1)*dp(1)/2+1i*kTra(3)*dp(3)/2) .* ( (z+dz*gridOffset+dz/2)>zInterface);
  
H0(:,:,:,2) = ( Hinc(2)  *  ei * exp(1i*kinc(2)*dp(2)/2) ...
    + ( HsRef(2) + HpRef(2) ) * er * exp(1i*kref(2)*dp(2)/2) ) .* ( (z+dz*gridOffset)<=zInterface) ...
    + ( HsTra(2) + HpTra(2) ) * et * exp(1i*kTra(2)*dp(2)/2) .* ( (z+dz*gridOffset)>zInterface);

E0(:,:,:,1) = ( Einc(1)  *  ei * exp(1i*kinc(2)*dp(2)/2+1i*kinc(3)*dp(3)/2) ...
    + ( EsRef(1) + EpRef(1) ) * er * exp(1i*kref(2)*dp(2)/2+1i*kref(3)*dp(3)/2) ) .* ( (z+dz*gridOffset+dz/2)<=zInterface) ...
    + ( EsTra(1) + EpTra(1) ) * et * exp(1i*kTra(2)*dp(2)/2+1i*kTra(3)*dp(3)/2) .* ( (z+dz*gridOffset+dz/2)>zInterface);
  
H0(:,:,:,1) = ( Hinc(1)  *  ei * exp(1i*kinc(1)*dp(1)/2) ...
    + ( HsRef(1) + HpRef(1) ) * er * exp(1i*kref(1)*dp(1)/2) ) .* ( (z+dz*gridOffset)<=zInterface) ...
    + ( HsTra(1) + HpTra(1) ) * et * exp(1i*kTra(1)*dp(1)/2) .* ( (z+dz*gridOffset)>zInterface);
    


end
