function epsOut = materialAvg( epsilon )
% epsOut = materialAvg( epsilon )
% Material averaging assuming material boundaries 
% fall on grid points
%
for p = 3: -1 : 1
    e  = epsilon(:,:,:,p);
    sh = zeros(1,3);
    sh(p) = 1;
    e1 = circshift( e, sh );
    bdd = ( e1 ~= e );
    if p==3
        bdd(:,:,[1,end]) = false;
    end
    e(bdd) = 1.0 ./ ( 0.5 ./ e(bdd) + 0.5 ./ e1(bdd) );
    epsOut(:,:,:,p) = e;
end

end


