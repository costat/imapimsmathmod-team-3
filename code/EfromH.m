function E = EfromH( H, epsInv, bloch, dx, dy, dz ) 
% E = EfromH( H, epsInv, bloch, dx, dy, dz ) 
% size(H) = size(E) = size(epsInv) = [ Nx, Ny, Nz, 3]
% bloch is a struct that contains the bloch factors 

E  = 1i * epsInv .* curlH( padH( H, bloch ), dx, dy, dz );

end


