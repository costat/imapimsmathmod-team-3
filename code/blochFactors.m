function bloch = blochFactors( kinc, Px, Py )
% bloch = blochFactors( kinc, Px, Py, x, y )
% kinc = [ kxinc, kyinc, kzinc ] 
% Px and Py are (scalar, real, positive) 
% sizes of the computation domain. 

bloch.xm = exp( - 1i * kinc(1) * Px );
bloch.xp = exp( + 1i * kinc(1) * Px );
bloch.ym = exp( - 1i * kinc(2) * Py );
bloch.yp = exp( + 1i * kinc(2) * Py );

end
