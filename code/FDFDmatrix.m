function FDFDmatrix( equationType )
% FDFDmatrix( equationType )
% equationType = 'E' or 'H' or 'EH'
% output is saved to FDFDmatrix_E.mat or FDFDmatrix_H.mat

% Set input parameters

flag=0;

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag);

src = sourceParameters( epsilonPlate, muPlate );

zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta,  isPML, nzPMLbot, nzPMLtop, isEtched  ]...
    = materialParameters( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset);



[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

bloch = blochFactors( src.kinc, Px, Py );


% solve the scattered fields

switch upper( equationType )
    case 'E'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz );
        
        % form the matrix:
        N = prod(ndim);
        A = sparse(N,N);
        tic;
        for jj = 1:N
            u = zeros(N,1);
            u(jj) = 1;
            A(:,jj) = matE( u, ndim, muInv, epsilon, bloch, dx, dy, dz );
        end
        toc
        A = sparse(A);
        condA = condest(A);
        fprintf('condition number of (Ae) = %g\n', condA );
        
        Esca = A \ rhs;
        Esca = reshape( Esca, ndim );
        E = Esca + Ebg;
        H = HfromE( E, muInv, bloch, dx, dy, dz );
    case 'H'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz );
        
        % form the matrix:
        N = prod(ndim);
        A = sparse(N,N);
        for jj = 1:N
            u = zeros(N,1);
            u(jj) = 1;
            A(:,jj) = matH( u, ndim, mu, epsInv, bloch, dx, dy, dz );
        end
        A = sparse(A);
        condA = condest(A);
        fprintf('condition number of (Ae) = %g\n', condA );
        
        Hsca = A \ rhs;
        Hsca = reshape( Hsca, ndim );
        H = Hsca + Hbg;
        E = EfromH( H, epsInv, bloch, dx, dy, dz );
        
    case 'EH'
        ndim = [Nx,Ny,Nz,3,2];
        rhs = forcingEH( Ebg, Hbg, epsDelta, muDelta );
        
        % form the matrix:
        N = prod(ndim);
        A = sparse(N,N);
        for jj = 1:N
            u = zeros(N,1);
            u(jj) = 1;
            A(:,jj) = matEH( u, ndim, mu, epsilon, bloch, dx, dy, dz  );
        end

        A = sparse(A);
        condA = condest(A);
        fprintf('condition number of (A) = %g\n', condA );
        
        u = A \ rhs;
        
        u = reshape( u, ndim );
        Esca = u(:,:,:,:,1);
        Hsca = u(:,:,:,:,2);
        E = Esca + Ebg;
        H = Hsca + Hbg;
        
end

gmres(A,rhs,[],[],100);

% store the solution
save( sprintf('FDFDmatrix_%s.mat',equationType))

end

