function FDFDkirc( equationType )
% FDFDkirc( equationType )
% equationType = 'E' or 'H' or 'EH'
% The scattered field is initialized to the Kirchhoff field
% E and H equations are 2nd degree
% EH equation is 1st degree but twice the size
%


% Set input parameters

flag=1;

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag);

src = sourceParameters( epsilonPlate, muPlate );


zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta, isPML, nzPMLbot, nzPMLtop, isEtched ]...
    = materialParameters( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset);



[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

[Ek,Hk] = kirchhoff( src, x, y, z, zInterface, etchDepth, gridOffset, ...
    isPML, isEtched, epsilonPlate, epsilonTop, muPlate, muTop ); 

% Kirchhoff Scattered Field
Ek = Ek - Ebg; 
Hk = Hk - Hbg; 

Ek(:,:,isPML,:) = 0; 
Hk(:,:,isPML,:) = 0;

att = ( 1.0 ./ ( 1 + exp( ( z - zInterface )/dz ) ) );
att = repmat( att, [1,1,1,3] ); 
Ek = Ek .* att; 
Hk = Hk .* att; 
clear att; 
Ek = 0.25 * circshift( Ek, [1,0,0,0] ) + 0.5 * Ek + 0.25 * circshift( Ek, [-1,0,0,0] ); 
Hk = 0.25 * circshift( Hk, [1,0,0,0] ) + 0.5 * Hk + 0.25 * circshift( Hk, [-1,0,0,0] ); 

bloch = blochFactors( src.kinc, Px, Py );

% solve the scattered fields

% gmres parameters:
restart = [];
tol = 1e-3;
maxit = 100;

switch upper( equationType )
    case 'E'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz );
        
        % first make sure that the function runs:
        matE( rhs, ndim, muInv, epsilon, bloch, dx, dy, dz );
        
        % X = GMRES(A,B,RESTART,TOL,MAXIT,M1,M2,X0)
        Esca = gmres( @(E)matE( E, ndim, muInv, epsilon, bloch, dx, dy, dz ), ...
            rhs, restart, tol, maxit, [], [], 0.2 * Ek(:) );
        
        Esca = reshape( Esca, ndim );
        E = Esca + Ebg;
        H = HfromE( E, muInv, bloch, dx, dy, dz );
    case 'H'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz );
        
        % make sure the function runs
        matH( rhs, ndim, mu, epsInv, bloch, dx, dy, dz );
        
        Hsca = gmres( @(H)matH( H, ndim, mu, epsInv, bloch, dx, dy, dz ), ...
            rhs, restart, tol, maxit, [], [], Hk(:) );
        Hsca = reshape( Hsca, ndim );
        H = Hsca + Hbg;
        E = EfromH( H, epsInv, bloch, dx, dy, dz );
        
    case 'EH'
        ndim = [Nx,Ny,Nz,3,2];
        rhs = forcingEH( Ebg, Hbg, epsDelta, muDelta ); 
        
        % make sure the function runs
        matEH( rhs, ndim, mu, epsilon, bloch, dx, dy, dz  ); 
        x0 = zeros( ndim ); 
        x0(:,:,:,:,1) = Ek; 
        x0(:,:,:,:,2) = Hk; 
        x = gmres( @(x)matEH( x, ndim, mu, epsilon, bloch, dx, dy, dz  ), ...
            rhs, restart, tol, maxit, [], [], 0.2*x0(:) );
        x = reshape( x, ndim );
        Esca = x(:,:,:,:,1);
        Hsca = x(:,:,:,:,2);
        E = Esca + Ebg;
        H = Hsca + Hbg;
        
end

% store the solution
save( sprintf('FDFDkirc_%s.mat', equationType ) )

end

