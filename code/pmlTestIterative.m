function [E, H, Etrue, Htrue] = pmlTestIterative( equationType, usePML )
% FDFDmain2( equationType )
% equationType = 'E' or 'H' or 'EH'
% E and H equations are 2nd degree
% EH equation is 1st degree but twice the size
%


% Set input parameters

flag=4;

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag);

src = sourceParametersPML( epsilonPlate, muPlate );


zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta ]...
    = materialParametersPML( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset, usePML);



[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

bloch = blochFactors( src.kinc, Px, Py );


% solve the scattered fields

% gmres parameters:
restart = [];
tol = 1e-3;
maxit = 100;

switch upper( equationType )
    case 'E'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz );
        
        % first make sure that the function runs:
        matE( rhs, ndim, muInv, epsilon, bloch, dx, dy, dz );
        
        % X = GMRES(A,B,RESTART,TOL,MAXIT,M1,M2,X0)
        Esca = gmres( @(E)matE( E, ndim, muInv, epsilon, bloch, dx, dy, dz ), ...
            rhs, restart, tol, maxit);
        
        Esca = reshape( Esca, ndim );
        E = Esca + Ebg;
        H = HfromE( E, muInv, bloch, dx, dy, dz );
    case 'H'
        ndim = [Nx,Ny,Nz,3];
        rhs  = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz );
        
        % make sure the function runs
        matH( rhs, ndim, mu, epsInv, bloch, dx, dy, dz );
        
        Hsca = gmres( @(H)matH( H, ndim, mu, epsInv, bloch, dx, dy, dz ), ...
            rhs, restart, tol, maxit);
        Hsca = reshape( Hsca, ndim );
        H = Hsca + Hbg;
        E = EfromH( H, epsInv, bloch, dx, dy, dz );
        
    case 'EH'
        ndim = [Nx,Ny,Nz,3,2];
        rhs = forcingEH( Ebg, Hbg, epsDelta, muDelta ); 
        
        % make sure the function runs
        matEH( rhs, ndim, mu, epsilon, bloch, dx, dy, dz  ); 
        
        x = gmres( @(x)matEH( x, ndim, mu, epsilon, bloch, dx, dy, dz  ), ...
            rhs, restart, tol, maxit);
        x = reshape( x, ndim );
        Esca = x(:,:,:,:,1);
        Hsca = x(:,:,:,:,2);
        E = Esca + Ebg;
        H = Hsca + Hbg;
        
end

% True solution 

zInterface=hPMLbot+Lg-etchDepth;

[Etrue,Htrue] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

% store the solution
save( sprintf('pmlIterative_%s.mat', equationType ) )

end

