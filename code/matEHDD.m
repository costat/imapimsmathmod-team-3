function Ax = matEHDD( x, ndim, mu, epsilon, bloch, dx, dy, dz, subdomain, forcingFlag   ) 
% Ax = matEH( x, ndim, mu, epsilon, bloch, dx, dy, dz  ) 

x = reshape( x, ndim ); 
E = x(:,:,:,:,1);
H = x(:,:,:,:,2);

	Ax(:,:,:,:,1) = -(1i *  mu ) .* H ... 
		+ curlE( padE( E, bloch), dx, dy, dz ); 
			
	Ax(:,:,:,:,2) =  (1i *  epsilon ) .* E ... 
		+ curlH( padH( H, bloch), dx, dy, dz ); 

% Overwrite entries that will enforce bc

if subdomain == 0

	switch forcingFlag
	
	case 0 % H bc are given

		% Do nothing
		
	case 1 % E bc are given
	 
		Ax(:, :, end, 1, 1) = -1 * E(:,:,end,1); 
		Ax(:, :, end, 2, 1) =  1 * E(:,:,end,2);
		% Ax(:, :, end, 3, 1) =  0;
	
	end
		
elseif subdomain == 1

	switch forcingFlag
	
	case 0 % H bc are given
	
		Ax(:, :, 1, 1, 2) =  1 * H(:,:,end,1);
		Ax(:, :, 1, 2, 2) = -1 * H(:,:,end,2);
		% Ax(:, :, 1, 3, 2) =  0;
	
	case 1 % E bc are given
	
		% Do nothing
			
	end	 
	
end
 
 
% convert the matrix-vector product to 1-D array
Ax = Ax(:); 

end
