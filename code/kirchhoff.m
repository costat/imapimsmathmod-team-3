function [Ek,Hk] = kirchhoff( src, x, y, z, zInterface, etchDepth, gridOffset, ...
    izPML, isEtched,  ...
    epsilonPlate, epsilonTop, muPlate, muTop )
     
% Exact solution for unetched plate
[Ek,Hk] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

% Exact solution for everywhere etched plate
[E1,H1] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface - etchDepth, gridOffset );

ndim = size(Ek); 

Ek = reshape( Ek, [ ndim(1)*ndim(2),ndim(3:4) ] ); 
Hk = reshape( Hk, [ ndim(1)*ndim(2),ndim(3:4) ] ); 

E1 = reshape( E1, [ ndim(1)*ndim(2),ndim(3:4) ] ); 
H1 = reshape( H1, [ ndim(1)*ndim(2),ndim(3:4) ] );

% Total field according to Kirchhoff
Ek( isEtched, :, : ) = E1( isEtched, :, : ); 
Hk( isEtched, :, : ) = H1( isEtched, :, : ); 

Ek = reshape( Ek, ndim ); 
Hk = reshape( Hk, ndim ); 

Ek(:,:,izPML,:) = 0; 
Hk(:,:,izPML,:) = 0; 

end 
