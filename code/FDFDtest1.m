function FDFDtest1()

% Set input parameters

flag=2;

gridOffset=0;

[ epsilonPlate, muPlate, epsilonTop, muTop,...
    Px, Py, Lv, Lg, etchDepth, Vrec, ...
    hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag);

src = sourceParameters( epsilonPlate, muPlate );

bloch = blochFactors( src.kinc, Px, Py );

zInterface=hPMLbot+Lg;

% Set x, y, z grid

dx=Px/Nx;                      % unit cell lengths
dy=Py/Ny;
dz=(hPMLbot+Lg+Lv+hPMLtop)/Nz;

[x,y,z] = meshgrid(0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1));

x = permute(x,[2,1,3]);
y = permute(y,[2,1,3]);
z = permute(z,[2,1,3]);

% Set material parameters and PML
[epsilon, mu, epsInv, muInv, epsDelta, muDelta ]...
    = materialParameters( epsilonPlate, muPlate, epsilonTop, muTop,  ...
    Px, Py, Lv, Lg, etchDepth, Vrec, hPMLtop, hPMLbot, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz, gridOffset);

ndim = [Nx,Ny,Nz,3];

% background fields
[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

% Exact Solution
[E,H] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilonTop, muPlate, muTop, zInterface-etchDepth, gridOffset );

Esca = ( E - Ebg ) ;
Hsca = ( H - Hbg ) ;

rhsE  = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz );
AE = reshape( ...
    matE( E(:), ndim, muInv, epsilon, bloch, dx, dy, dz ),...
    ndim);

figure(1)
for iv=1:3
    % matE * E should be outside the PML
    subplot(2,3,iv)
    myimage( real( squeeze( AE(:,40,:,iv) ) )' );
    xlabel('X'); ylabel('Z'); caxis([-0.1,0.1])
    title(sprintf('(Ae E)_%d',iv))
    subplot(2,3,iv+3)
    myimage( real( squeeze( AE(:,:,39,iv) ) )' );
    xlabel('X'); ylabel('Y'); caxis([-0.1,0.1])
    title(sprintf('(Ae E)_%d',iv))
end

%Eksca(:,:,izPML,:) = 0;
AEsc = matE( Esca(:), ndim, muInv, epsilon, bloch, dx, dy, dz );
AEsc = reshape( AEsc, ndim );
rhsE = reshape( rhsE, ndim );
resE = AEsc - rhsE; 

figure(2)
for iv=1:3
    % matE * Esca - rhsE should be zero outside the PML
    subplot(2,3,iv)
    myimage( real( squeeze( resE(:,40,:,iv) ) )' );
    xlabel('X'); ylabel('Z'); caxis([-0.1,0.1])
    title(sprintf('(Ae  Esca - rhsE)_%d',iv))
    subplot(2,3,iv+3)
    myimage( real( squeeze( resE(:,:,39,iv) ) )' );
    xlabel('X'); ylabel('Y'); caxis([-0.1,0.1])
    title(sprintf('(Ae  Esca - rhsE)_%d',iv))
end

% Test Magnetic Field Equation
rhsH = forcingH( Ebg, Hbg, epsDelta, muDelta, epsInv, bloch, dx, dy, dz );
AH   = matH( H, ndim, mu, epsInv, bloch, dx, dy, dz );
AHsc = matH( Hsca, ndim, mu, epsInv, bloch, dx, dy, dz );

rhsH = reshape( rhsH, ndim );
AH   = reshape( AH, ndim );
AHsc = reshape( AHsc, ndim );
resH = AHsc - rhsH; 

figure(3)
for iv=1:3
    % matH * H should be outside the PML
    subplot(2,3,iv)
    myimage( real( squeeze( AH(:,40,:,iv) ) )' );
    xlabel('X'); ylabel('Z'); caxis([-0.1,0.1])
    title(sprintf('(Ah H)_%d',iv))
    subplot(2,3,iv+3)
    myimage( real( squeeze( AH(:,:,39,iv) ) )' );
    xlabel('X'); ylabel('Y'); caxis([-0.1,0.1])
    title(sprintf('(Ah H)_%d',iv))
end



figure(4)
for iv=1:3
    % matH * Hsca - rhsH should be zero outside the PML
    subplot(2,3,iv)
    myimage( real( squeeze( resH(:,40,:,iv) ) )' );
    xlabel('X'); ylabel('Z'); caxis([-0.1,0.1])
    title(sprintf('(Ah  Hsca - rhsH)_%d',iv))
    subplot(2,3,iv+3)
    myimage( real( squeeze( resH(:,:,39,iv) ) )' );
    xlabel('X'); ylabel('Y'); caxis([-0.1,0.1])
    title(sprintf('(Ah  Hsca - rhsH)_%d',iv))
end

end

