function [E0, E1, H0, H1, d1Time, d2Time, ddresvec] = ...
                                                                 dd_driver( ...
                                                                     theta, ...
                                                                ddRoughTol, ...
                                                                     maxit, ...
                                                              ddPreciseTol, ...
                                                            linSysRoughTol, ...
                                                          linSysPreciseTol )

% Set solverType and maxit based on number of inputs. If user does not supply
% linSys tolerances and a ddPrecise tol, than solver will use a direct method.
% If the user doesn't supply a maximum # of iterations, code will stop trying
% after 50. For now we only plan to run with a direct solve, but
% I am leaving the options in here in case that changes.
if nargin < 4
  solverType = 'direct';
  ddPreciseTol = ddRoughTol;
  linSysRoughTol = 0;
  linSysPreciseTol = 0;
else
  solverType = 'iterative';
end
if nargin < 3
  maxit = 50;
end

% book-keeping parameters
ddit = 0;
ddres = 1;
linSysTol = linSysRoughTol;

% define the normal, assuming a horizontal cut, with domain 0
% below domain 1
unitNormal = [ 0, 0, 1 ];

% preprocess arrays
[mat00, Nx0, Ny0, Nz0, dz0] = FDFDmatrixDD( 'EH', 0, 0 );
[mat01, Nx0, Ny0, Nz0, dz0] = FDFDmatrixDD( 'EH', 0, 1 );
[mat10, Nx1, Ny1, Nz1, dz1] = FDFDmatrixDD( 'EH', 1, 0 );
[mat11, Nx1, Ny1, Nz1, dz1] = FDFDmatrixDD( 'EH', 1, 1 );

fprintf('Matrices constructed\n');

% define force vectors
[ forceBgD01, forceBgD11, Nz ] = bgForceDD( Nz0 );

% the code now assumes a horizontal cut, thus we expect
% Nx0 = Nx1, Ny0 = Ny1.
assert( Nx0 == Nx1 );
assert( Ny0 == Ny1 );
assert( Nz == (Nz0 + Nz1) );
Nx = Nx0;
Ny = Ny0;
n0dim = [Nx0, Ny0, Nz0, 3, 2];
n1dim = [Nx1, Ny1, Nz1, 3, 2];

% some global variables:
% chi0, chi1 are place holders for the ddresidual calculation, chi
% is the H bc given to the auxiliary problem.
% eta0 and eta1 are placeholders for the lambda update calculation.
chi0 = zeros( Nx, Ny, 3 );
chi1 = zeros( Nx, Ny, 3 );
chi = zeros( Nx, Ny, 3 );
eta0 = zeros( Nx, Ny, 3);
eta1 = zeros( Nx, Ny, 3);

% initial guess for lambda, set z component to 0 since we know that already
lambda = zeros( Nx, Ny, 3 );

%% main iterative loop
while 1

  ddit = ddit + 1

  tic

  forceBgD01out = forcingEHDD( forceBgD01, lambda, 0, 1, dz0 );
  forceBgD01out = forceBgD01out(:);
  EH0 = mat01 \ forceBgD01out;
  x01 = reshape( EH0, n0dim );
  E0 = x01( :, :, :, :, 1 );
  H0 = x01( :, :, :, :, 2 );

  d1Time(ddit) = toc;

  fprintf('Domain 0 primary solve finished at ddit %d\n', ddit);

  tic

  forceBgD11out = forcingEHDD( forceBgD11, lambda, 1, 1, dz1 );
  forceBgD11out = forceBgD11out(:);
  EH1 = mat11 \ forceBgD11out;
  x11 = reshape( EH1, n1dim );
  E1 = x11( :, :, :, :, 1 );
  H1 = x11( :, :, :, :, 2 );

  d2Time(ddit) = toc;

  fprintf('Domain 1 primary solve finished at ddit %d\n', ddit);

  % calculate chi0 = (n\times H0)|_Gamma, chi1 = (n\times H1)|_Gamma,
  % size(chii) = [Nx, Ny, 2, 3]
  for i = 1:Nx;
    for j = 1:Ny;
      H0ph = reshape( H0( i, j, Nz0, : ), [1,3] );
      H1ph = reshape( H1( i, j, 1, : ), [1,3] );
      chi0( i, j, : ) = cross( unitNormal, H0ph );
      chi1( i, j, : ) = cross( unitNormal, H1ph );
    end
  end
  chi = chi0 - chi1;

  % calculate ddres
  ddresph = reshape( chi, [1, Nx * Ny * 3] );
  ddres = max(abs( ddresph ));
  ddresvec( ddit ) = ddres;

  fprintf( 'At nnMaxwell iteration %d the residual was %E\n', ddit, ddres );

  % loop control
  if ddres < ddRoughTol
    linSysTol = linSysPreciseTol
  end
  if ddres < ddPreciseTol
    fprintf('Algorithm nnMaxwell converged to the desired tolerance\n');
    fprintf('%E in %d iterations. Final calculated residual was %E.\n', ...
      ddPreciseTol, ddit, ddres);
    break
  end
  if ddit == maxit
    fprintf('Algorithm nnMaxwell has reached the maximum allowed\n');
    fprintf('%d iterations. Exiting with a final residual of %E', maxit, ddres);
    break
  end

  forceBgD00 = zeros( Nx, Ny, Nz0, 3, 2 );
  forceBgD10 = zeros( Nx, Ny, Nz1, 3, 2 );

  forceBgD00out = forcingEHDD( forceBgD00, chi, 0, 0, dz0 );
  forceBgD00out = forceBgD00out(:);
  PP0 = mat00 \ forceBgD00out;
  x00 = reshape( PP0, n0dim );
  Psi0 = x00( :, :, :, :, 1 );
  Phi0 = x00( :, :, :, :, 2 );

  fprintf('Domain 1 aux solve finished at ddit %d\n', ddit);

  forceBgD10out = forcingEHDD( forceBgD10, chi, 1, 0, dz1 );
  forceBgD10out = forceBgD10out(:);
  PP1 = mat10 \ forceBgD10out;
  x10 = reshape( PP1, n1dim );
  Psi1 = x11( :, :, :, :, 1 );
  Phi1 = x11( :, :, :, :, 2 );

  fprintf('Domain 2 aux solve finished at ddit %d\n', ddit);

  % calculate eta0 = (n\times Psi0)|_Gamma, eta1 = (n\times Psi1)|_Gamma
  % size(etai) = [Nx, Ny, 3]
  for i = 1:Nx;
    for j = 1:Ny;
      Psi0ph = reshape( Psi0( i, j, Nz0, : ), [1,3] );
      Psi1ph = reshape( Psi1( i, j, Nz1, : ), [1,3] );
      eta0( i, j, : ) = cross( unitNormal, Psi0ph);
      eta1( i, j, : ) = cross( unitNormal, Psi1ph );
    end
  end
  % Update lambda
  lambda = lambda - theta * (eta0 - eta1);

end

end
