function Ef = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz )
% Ef = forcingE( Ebg, Hbg, epsDelta, muDelta, muInv, bloch, dx, dy, dz )
% Calculates the RHS for the E-field FDFD equation. 

Jf = -1i * epsDelta .* Ebg; 
Mf = +1i * muDelta  .* Hbg; 

H  = 1i *  muInv .* Mf; 
Ef = Jf  - curlH( padH( H, bloch ), dx, dy, dz );

Ef = Ef(:); 

end
