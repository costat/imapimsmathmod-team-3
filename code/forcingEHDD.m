
function totalrhs = forcingEHDD( forcingbg, lambda, subdomain, forcingFlag, dz)


% IF FORCINGFLAG IS ZERO, FORCINGBG SHOULD BE ZERO or else


if (subdomain == 0)
    
   if (forcingFlag == 1)
       
       % E is prescribed
       totalrhs = forcingbg(:,:,:,:,:);
       
       for p = 1:1:3;
           % Prescribing E directly
           totalrhs(:,:,end,p,1) = lambda(:,:,p);
       end
       
   elseif (forcingFlag == 0)
       % H is prescribed
       
       totalrhs = forcingbg(:,:,:,:,:);
       
        % x part of curl
        totalrhs(:,:,end,1,2) = totalrhs(:,:,end,1,2)  +  (lambda(:,:,2) / dz);
        
        % y part of curl
        totalrhs(:,:,end,2,2) = totalrhs(:,:,end,2,2)  - (lambda(:,:,1) / dz);
       
   end
    
    
    
elseif (subdomain == 1)

    if (forcingFlag == 1)
        % E is prescribed
        
        totalrhs = forcingbg(:,:,:,:,:);
       
        % x part of curl
        totalrhs(:,:,1,1,1) = totalrhs(:,:,1,1,1)  -  (lambda(:,:,2) / dz);
        
        % y part of curl
        totalrhs(:,:,1,2,1) = totalrhs(:,:,1,2,1)  + (lambda(:,:,1) / dz);

                
    elseif (forcingFlag == 0)
        % H is prescribed
        
        totalrhs = forcingbg(:,:,:,:,:);
        
        for p = 1:1:3;
            % Prescribing H directly
            totalrhs(:,:,1,p,2) = lambda(:,:,p);
        end
        
    end
    
end









end