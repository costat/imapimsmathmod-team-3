function FDFDmain()

% Set input parameters

% Set x, y, z grid
% PML
% set material parameters
ndim = [Nx,Ny,Nz,3];

% incident plane wave
src = sourceParameters( epsilonPlate, muPlate ); 

% [Ebg,Hbg] = backgroundFields(...); 
[Ebg,Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
    epsilonPlate, epsilon2, muPlate, mu2, zInterface ); 

bloch = blochFactors( src.kinc, Px, Py, x, y ); 

% solve the scattered fields
switch upper( equationType )
    case 'E'
        rhs  = forcingE( Ebg, Hbg, epsilon, epsilon0, mu, mu0, bloch );
        Esca = gmres( @(E)matE( E, ndim, muInv, epsilon, bloch ), rhs ); 
        Esca = reshape( Esca, ndim ); 
        E = Esca + Ebg; 
        H = HfromE( E, muInv, bloch );  
    case 'H'
        rhs  = forcingH( Ebg, Hbg, epsilon, epsilon0, mu, mu0, bloch );
        Hsca = gmres( @(H)matH( H, ndim, mu, epsInv, bloch ), rhs); 
        Hsca = reshape( Hsca, ndim ); 
        H = Hsca + Hbg;
        E = EfromH( H, epsInv, bloch ); 
end

% store the solution 
save FDFD.mat

end

