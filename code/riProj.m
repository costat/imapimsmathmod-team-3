function x = riProj( x, kmask )

for vc = 1:6    
    x(:,:,:,vc) = ifftn( fftn( x(:,:,:,vc) ) .* kmask ); 
end

end