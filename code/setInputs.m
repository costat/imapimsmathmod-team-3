function [epsilonPlate, muPlate, epsilonTop, muTop, ...
    Px, Py, Lv, Lg, D, Vrec, ...
    Lt, Lb, m, kappa_max, Rb, Rt, ...
    Nx, Ny, Nz] ...
    = setInputs(flag)
%
%
%   Defining the glass plate, etchings, and vaccum layer
%
%        epsilonPlate: relative permittivity of glass
%        muPlate: relative permeability of glass
%        Px: length of plate (one period) in x-direction
%        Py: length of plate (one period) in y-direction
%        Lv: length of vaccum layer in z-direction
%        Lg: length of glass in z-direction
%        D: depth of etchings in z-direction, aligned with cell faces
%        Vrec: array of vertices defining rectangles that are aligned
%              with cell faces. Size of Vrec is 2 x 2 x Nrec, where
%              Nrec is the number of rectangles. For the j-th
%              rectangle, we have
%
%              Vrec(:, :, j) = [ x1, x2]
%                              [ y1, y2]
%
%              corresponding to
%
%                 (x1, y2) _______________________ (x2, y2)
%                         |                       |
%                         |                       |
%                         |                       |
%                         |_______________________|
%                 (x1, y1)                         (x2, y1)
%
%
%
%   Defining the PML
%
%        Lt: length of top PML
%        Lb: length of bottom PML (Q: do we need??)
%        m: polynomial degree used to define kappa and sigma
%        kappa_max
%        Rb, Rt: desired reflection errors
%
%   Defining material cell grid. (Note: Yee grid shifted by 1/4)
%
%        Nx: number of unit cells in x-direction
%        Ny: number of unit cells in y-direction
%        Nz: number of unit cells in z-direction


switch flag %

    case 0

        epsilonPlate = 1.56^2;
        muPlate = 1.00;

        epsilonTop = 1;
        muTop = 1;

        Px = 1 * 2 * pi;
        Py = 1 * 2 * pi;
        Lv = 0.5 * 2*pi;
        Lg = 1 * 2*pi;
        D  = 0.5 * 2*pi;
        Vrec=zeros(2,2,1);
        Vrec(:,:,1)=[
            0.3, 0.8;
            0.3, 0.8] * 2*pi;
        Lt=1.0 * 2*pi;
        Lb=1.0 * 2*pi;
        m=3;
        kappa_max=1;
        Rb=exp(-16);    % optimal for 10 PML cells in z-direction
        Rt=exp(-16);
        Nx=10;
        Ny=10;
        Nz=35;

    case 1

        epsilonPlate = 1.56^2;
        muPlate = 1.00;

        epsilonTop = 1;
        muTop = 1;

        Px = 6 * 2 * pi;
        Py = 5 * 2 * pi;
        Lv = 0.5 * 2*pi;
        Lg = 1.5 * 2*pi;
        D  = 1.0 * 2*pi;
        Vrec=zeros(2,2,2);
        Vrec(:,:,1)=[
            1.25, 2.50;
            1.25, 2.75] * 2*pi;
        Vrec(:,:,2)=[
            2.50, 3.00;
            2.50, 3.75] * 2*pi;
        Lt=1.0 * 2*pi;
        Lb=1.0 * 2*pi;
        m=3;
        kappa_max=1;
        Rb=exp(-16);                 % optimal for 10 PML cells in z-direction
        Rt=exp(-16);
        Nx=96;
        Ny=80;
        Nz=64;

    case 2

        % the scatterer is a full layer
        epsilonPlate = 1.56^2;
        epsilonTop = 1;

        muPlate = 1.00;
        muTop = 1;

        Px = 6 * 2 * pi;
        Py = 5 * 2 * pi;
        Lv = 0.5 * 2*pi;
        Lg = 1.5 * 2*pi;
        D  = 1.0 * 2*pi;
        Vrec=zeros(2,2,1);
        Vrec(:,:,1)=[
            0, Px;
            0, Py] ;
        Lt=1.0 * 2*pi;
        Lb=1.0 * 2*pi;
        m=3;
        kappa_max=1;
        Rb=exp(-16);                 % optimal for 10 PML cells in z-direction
        Rt=exp(-16);
        Nx=96;
        Ny=80;
        Nz=64;
        
        
    case 4   % PML test

        % the scatterer is a full layer
        epsilonPlate = 1.56^2;
        epsilonTop = 1;

        muPlate = 1.00;
        muTop = 1;

        Px = 4 * 2 * pi;
        Py = 0.2 * 2 * pi;
        Lv = 0.5 * 2*pi;
        Lg = 1.0 * 2*pi;
        D  = 0.1 * 2*pi;
        Vrec=zeros(2,2,1);
        Vrec(:,:,1)=[
            0, Px;
            0, Py] ;
        Lt=1.0 * 2*pi;
        Lb=1.0 * 2*pi;
        m=3;
        kappa_max=1;
        Rb=exp(-16);                 % optimal for 10 PML cells in z-direction
        Rt=exp(-16);
        Nx=40;
        Ny=2;
        Nz=35;
        
    case 5

        epsilonPlate = 1.56^2;
        muPlate = 1.00;
        
        epsilonTop = 1;
        muTop = 1;
        
        Px = 6 * 2 * pi;
        Py = 5 * 2 * pi;
        Lv = 0.5 * 2*pi;
        Lg = 1.5 * 2*pi;
        D  = 1.0 * 2*pi;
        
        Vrec(:,:,1)=[
            1.00, 1.75;
            1.25, 2.75] * 2*pi;
        Vrec(:,:,2)=[
            1.75, 2.25;
            1.25, 1.75] * 2*pi;
        Vrec(:,:,3)=[
            1.75, 2.25;
            2.25, 2.75] * 2*pi;
        Vrec(:,:,4)=[
            2.25, 3.00;
            1.25, 2.75] * 2*pi;
        Vrec(:,:,5)=[
            3.00, 3.50;
            1.25, 1.75] * 2*pi;
        Vrec(:,:,6)=[
            3.00, 3.50;
            2.25, 2.75] * 2*pi;
        Vrec(:,:,7)=[
            3.50, 4.25;
            1.25, 2.75] * 2*pi;
        Vrec(:,:,8)=[
            4.75, 5.25;
            1.25, 3.75] * 2*pi;
        
        Vrec(:,:,9)=[
            1.00, 4.25;
            3.25, 3.75] * 2*pi;
        
        Lt=1.0 * 2*pi;
        Lb=1.0 * 2*pi;
        m=3;
        kappa_max=1;
        Rb=exp(-16);                 % optimal for 10 PML cells in z-direction
        Rt=exp(-16);
        Nx=96;
        Ny=80;
        Nz=64;
        
end
assert( D < Lg )
end
