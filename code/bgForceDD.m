function [forceBgD0, forceBgD1, Nz] = bgForceDD( Nz0 )

flag = 3;
domain = 2;

gridOffset = 0;

[ epsilonPlate, muPlate, epsilonTop, muTop, ...
  Px, Py, Lv, Lg, D, Vrec, ...
  Lt, Lb, m, kappa_max, Rb, Rt, ...
  Nx, Ny, Nz] ...
  = setInputsDD( flag, domain );

[epsilon, mu, epsInv, muInv, epsDelta, muDelta, isPML, nzPMLbot, ...
  nzPMLtop, isEtched] = ...
    materialParametersDD( epsilonPlate, muPlate, epsilonTop, muTop, ...
    Px, Py, Lv, Lg, D, Vrec, Lt, Lb, m, kappa_max, Rb, Rt, Nx, Ny, Nz, ...
    gridOffset, domain);

src = sourceParameters( epsilonPlate, muPlate );

zInterface=Lb+Lg;

dx = Px/Nx;
dy = Py/Ny;
dz = (Lb+Lg+Lv+Lt)/Nz;

[x, y, z] = meshgrid( 0:dx:dx*(Nx-1), 0:dy:dy*(Ny-1), 0:dz:dz*(Nz-1) );

x = permute(x, [2,1,3]);
y = permute(y, [2,1,3]);
z = permute(z, [2,1,3]);

[Ebg, Hbg] = backgd( src.kinc, src.Einc, x, y, z, ...
  epsilonPlate, epsilonTop, muPlate, muTop, zInterface, gridOffset );

[forceBgTotal] = forcingEH( Ebg, Hbg, epsDelta, muDelta );
forceBgTotal = reshape( forceBgTotal, [Nx, Ny, Nz, 3, 2] );

forceBgD0 = forceBgTotal( :, :, 1:Nz0, : );
forceBgD1 = forceBgTotal( :, :, Nz0+1:Nz, : );
