\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Model}{3}
\contentsline {subsection}{\numberline {2.1}Time-Harmonic Maxwell's Equations and Boundary Conditions}{3}
\contentsline {subsection}{\numberline {2.2}Perfectly Non-reflecting Boundary Condition}{4}
\contentsline {subsubsection}{\numberline {2.2.1}Perfectly Matched Layer Boundary Conditions}{4}
\contentsline {section}{\numberline {3}Kirchhoff's Approximation}{4}
\contentsline {section}{\numberline {4}Domain Decomposition}{4}
\contentsline {subsection}{\numberline {4.1}Two-domain time harmonic Maxwell's equations}{6}
\contentsline {subsection}{\numberline {4.2}Independent Subdomain Problems \& Steklov-Poincar\'{e} Interface Equation}{6}
\contentsline {subsection}{\numberline {4.3}Algorithm nnMaxwell}{7}
\contentsline {subsection}{\numberline {4.4}Convergence Analysis}{8}
\contentsline {section}{\numberline {5}Multigrid}{8}
\contentsline {section}{\numberline {6}Conclusion}{9}
