\documentclass[8pt]{beamer}

% Setup appearance:
\usetheme{Frankfurt}
\usecolortheme{default}
\setbeamertemplate{navigation symbols}{}
%\setbeamertemplate{footline}[frame number]
% Standard packages
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{verbatim}
\usepackage{fancybox}
\usepackage{multimedia}
\usepackage{graphicx}
\usepackage{tcdef}
\usepackage{enumerate}
\usepackage{caption}
\usepackage{xcolor}
\usepackage{natbib}
\usepackage{epstopdf}
\usepackage{animate}
%\newcommand{\newblock}{}

% Author, Title, etc.
\title[]
{Team 3 Progress Report: Fast Calculation of Diffraction by Photomasks}
\author[]{Timothy Costa, John Cummings, Michael Jenkinson, \\ Yeon Eung Kim,
  Jose de Jesus Martinez, Nicole Olivares, Apo Sezginer}
\date[August, 2014]
  {Mathematical Modeling in Industry XVIII, IMA / PIMS,
    August, 2014
  }

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}
\subsection{Motivation}

\begin{frame}{Motivation: Optical Lithography}
  \vspace{-.05cm}
  \hspace{-.35cm}\includegraphics[scale=.6]{images/optlith.pdf}
  \pause
  \begin{block}{Photomask pattern $\not=$ scaled copy of device pattern}
    \begin{itemize}
      \small
      \item Precise characteristics of mask don't appear in
        device (non-uniqueness of mask for IC).
      \item Optimization: best mask to handle various
            problems, e.g. imperfect focus.
      \item Requires fast forward solver.
      \item Takes on the order of 100,000 CPU hours to design a mask for a single
            integrated circuit.
    \end{itemize}
  \end{block}
\end{frame}


\begin{frame}{The Goal}
  \vspace{-.7cm}\hspace{-.55cm}\includegraphics[scale=.5]{images/optlith2.pdf}
\pause

  \begin{block}{Expensive!}
    \begin{itemize}
      \item A typical problem size for the scattering problem is $5e5 \times 5e5$
        wavelengths in the $x$-$y$ plane (smaller in the $z$ direction).
       10 grid cells per wavelength: huge!
      \item Requires new efficient methods.
    \end{itemize}
  \end{block}
\end{frame}

\section{Model}
\subsection{Time-Harmonic Maxwell's Equations}

\begin{frame}{Mathematical Model}
  \begin{block}{Model: 3d Time-harmonic Maxwell's Equations}
    \vspace{-.3cm}
    \begin{align*}
      i \mu_r H = \nabla \times E + f^h, \quad \quad
      -i \epsilon_r E = \nabla \times H + f^e.
    \end{align*}
  \end{block}
  \begin{columns}
    \column{.5\textwidth}
    \begin{block}{Primary Variables}
      \vspace{-.3cm}
      \begin{align*}
        &E: \quad \text{Electric Field} \\
        &H: \quad \text{Magnetic Field}
      \end{align*}
    \end{block}
    \column{.5\textwidth}
    \begin{block}{Data}
      \vspace{-.3cm}
      \begin{align*}
        &\mu_r: \quad \text{Relative magnetic permittivity} \\
        &\epsilon_r: \quad \text{Relative electric permittivity}, \\
        &f^{e/h}: \quad \text{Describes incident field}.
      \end{align*}
    \end{block}
  \end{columns}
  \begin{block}{Boundary Conditions}
    \begin{itemize}
      \item Periodic in $x$-$y$ directions.
      \item Require perfectly non-reflecting condition in $z$ direction.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Perfectly Non-reflecting Boundary Conditions}
\begin{columns}
\column{.4\textwidth}
\begin{block}{Perfectly Matched Layer}
  \begin{itemize}
    \item Pad the computational domain.
    \item Design synthetic matherial properties for PML layer for perfect absorbtion.
    \item Severely damp waves in the PML layer, 0 by the time they hit new boundary.
  \end{itemize}
\end{block}
\column{.6\textwidth}
\centering
\includegraphics[scale=.35]{images/pml2.pdf}
\end{columns}
\end{frame}

\section{Kirchhoff's Approximation}
\subsection{Kirchoff's Approximation}
\begin{frame}{Kirchhoff's Approximation}
\begin{columns}
\column{.6\textwidth}
  \includegraphics[width=1\textwidth]{images/kirchh.pdf}
\column{.4\textwidth}
\begin{itemize}
  \item Kirchhoff's approximation can be computed in $O(N)$ operations.
  \item This is not an accurate approximation, causes discontinuities
  near interface between aperture and screen.
  \item Various methods exist to improve the accuracy.
  \item We will use the Kirchhoff approximation as an initial guess
  for iterative methods which will improve it through the solution
  of the time-harmonic Maxwell system.
\end{itemize}
\end{columns}
\end{frame}

\section{Domain Decomposition}
\subsection{Iterative Substructuring Method}

\begin{frame}{Domain Decomposition for Partial Differential Equations Overview}
\blu{\it{"We used to think that if we knew one, we knew two. We are finding
that we must learn a great deal more about the 'and.'"}} \\
\hfill{\blu{-Sir Arthur Eddington}}\\
\hspace{-2cm}\includegraphics[scale=.27]{images/domdecimage.pdf}
\pause
\vspace{-1.5cm}
\begin{itemize}
  \item Two major purposes:
  \begin{itemize}
    \normalsize{
      \item Parallelization and Out-of-Core Problems
      \item Multiscale \& Multiphysics Problems, Interface Problems
    }
  \end{itemize}
\end{itemize}
\begin{itemize}
  \item Iterative Substructuring Methods:
    \begin{enumerate}
      \normalsize{
        \item Write independent subdomain problems satisfying 1 transmission
              condition.
        \item Write \alert{Steklov-Poincar\'{e}} Interface Equation, solution
              guaranteeing 2nd transmission condition.
        \item Solve \alert{SP} interface equation iteratively from initial guess,\\
              e.g. \alert{Kirchhoff's approximation!}
      }
    \end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{Two-domain Time Harmonic Maxwell's Equations}
\begin{itemize}
  \item Notation:
    \begin{align*}
      \phi_i &:= \phi|_{\Omega_i}, \quad
      \phi_i^\Gamma := \lim_{x\to \Gamma} \phi_i, \quad
      [\phi]_\Gamma := \phi_1^\Gamma - \phi_2^\Gamma.
    \end{align*}
\end{itemize}
\begin{columns}
  \column{.5\textwidth}
  \begin{block}{Subdomain Problems}
    \vspace{-.3cm}
    \begin{align*}
      \blu{i \mu_{r,i} H_i} &\blu{= \nabla \times E_i + f_i^h,} \\
      \blu{-i \epsilon_{r,i} E_i} &\blu{= \nabla \times H_i + f_i^e.}
    \end{align*}
  \end{block}
  \column{.5\textwidth}
  \begin{block}{Transmission Conditions}
    \vspace{-.3cm}
    \begin{align*}
      \red{[(\n \times E)]_\Gamma} &\red{= 0,} \\
      \red{[(\n \times \curl E)]_\Gamma} &\red{= 0.}
    \end{align*}
  \end{block}
  \end{columns}
  \pause
  \begin{block}{Transmission Conditions}
    \begin{itemize}
      \item $\red{[(\n \times E)]_\Gamma = 0}$ enforces continuity of the tangential
        component of the electric field across the interface.
      \item $\red{[(\n \times \curl E)]_\Gamma = 0}$ enforces continuity of the
        tangential component of the magnetic field across the interface.
    \end{itemize}
 \end{block}
\end{frame}

\begin{frame}{Steklov-Poincar\'{e} for Time-Harmonic Maxwell's}
\begin{itemize}
  \item Would like to solve, for $i=1,2$:
    \begin{align*}
      &\blu{i\mu_{r,i} H_i = \nabla \times E_i + f^h_i, \quad
      -i\epsilon_{r,i} E_i = \nabla \times H_i + f_i^e,} \quad
      \red{(\n \times E_i) = \lambda}.
    \end{align*}
  \pause
  \item $\red{\lambda}$ must guarantee
    \begin{align*}
     \red{(\n \times \curl E_1) = (\n \times \curl E_2), \; \text{on } \Gamma.}
    \end{align*}
  \pause
  \item Superposition: define $\blu{E_i^\lambda}$
        as the response of the electric field to the
        interface data $\red{\lambda}$, and
        $\blu{E_i^f}$ as the reponse to the forcing terms.
  \item Define operators
    \begin{align*}
      \red{S\lambda} &\red{:= (\n \times \curl E_1^\lambda)|_\Gamma
                - (\n \times \curl E^\lambda_2)|_\Gamma,} \\
      \red{\chi} &\red{:= (\n \times \curl E_2^f)|_\Gamma
                - (\n \times \curl E_1^f)|_\Gamma.}
    \end{align*}
\end{itemize}
\begin{lemma}[Equivalence of \red{Steklov-Poincar\'{e}} system to TH Maxwell's]
  Suppose $\lambda$ satisifes $S\lambda = \chi$. Then the solution
  to the independent subdomain problems with Dirichlet data $\lambda$
  at the interface solves the time-harmonic Maxwell's equations on
  $\Omega$.
\end{lemma}
\end{frame}

\begin{frame}{A Neumann-Neumann Algorithm for Time-Harmonic Maxwell's}
  \begin{block}{Algorithm nnMaxwell}
    Given $\red{\lambda^0}$, for each $k\geq 0$,
    \begin{enumerate}
      \item Find $\blu{[E^{n+1}, H^{n+1}]}$ by solving, for $i=1,2$,
        \begin{align*}
          \hspace{-.3cm}&\blu{i \mu_{r,i} H_i^{n+1} = \nabla \times E^{n+1}_i + f^h_i,
          \quad
          -i\epsilon_{r,i} E^{n+1}_i = \nabla \times H^{n+1}_i
            + f_i^e,} \quad
          \red{(\n \times E^{n+1}_i) = \lambda^n}.
        \end{align*}
      \item Then find $\blu{\Psi^{k+1}}$ by solving, for $i=1,2$,
        \begin{align*}
          &\blu{\nabla \times (\mu_{r,i}^{-1} \nabla \times \Psi_i^{n+1})
           - \epsilon_{r,i} \Psi_i^{n+1} = 0,}
          \\
          &\red{(\n \times \curl \Psi_i^{n+1}) = [\n\times \curl E_i^{n+1}]|_\Gamma.}
        \end{align*}
      \item Update $\red{\lambda}$ by
        \begin{align*}
          \red{\lambda^{n+1} = \lambda^n - \theta[\Psi^{n+1}]_\Gamma.}
        \end{align*}
      \item Continue with (1) unless stopping criterium
      $\red{\|[\n \times \curl E]_\Gamma\|}$
      holds.
    \end{enumerate}
  \end{block}
  \pause
  \vspace{-.1cm}
  \begin{block}{Preconditioned Richardson Scheme}
    \begin{align*}
      \red{\lambda^{n+1} = \lambda^n - \theta(S_1^{-1} + S_2^{-1})(S\lambda^n - \chi).}
    \end{align*}
  \vspace{-.3cm}
  \end{block}

\end{frame}

\section{Finite Difference Frequency Domain}
\subsection{FDFD}
\begin{frame}{Finite Difference Frequency Domain}
\begin{columns}
  \column{.3\textwidth}
  \begin{block}{FDFD}
    \begin{itemize}
      \small
      \item Finite difference discretization of the time-harmonic problem.
      \item Shares characteristics with the Finite Difference Time Domain method.
      \item Uses \red{Yee's grid.}
    \end{itemize}
  \end{block}
  \begin{block}{Yee's Grid}
    \begin{itemize}
    \small
    \item 2nd order accuracy.
    \item Stores different field components at different locations.
    \item $H$ field determined by surrounding $E$ field and visa-versa.
    \end{itemize}
  \end{block}
  \column{.7\textwidth}
  \includegraphics[width=\textwidth]{images/yeecell.jpg}
\end{columns}
\end{frame}

\section{Multigrid}
\subsection{Multigrid}
\begin{frame}{Multigrid Introduction}
\vspace{-.2cm}\onslide<1->\begin{block}{Motivation}
\begin{itemize}
  \item Many iterative solvers will smooth oscillatory modes of the error of a PDE
    discretization quickly, but struggle to reduce smooth modes of the error.
  \item At a coarser grid, smooth modes of the error are relatively more oscillatory.
\end{itemize}
\end{block}
\onslide<2->\vspace{-1.6cm}\begin{block}{Multigrid}
\begin{itemize}
  \item Exploit a
    heirarchy of discretizations to smooth errors at different
    frequencies.
  \item At each level a stationary iterative solver, e.g. Gauss-Seidel,
    is employed.
  \item Cycling between levels efficiently reduces error.
  \item Extremely efficient: can achieve $O(N)$ complexity.
\end{itemize}
\end{block}
\onslide<3->\vspace{-2.175cm}\begin{block}{What's the process? A simple "V-cycle":}
  \begin{enumerate}
    \item Discretize PDE on the fine grid, obtain linear system $A^h u = f^h$.
    \item Perform the \alert{smoothing step}:
      a few iterations of an iterative solver, e.g. Gauss-Seidel, to reduce
      high-frequency errors, obtain approximate solution $u^h$.
    \item \alert{Project / Relax} to
      to the coarse grid and solve error equation
      $A^{2h} e^{2h} = r^{2h} := f^{2h} - A^{2h} u^{2h}$.
    \item \alert{Interpolate / Prolong}
      error $e^{2h}$ to fine grid and update $u^h + e^{2h}$.
    \item Perform an additional few iterations of Gauss-Seidel, beginning from
      $u^h + e^{2h}$.
  \end{enumerate}
\end{block}
\centering
\onslide<1->\includegraphics[width=.76\textwidth]{images/vcycle.jpeg}
\end{frame}

\section{Conclusion}
\subsection{Conclusion}
\begin{frame}{Summary}
\vspace{-.2cm}
\centering
\includegraphics[scale=.7]{images/comp_setup.pdf}
\begin{itemize}
  \item To efficiently compute the near-field in our optical lithography
  application we will:
  \begin{itemize}
    \normalsize{
    \item[$\to$] Apply iterative domain decomposition, initialized
      by the Kirchhoff approximation.
    \item[$\to$] Discretize subdomain time-harmonic Maxwell equations by
      the finite difference frequency domain approach, with PML and periodic
      boundary conditions.
    \item[$\to$] Solve subdomain problems with a matrix-free multigrid method.
  }
  \end{itemize}
\end{itemize}
\end{frame}
\end{document}
