\beamer@endinputifotherversion {3.26pt}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Model}{6}{0}{2}
\beamer@subsectionintoc {2}{1}{Time-Harmonic Maxwell's Equations}{6}{0}{2}
\beamer@sectionintoc {3}{Kirchhoff's Approximation}{8}{0}{3}
\beamer@subsectionintoc {3}{1}{Kirchoff's Approximation}{8}{0}{3}
\beamer@sectionintoc {4}{Domain Decomposition}{9}{0}{4}
\beamer@subsectionintoc {4}{1}{Iterative Substructuring Method}{9}{0}{4}
\beamer@sectionintoc {5}{Finite Difference Frequency Domain}{18}{0}{5}
\beamer@subsectionintoc {5}{1}{FDFD}{18}{0}{5}
\beamer@sectionintoc {6}{Multigrid}{19}{0}{6}
\beamer@subsectionintoc {6}{1}{Multigrid}{19}{0}{6}
\beamer@sectionintoc {7}{Conclusion}{22}{0}{7}
\beamer@subsectionintoc {7}{1}{Conclusion}{22}{0}{7}
