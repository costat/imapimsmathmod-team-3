\documentclass[10pt]{extarticle}
\usepackage{tcdef}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{framed,color}
\definecolor{shadecolor}{rgb}{.7,.7,.7}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}{Corollary}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{proposition}{Proposition}[section]

\begin{document}
\centerline{\Large{IMA \& PIMS Math Modeling Workshop}}
\vspace{.3cm}
\centerline{\large{Team 3 Progress Report: Fast Calculation of Diffraction
  by Photomasks}}
\vspace{.3cm}
\centerline{Timothy Costa, John Cummings, Michael Jenkinson,}
\centerline{Yeon Eung Kim,
  Jose de Jesus Martinez, Nicole Olivares, Apo Sezginer}
\vspace{.5cm}
\hrule
\vspace{.5cm}

\tableofcontents

\section{Introduction}

This work is motivated by the problems
of manufacturing and quality control of integrated circuits. Integrated circuits
are manufactured by optical lithography.
This process involves several components,
first a photomask is designed, consisting of a layered glass plate
with a pattern etched into
the surface. Light is shown through this plate, and then focused to develop
a photoresist which sits on top of the circuit to be manufactured. The pattern
on the photoresist is then etched onto a silicon wafer. In Figure \ref{fig:optlith1}
a schematic diagram of this process is presented.\\

\begin{figure}
  \includegraphics[width=\textwidth]{talk/images/optlith.pdf}
  \caption{\label{fig:optlith1}Schematic diagram of optical lithography process.}
\end{figure}

Simulation of this process has three components. The propagation of a laser
pulse from the source to the photomask, the calculation of the near field
after passing through the photomask, and the propagation of the scattered field
from the photomask to the device. However, the first and third components of this
process are easily and efficiently modeled by Fourier Optics, while the scattering
problem, i.e. the calculation of the near-field after passing through the photomask,
is computationally intensive. Figure \ref{fig:optlith2} diagrams this procedure.\\

\begin{figure}
  \includegraphics[width=\textwidth]{talk/images/optlith2.pdf}
  \caption{\label{fig:optlith2}Schematic diagram of of the components in simulating
    the optical lithography process.}
\end{figure}

We are interested in the efficient
calculation of the near-field as the light passes through the
photomask. Efficient simulation of the near-field is necessary for
quality control of the production of the photomask, as well as in the optimization
process of the design of photomasks for new chips. This computation is very intensive,
requiring on average 100,000 CPU hours for the design of a single integrated circuit.\\

The near-field in this problem is modeled by the time-harmonic Maxwell equations,
posed in a large 3-dimensional domain with heterogeneous coefficients varying
according to the layers and etching in the photomask. Simulation
of this problem is computationally intensive due to the size of the problem,
and made imprecise by the need to average material parameters over grid cells
that may encompass parts of more than one material.\\

We propose several tools to make the problem computationally tractable, as well
as to alleviate the difficulties presented by material interfaces and piecewise
continuous data. First, we subdivide the domain into material subdomains, and
develop and employ a new non-overlapping
iterative domain decomposition algorithm allowing us to solve the problem
in parallel as well as creating subdomain problems with constant material parameters.
We initialize this iterative procedure by Kirchhoff's approximation, a classical
but inaccurate approximation of the near field which can be calculated in $O(N)$
operations.
Second, we discretize by the finite difference frequency domain method (FDFD), and
propose solving the subdomain problems by a matrix-free multigrid method.\\

We expect that by employing domain decomposition parallelism
initiated by the Kirchhoff approximation, and solving subdomain problems
with matrix-free multigrid will provide a robust and efficient solution
to the near-field scattering problem in the optical lithography application.

\section{Model}
\subsection{Time-Harmonic Maxwell's Equations and Boundary Conditions}
This problem is modeled by the time-harmonic Maxwell's equations. We will
consider the model after normalization, which is detailed in the appendix.
These are given by,
\begin{align}
  \label{eq:hfield}
  i\mu_r H &= \nabla \times E + f^h, \\
  \label{eq:efield}
  -i\epsilon_r E &= \nabla \times H + f^e,
\end{align}
along with appropriate boundary conditions. Here $E$ denotes the electric field,
$H$ the magnetic field, $\epsilon_r$
is the relative electric permittivity, and $\mu_r$ is the relative
magnetic permittivity. We include forcing terms $f^h$ and $f^e$ to
model the incident field caused by a laser pulse before the scatterers (etched
regions) are introduced.\\

For the purposes of proof-of-concept simulations during this work,
we pose the problem of determining the solution of Equations \eqref{eq:hfield} and \eqref{eq:efield} in a 3d computational domain that captures the essential features of the photomask, with appropriate boundary conditions. The computational domain is shown in Figure \ref{fig:pml}. The domain is layered
in the $z$-direction consisting of materials with different permittivities, and etching
creates changes in permittivity in the $x$- and $y$-directions. We assume the geometry
is such that the permittivity is constant on polygonal sections with edges in
coordinate directions. Thus the permittivities are given by diagonal matrices. The photomask is assumed to be infinite and periodic in the $x$- and $y$-directions. Thus, the computational domain consists of one period, and periodic boundary conditions in the $x$- and $y$-directions. In the positive and negative $z$-directions, we must simulate homogeneous media extending to infinity. When truncating the domain, we require a treatment of these boundaries that does not introduce nonphysical reflections of the outgoing scattered field back into the region of interest. This may be achieved in one of a few ways. The first possibility is the use of a perfectly matched layer (PML) boundary condition.
The second is the use of the fast-fourier transform and Green's functions. Both
of these methods are expanded upon in subsequent subsections.    \\


\subsection{Perfectly Non-reflecting Boundary Condition}

For ease of exposition, consider simple a domain $U\subset \mathbb{R}^2$, e.g.
the unit square $[0,1]\times [0,1]$. If a wave travels towards the boundary, then
as is well known, for any Dirichlet type condition the inverse of the wave will be
reflected back into the domain. For a Neumann type condition the wave is
reflected back into the domain with the same orientation. If we were to impose
a periodic boundary condition then the wave would appear on the other side of the domain.
None of these situations are appropriate for our application, and we must enforce
a perfectly non-reflecting boundary condition. That is, we require
that the wave is completely absorbed into the boundary with no reflection. This can
be accomplished by several methods, and we will explore a perfectly matched layer.

\subsubsection{Perfectly Matched Layer Boundary Conditions}

A perfectly matched layer boundary condition is employed by padding
the computational domain along the non-reflecting boundary. In the
case of the unit square, we would simulate the problem on $[0-\eta,
1+\eta]^2$ for some
$\eta > 0$.\\

The non-reflection condition is achieved by designing an
artificial material in the PML region that does not reflect waves. In
addition, the material is designed to very quickly damp the waves that enter into it such
that, by the time an outgoing wave reaches the exterior boundary of the PML layer,
it has decayed to be nearly $0$.
A homogeneous Dirichlet boundary condition may now
be used here since the only waves reaching it
(that is, the only waves that will be reflected back towards the region of interest)
are almost entirely attenuated, and cause very little error in the solution. \\

\begin{figure}
  \centering
  \includegraphics[width=.5\textwidth]{talk/images/pml2.pdf}
  \caption{\label{fig:pml}Illustration of the Perfectly Matched Layer
    concept.}
\end{figure}

\section{Kirchhoff's Approximation}

In the late 1800s Gustav Kirchhoff studied light diffraction by an aperture
in an opaque screen. Kirchhoff devised an approximation of the near-field
that allows for a computation of $O(N)$ complexity. This efficiency
is obtained by ignoring the aperture next to the screen, and ignoring
the screen next to the aperture. Once the near-field is approximated
in a plane, it is easily propagated to the image plane using Green's Theorem.
Figure \ref{fig:kirchh} illustrates this idea.\\

\begin{figure}
  \centering
  \includegraphics[width=.5\textwidth]{talk/images/kirchh.pdf}
  \caption{\label{fig:kirchh}Illustration of Kirchoff approximation.}
\end{figure}

Clearly this approximation ignores essential physics if one is greatly concerned
with accuracy. Thus various improvements have been made. However, these
improvements are still inaccurate, and the explicit computation
of the time-harmonic Maxwell's equations would provide a better solution. However,
for a problem as large as must be simulated for the problem of calculating
the near-field for a photomask the direct solution of Maxwell's equations is
computationally too inefficient. Thus in this work, methods
for solving the time-harmonic Maxwell's equations will be developed that
use the Kirchoff approximation as an initial starting point, and proceed
iteratively to an improved solution.

\section{Domain Decomposition}

Domain decomposition (DDM)
techniques for partial differential equations attempt to solve independent subdomain
problems, rather than the problem as originally posed on a the full domain.
These methods have been designed with two primary purposes in mind. Originally,
domain decomposition was developed as a methodology of parallelism for large, out-of-core
problems. There exist a library of literature in this context from
mathematicians, computer scientists, and computational scientists over decades
that is too large to appropriately summarize here. \\

In more recent years DDM has received growing interest as a tool
for handing multi-physics problems where material parameters or physical models
vary between physical subdomains, as well as for problems with multi-scale
phenomena characterized by interfaces. In this context DDM has been successfully
applied to problems of fluid flow \cite{girault2005discontinuous},
the coupling of Darcy and Stokes flows \cite{discacciati2003analysis},
\cite{discacciati2007robin}, as well
as in problems of electrostatics \cite{haii}, \cite{haii2}, among other applications.
\\

The physical problem we are interested in computing is composed of layered materials
separated by interfaces. This combined with the size of the problem
suggests the use of domain decomposition techniques. \\

In recent years there has been a growing interest in domain decomposition
for the time-harmonic Maxwell's equations. Methods of iterative substructuring
type \cite{alonso1997domain}, \cite{alonso1999optimal} have been developed, as well as
Schwarz
type preconditioners \cite{lietal}. In this work, we will develop
and analyze a iterative substructuring algorithm of Neumann-Neumann type, and then
extend the algorithm to the case of many subdomains by incorporating a two-level
correction, i.e. a balancing Neumann-Neumann method. Neumann-Neumann methods
have the advantage of employing non-overlapping subdomains and obtaining convergence
independent of the resolution. That is, the Neumann-Neumann method
has $O(1)$ complexity. Thus the computational complexity of a computational paradigm
employing Neumann-Neumann methods is determined entirely by the complexity of
the subdomain solvers, while simultaneously reducing the size of $N$ for these
solvers and allowing parallelism.\\

Iterative substructuring algorithms of Neumann-Neumann type work by first
defining a multi-domain formulation of a model partial differential equation problem.
An equation posed on the interface is then developed whose solution will guarantee
that the multidomain formulation is consistent with the original partial differential
equation. An iterative procedure is then performed to solve the interface equation
and the independent subdomain problems.

\subsection{Two-domain time harmonic Maxwell's equations}

In this section we develop the two-domain model. This is trivially extended
to the many subdomain model by requiring the transmission conditions obtained here
hold between all adjacent subdomains.\\

Rather than solving the problem on the domain $\Omega$, we subdivide the domain
into two components $\Omega_1$ and $\Omega_2$ such that $\cap_i \Omega_i = \emptyset$,
$\cup_i \overline{\Omega_i} = \overline{\Omega}$, and
$\cap_i \overline{\Omega_i} := \Gamma$ is an 2 dimensional manifold.\\

Then we consider the two-domain formulation of the time harmonic Maxwell
system given by,
\begin{align}
  \label{eq:multieq}
  \text{Subdomain problems:  }
  &\left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E_i + f^h, & \text{in } \Omega_i, i=1,2, \\
    -i\epsilon_{r,i} E_i = \nabla \times H_i + f^e, & \text{in }
      \Omega_i, i=1,2.
  \end{array}\right.\\
  \label{eq:trans}
  \text{Transmission conditions: }
  &\left\lbrace \begin{array}{l}
    (\n \times E_1)|_\Gamma = (\n \times E_2)|_\Gamma, \\
    (\n \times \curl E_1)|_\Gamma = (\n \times \curl E_2)|_\Gamma.
  \end{array}\right.
\end{align}
The first of these transmission conditions ensures that the tangential
component of the electric field is continuous across the interface $\Gamma$, while
the second of these transmission conditions ensures that the tangential
component of the magnetic field is continuous across the interface $\Gamma$.\\

It is a well-known result that these transmission conditions ensure that a solution to
the two-domain problem solves the single domain problem \cite{alonso1999optimal}.

\subsection{Independent Subdomain Problems \& Steklov-Poincar\'{e} Interface
Equation}

Clearly the problem given by \eqref{eq:multieq}-\eqref{eq:trans} does not
consist of independent subdomain problems, as the transmission conditions
\eqref{eq:trans} couple the solutions of the two subdomain problems. Thus
we would like to solve instead the decoupled problems,
\begin{align}
  \left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E_i + f^h & \text{in } \Omega_i, \\
    -i\epsilon_{r,i} E_i = \nabla \times H_i + f^e & \text{in }
      \Omega_i, \\
    (\n \times E_i)|_\Gamma = \lambda,
  \end{array}\right.
\end{align}
for some value $\lambda$ to be determined. For any value of $\lambda$, we satisfy the
first transmission condition
\begin{align}
  (\n \times E_1)|_\Gamma = (\n \times E_2)|_\Gamma,
\end{align}
however, we have no reason to expect that this problem ensures continuity
of the tangential component of the magnetic field. Thus we must develop
a tool for finding the correct $\lambda$ such that
\begin{align}
  (\n \times \curl E_1)|_\Gamma = (\n \times \curl E_2)|_\Gamma
\end{align}
is satisfied.\\

Applying superposition, we define $E_i^\lambda$ and $E_i^f$ to be the responses
to the interface data $\lambda$, or forcing term $f$. Thus, these terms solve
the equations,
\begin{align}
  &\left\lbrace
  \begin{array}{lr}
    \nabla \times(\mu_{r,i}^{-1} \nabla \times E_i^\lambda)
    - \epsilon_{r,i} E_i^\lambda = 0
    & \text{in } \Omega_i, \\
    (n \times E_i^\lambda) = \lambda & \text{on } \Gamma.
  \end{array}
  \right.\\
  &\left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E^f_i + f^h & \text{in } \Omega_i, \\
    -i \epsilon_{r,i} E^f_i = \nabla \times H_i + f^e & \text{in }
      \Omega_i, \\
    (\n \times E_i)|_\Gamma = 0,
  \end{array}\right.
\end{align}
respectively.\\

Then we define the operators
\begin{align}
  S\lambda &:= (\n \times \curl E_1^\lambda)|_\Gamma
    - (\n \times \curl E_2^\lambda)|_\Gamma,\\
  \chi &:= (\n \times \curl E_2^f)|_\Gamma - (\n \times \curl E_1^f)|_\Gamma,
\end{align}
and seek to solve
\begin{align}
  S\lambda = \chi.
\end{align}

It is an exercise in algebra to see that if $\lambda$ satisfies this
Steklov-Poincar\'{e} equation, then a solution to the independent subdomain problems
will solve the two-domain formulation of the time harmonic Maxwell system, and thus
solves the original problem.

\subsection{Algorithm nnMaxwell}

An algorithm of the Neumann-Neumann type for this problem
has the following form:\\

Given $\lambda^0$,
\begin{enumerate}
  \item Solve
    \begin{align}
      \left\lbrace \begin{array}{lr}
        i\mu_{r,i} H^{n+1}_i = \nabla \times E^{n+1}_i
         + f^h & \text{in } \Omega_i, \\
        -i\epsilon_{r,i}E^{n+1}_i
        = \nabla \times H^{n+1}_i + f^e
        & \text{in } \Omega_i, \\
        (\n \times E^{n+1}_i) = \lambda^n & \text{on } \Gamma.
      \end{array}\right.
    \end{align}
  \item Then solve the auxiliary problem,
    \begin{align}
      \left\lbrace
      \begin{array}{lr}
        \nabla \times(\mu_{r,i}^{-1} \nabla \times \Psi^{n+1}_i) - \epsilon_{r,i}
          \Psi^{n+1}_i = 0 & \text{in } \Omega_i, \\
        (\n \times \curl \Psi^{n+1}_i) = [\n\times \curl E_i^{n+1}]|_\Gamma
        & \text{on } \Gamma.
      \end{array}
    \right.
    \end{align}
  \item Then update $\lambda$,
  \begin{align}
    \lambda^{n+1} = \lambda^n - \theta [\Psi^{n+1}]_\Gamma.
  \end{align}
  \item Check stopping criteria, e.g. $\|[(\n \times \curl E)]_\Gamma \|$,
    return to (1) if criteria is not met, else exit..
\end{enumerate}

\subsection{Convergence Analysis}

We note that this is a pre-conditioned Richardson scheme for the equation
$S\lambda = 0$. To see this, notice,
\begin{align}
  \Psi_1^{n+1} = S_1^{-1}(S\lambda^n - \chi), \\
  \Psi_2^{n+1} = -S_2^{-1}(S\lambda^n - \chi),
\end{align}
so that we can write the $\lambda$ update step as,
\begin{align}
  \lambda^{n+1} = \lambda^n - \theta(S_1^{-1} + S_2^{-1}) (S\lambda^n - \chi).
\end{align}

The question of convergence of this scheme will depend on properties of the operator
$S$, and will be explored in the weak setting.

%\section{Finite Difference Frequency Domain}
%
%\begin{figure}
%  \centering
%  \includegraphics[width=.5\textwidth]{talk/images/yeecell.jpg}
%  \caption{\label{fig:yee}A single Yee cell in 3d.}
%\end{figure}

\section{Multigrid}

Multigrid methods are iterative solves for the discretization of partial
differential equations that exploit a hierarchy of discretizations
to smooth errors at different frequencies. These methods have been shown
to be extremely efficient solvers, often achieving $O(N)$ computational complexity.\\

Stationary iterative solvers such as the Jacobi or Gauss-Seidel methods
very efficiently reduce high frequency, highly oscillating components of the error
in a linear system. However these methods are inefficient at resolving the smoother
parts of the error. Multigrid takes advantage of the fact that smooth components of the
error on a fine grid are (relatively) highly oscillating on a coarser grid. Thus
a stationary iterative solver is employed for a few iterations at a hierarchy
of grid levels in order to resolve the total error.\\

Multigrid methods can be decomposed into three steps:
\begin{enumerate}
  \item Smoothing: this step reduces high frequency errors by a standard iterative
    method, e.g. the Gauss-Seidel method.
  \item Restriction: this step downsamples the residual error to a coarser grid.
  \item Interpolation or prolongation: this step interpolates a coarse correction
    from the restriction step into a finer grid.
\end{enumerate}

For exposition, we consider a simple two-level $V$-cycle.
Considering a linear system, $A^h u = f^h$ resulting from the discretization
of a partial differential equation on a grid of mesh size $h$, we denote by
$u^h$ the results of performing a limited number of iterative steps in a
stationary solver such as the Jacobi or Gauss-Seidel methods. This is
referred to as a smoothing step. Next, the error equation is considered,
\begin{align}
  A^h e^h = r^h := f^h - A^h u^h.
\end{align}
The solution $e^h$ is then restricted, or projected, to a coarser grid, e.g.
a mesh size $2h$, and the error equation is solved. Denoting by $P^h_{2h}$
the operator that performs this restriction, we solve
\begin{align}
  A^{2h} e^{2h} = r^{2h} := A^{2h} P^h_{2h} u^h - f^{2h}.
\end{align}
Denoting by $I^{2h}_h$ the operator that interpolates or prolongs from the coarse grid
back to the finer grid, we then update the solution $u^h$ by
\begin{align}
  u^h = u^h + I^{2h}_h e^{2h}.
\end{align}

The $V$-cycle is the simplest form of the multigrid method, and many variations
in cycling exist. Figure \ref{fig:multigrid} illustrates the
hierarchy of grids.\\

\begin{figure}
  \centering
  \includegraphics[width=.8\textwidth]{talk/images/vcycle.jpeg}
  \caption{\label{fig:multigrid}Illustration of grid hierarchy
    in a simple V-cycle multigrid method.}
\end{figure}

It is worth noting as well that multigrid methods with intentionally reduced
tolerances can be used as an efficient preconditioner for some other
solver. This is particularly advantageous for nonlinear problems, where
the use of multigrid preconditioning for another multigrid solver
can still obtain $O(N)$ efficiency.\\

For our purposes we will apply the multigrid method to the subdomain problems
generated by the domain decomposition algorithm, as an efficient method
of resolving the resulting linear systems.

\section{Conclusion}

The problem of calculating the near-field on the far side of a photomask
is computationally expensive, and must be performed many times in the
process of optimizing the manufacturing of integrated circuits. In
our work, we will use the Kirchhoff approximation as an initial guess,
and simulate the time-harmonic Maxwell system iteratively to improve
the solution. \\

We will develop, analyze and test Neumann-Neumann type iterative substructuring methods,
as well as apply a multigrid solver to the subdomains resulting from
domain decomposition. This work will be discretized by the
finite difference frequency domain method. Further, we will explore
a novel methodology for the creation of a perfectly absorbing
boundary condition, and compare its performance to a perfectly matched layer
paradigm.

\nocite{*}
\bibliographystyle{plain}
\bibliography{team3_progress_report}

\end{document}
