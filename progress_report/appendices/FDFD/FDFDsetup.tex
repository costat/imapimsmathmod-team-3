\documentclass[10pt]{article}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{framed,color}
\definecolor{shadecolor}{rgb}{.7,.7,.7}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}{Corollary}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{proposition}{Proposition}[section]

\begin{document}
\section{Forcing Term Expressed as Deviation from a Background}

The electromagnetic field $(E, H)$ is sourced by the electric and magnetic current densites $J,M$.        Frequently the original source of the EM radiation is far from the computation domain.  For example the laser that illuminates a lithographic photomask is far from the photomask and there are many optical components in between.  In such cases expressing the forcing function in terms of a known incident wave is advantageous.  We introduce the concepts of background geometry, materials and fields.  The background problem has three properties: 
it is easy to solve; it has the same primary sources as in the original problem; the background geometry and materials agree with those in the original problem in most places.  
The original problem is: 
\begin{align}
&\nabla \times E = i \mu_r H  + M_{pri}\\
&\nabla \times H = -i \epsilon_r E + J_{pri} \\
\end{align}
where $\mu_r$ and $\epsilon_r$ are inhomogeneous and possibly anisotropic. The background problem is: 
\begin{align}
&\nabla \times E_{bg} = i \mu_{bg} H_{bg}  + M_{pri}\\
&\nabla \times H_{bg} = -i \epsilon_{bg} E_{bg} + J_{pri} \\
\end{align}
We difference the above equations to obtain: 
\begin{align}
&\nabla \times(E- E_{bg}) = i \mu_r (H- H_{bg})  +  i ( \mu_r - \mu_{bg} ) H_{bg} \\
&\nabla \times (H- H_{bg}) = -i \epsilon_r (E-E_{bg}) - i ( \epsilon_r - \epsilon_{bg} ) E_{bg} \\
\end{align}
We define the scattered field as the difference between the fields of the original problem and the background fields: 
\begin{align}
& E_{sca} = E - E_{bg}\\
& H_{sca} = H - H_{bg}\\
\end{align} 
We define the induced magnetic and electric current densities: 
\begin{align}
& M_{ind} =  i ( \mu_r - \mu_{bg} ) H_{bg}\\
& J_{ind} = - i ( \epsilon_r - \epsilon_{bg} ) E_{bg}\\
\end{align}
The induced currents exist only where the background materials differ from the materials of the original problem. 
Maxwell's equations for the scattered fields are: 
\begin{align}
&\nabla \times E_{sca} = i \mu_r  H_{sca}  + M_{ind}\\
&\nabla \times H_{sca} = -i \epsilon_r E_{sca} + J_{ind} \\
\end{align}
 The scattered field satisfies the original PDE but with a different forcing function.   The scattered fields are excited by the induced electric current density $J_{ind}$ and the induced magnetic current density $M_{ind}$. 


\section{Setting up FDFD Equations}
We discretize the time-harmonic Maxwell's equations:
\begin{align}
&\nabla \times E_{sca} = i \mu_r H_{sca}  + M_{ind}\\
&\nabla \times H_{sca} = -i \epsilon_r E_{sca} + J_{ind} \\
\end{align}
 using Yee's grid 
[Kane S. Yee, "Numerical Solution of Initial Boundary Value Problems Involving Maxwell's Equations in Isotropic Media," IEEE Trans. Anntennas and Propagation, Vol. AP-14, No. 3, pp. 302-307, May 1966]. 
The discretized equations form a linear system of equations. There are at least three ways to  set up the linear system of equations.  We can eliminate the magnetic or the electric field from the vector of unknowns.  Then we have $3 N_x N_y N_z$ complex, scalar unknowns where $N_x N_y N_z$ is the number of Yee cells in the computation domain.  Alternatively, we can have $6 N_x N_y N_z$ complex, scalar unknowns which are the electric and magnetic vector field components; and solve a first-order difference equation.   




\section{Electric Field FDFD Equation}

We eliminate the magnetic field and  set up a linear system of equations for the electric field.  The action of the coefficient matrix on the unknown vector E is calculated as follows: 
$$
A_e \; E_{sca} = -i \nabla_h \times \left(  \mu_r^{-1} \nabla_e \times E_{sca}  \right)+ i \epsilon_r E_{sca}
$$
The finite-difference curl operators $\nabla_e$ and $\nabla_h$ are different because they act on staggered grids.  These operators particularly differ at the boundary of the computation domain. 
The forcing term for the electric field FDFD equations is: 
$$
f_e = J_{ind} - i \nabla_h \times \left(  \mu_r^{-1} M_{ind}  \right)
$$
We obtain the electric field by solving the equation:
$$
A_e \; E_{sca} = f_e 
$$
and calculate the total field: $  E =  E_{sca} + E_{bg} $.  The magnetic field is obtained from the electric field: 
$$
 H =   -i \mu_r^{-1}  \nabla_e \times E
$$
In the last equation we assumed that the magnetic current source of the original problem $ M_{ori}$ is zero in the computation domain. 



\section{Magnetic Field FDFD Equation}
We eliminate the electric field to set up a linear system of equations for the magnetic field.   The action of the coefficient matrix on the unknown vector H is calculated as follows: 
$$
A_h \; H_{sca} = i \nabla_e \times \left( \epsilon_r^{-1} \nabla_h \times H_{sca} \right) - i \mu_r H_{sca}
$$
The forcing term for the magnetic field FDFD equations is: 
$$
f_h = M_{ind} +i \nabla_e \times \left( \epsilon^{-1} J_{ind} \right)
$$
We obtain the magnetic field by solving the equation:
$$
A_h \; H_{sca} = f_h 
$$
and calculate the total field: $  H =  H_{sca} + H_{bg}  $
 The electric field is obtained from the magnetic field: 
$$
E =  i \epsilon_r^{-1} \nabla_h \times H
$$
In the last equation we assumed that the electric current source of the original problem $ J_{ori}$ is zero in the computation domain. 


\section{First-Order FDFD Equation}
In this case we choose the unknown vector $[E,H]$.  We don't eliminate either $E$ or $H$.  This results in an equation that has only first order derivatives.  We solve the linear equation: 
$$
A \quad \left[ \begin{array}{c} E \\ H \end{array} \right]  = f
$$
The matrix-vector product is calculated as: 
$$
A \quad \left[ \begin{array}{c} E \\ H \end{array} \right] = 
\begin{bmatrix} 
\nabla_e \times E & -i \mu_r H \\
-i \epsilon_r E & \nabla_h  \times H \\ 
\end{bmatrix}
$$ 
The forcing term is: 
$$
f =  \left[  \begin{array}{c} 
 M_{ind} \\
 J_{ind}  \\
 \end{array} \right] 
$$

\section{Condition Number of Equations} 

We initially coded the linear equations without storing the coefficient matrices.  We evaluated the matrix-product by evaluating finite-difference curls according to Yee's grid.  We found that GMRES reduced the residual slowly and stalled.  To investigate the cause, we formed the sparse matrices for a small problem ( 1 wavelength x 1 wavelength x 3.5 wavelength ) and solved the linear equations with MATLAB back slash operation and estimated the condition numbers. We observed the following performance: 

\begin{quote}
\begin{tabular}{  | c | r | r | r |  }
\hline
Unknown Vector&  \# Unknowns & Condition Number &  Relative Residual after\\
                          &                        &                              & 100 GMRES Iterations \\
\hline
$E$ & 10500 & 23400 &  0.29 \\
$H$ & 10500 & 15500 &  0.13 \\
$[E,H]$ & 31500 & 4400 & 0.87 \\
\hline
\end{tabular}
\end{quote}
The first-order equation had the lowest condition number yet it had the largest GMRES relative residual.  GMRES was run 100 iterations without restarting.  Restarting did not reduce the residual.   

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{spy_FDFD_E.jpg}
  \caption{Coefficient matrix for a 3-D FDFD E-field equation}
\end{figure}



\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{spy_FDFD_H.jpg}
  \caption{Coefficient matrix for a 3-D FDFD H-field equation}
\end{figure}

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{spy_FDFD_EH.jpg}
  \caption{Coefficient matrix for a 3-D, first-order FDFD equation}
\end{figure}

The fields computed by the three formulations agree when the linear equations are solved without using an iterative solver.  This can be seen in the figures below.
\begin{figure}[h!]
  \centering
\vspace*{-2 cm}
    \includegraphics[width=\textwidth]{smallProb_Ey.jpg}
    \includegraphics[width=\textwidth]{smallProb_Hx.jpg}
  \caption{The real parts of $E_y$ and $H_x$  field components computed by three different FDFD formulations.  The relative difference is on the order of 3\%.  The mesh size is 1/10 of the vacuum wavelength.  Relative difference is the ratio of $\ell_2$ norms.}
\end{figure}



\end{document}