\documentclass[8pt]{beamer}

% Setup appearance:
\usetheme{Frankfurt}
\usecolortheme{default}
\setbeamertemplate{navigation symbols}{}
%\setbeamertemplate{footline}[frame number]
% Standard packages
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{verbatim}
\usepackage{fancybox}
\usepackage{multimedia}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subcaption}
\usepackage{tcdef}
\usepackage{enumerate}
\usepackage{caption}
\usepackage{xcolor}
\usepackage{natbib}
\usepackage{epstopdf}
\usepackage{animate}
%\newcommand{\newblock}{}

\newcommand{\eq}[1]{\begin{align*}#1\end{align*}}
\newcommand{\eql}[2]{\begin{align}\label{#1}#2 \end{align}} %usage: \eq{ .... }

% Author, Title, etc.
\title[]
{Team 3 Final Report: Fast Calculation of Diffraction by Photomasks}
\author[]{Timothy Costa, John Cummings, Michael Jenkinson, \\ Yeon Eung Kim,
  Jose de Jesus Martinez, Nicole Olivares, Apo Sezginer}
\date[August, 2014]
  {Mathematical Modeling in Industry XVIII, IMA / PIMS,
    August, 2014
  }

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Motivation}
\subsection{Motivation}

\begin{frame}{Motivation: Optical Lithography}
  \vspace{-.05cm}
  \hspace{-.35cm}\includegraphics[scale=.6]{images/optlith.pdf}
  \pause
  \begin{block}{Photomask pattern $\not=$ scaled copy of device pattern}
    \begin{itemize}
      \small
      \item Precise characteristics of mask don't appear in
        device (non-uniqueness of mask for IC).
      \item Optimization: best mask to handle various
            problems, e.g. imperfect focus.
      \item Requires fast forward solver.
      \item Takes on the order of 100,000 CPU hours to design a mask for a single
            integrated circuit.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{The Goal}
  \vspace{-.7cm}\hspace{-.55cm}\includegraphics[scale=.5]{images/optlith2.pdf}
\pause
  \begin{block}{Expensive!}
    \begin{itemize}
      \item A typical problem size for the scattering problem is $5e5 \times 5e5$
        wavelengths in the $x$-$y$ plane (smaller in the $z$ direction).
       10 grid cells per wavelength: huge!
      \item Requires new efficient methods.
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Kirchhoff's Approximation}
\begin{columns}
\column{.6\textwidth}
  \includegraphics[width=1\textwidth]{images/kirchh.pdf}
\column{.4\textwidth}
\begin{itemize}
  \item Kirchhoff's approximation can be computed in $O(N)$ operations.
  \item This is not an accurate approximation, causes discontinuities
  near interface between aperture and screen.
  \item Various methods exist to improve the accuracy.
\end{itemize}
\end{columns}
\end{frame}

\section{Model}
\subsection{Time-Harmonic Maxwell's Equations}

\begin{frame}{Mathematical Model}
  \begin{block}{3d Time-harmonic Maxwell's Equations}
    \vspace{-.3cm}
    \begin{align*}
      i \mu_r H &= \nabla \times E - M \\ -i \epsilon_r E &= \nabla
      \times H - J.
    \end{align*}
  \end{block}
  \vspace{0.5cm}
  \begin{columns}
    \column{.5\textwidth}
    \begin{block}{Primary Variables}
      \vspace{-.3cm}
      \begin{align*}
        &E: \quad \text{Electric Field} \\
        &H: \quad \text{Magnetic Field}
      \end{align*}
    \end{block}
    \column{.5\textwidth}
    \begin{block}{Data}
      \vspace{-.3cm}
      \begin{align*}
        &\mu_r: \quad \text{Relative magnetic permeability} \\
        &\epsilon_r: \quad \text{Relative electric permittivity} \\
        &M, J: \quad \text{Source terms}
      \end{align*}
    \end{block}
  \end{columns}
  \vspace{1cm}
  \begin{center}
    Plus Boundary Conditions...
  \end{center}
\end{frame}


% -------------------------------------------------

\begin{frame}{Boundary Conditions}
  \begin{columns}
    \column{.4\textwidth}
    \begin{block}{Boundary Conditions}
      \begin{itemize}
      \item Periodic in $x$- and $y$-directions
      \item Non-reflecting boundary condition in $z$-direction
      \end{itemize}
    \end{block}
    \column{.6\textwidth} \centering \vspace{-0.62cm}
    \includegraphics[scale=.35]{images/pml2a.eps}
    
  \end{columns}
\end{frame}


% ---------------------------------------------------------------------------

\begin{frame}{Incorporating a Plane Wave Source at Infinity}

\begin{columns}
  \column{0.53\textwidth}
  \begin{block}{The Original Problem}
    \begin{align*}
      &\nabla \times E = i \mu_r H  + M_{pri}\\
      &\nabla \times H = -i \epsilon_r E + J_{pri} \\
    \end{align*}
  \end{block}
  \column{0.53\textwidth}
  \begin{block}{The Background Problem}
    \begin{align*}
      &\nabla \times E_{bg} = i \mu_{bg} H_{bg}  + M_{pri}\\
      &\nabla \times H_{bg} = -i \epsilon_{bg} E_{bg} + J_{pri}\\
    \end{align*}
  \end{block}
\end{columns}

\vspace{0.5cm}

The background problem:
\begin{itemize}
\item can be solved exactly
\item has the same primary sources as the original problem
\item agrees with the geometry and materials of the original problem
  in most places
\end{itemize}

\pause \vspace{0.5cm} Next:
\begin{itemize}
\item subtract the two sets of equations
\item define the scattered fields $E_{sca} = E - E_{b g}$ and $H_{sca}
  = H - H_{b g}$
\item obtain a new problem for the scattered fields
\end{itemize}



\end{frame}
% ----------------------------------------------------------------------------

\begin{frame}{PDEs to be Discretized}

  \begin{block}{The Scattered Field Problem}
    \begin{align*}
      &\nabla \times E_{sca} = i \mu_r  H_{sca}  + M_{ind}\\
      &\nabla \times H_{sca} = -i \epsilon_r E_{sca} + J_{ind} \\
      \nonumber
    \end{align*}
  \end{block}
  \vspace{0.5cm}


  Induced magnetic and electric current densities:
  \begin{align*}
    & M_{ind} =  i ( \mu_r - \mu_{bg} ) H_{bg}\\
    & J_{ind} = - i ( \epsilon_r - \epsilon_{bg} ) E_{bg}\\
  \end{align*}

  After solving for $(E_{sca}, H_{sca})$, the total fields may be
  recovered:

\begin{align*}
  E &= E_{sca} + E_{b g}\\
  H &= H_{sca} + H_{b g}\\
\end{align*}


\end{frame}

% ----------------------------------------------------------------------------
\section{Perfectly Matched Layer}
\subsection{Perfectly Matched Layer}



\begin{frame}{Perfectly Matched Layer}
  \begin{columns}
    \column{.4\textwidth}
    \begin{block}{Boundary Conditions}
      \begin{itemize}
      \item Periodic in $x$- and $y$-directions
      \item Non-reflecting boundary condition in $z$-direction
      \end{itemize}
    \end{block}
    \uncover<2-2>{
      \begin{block}{Perfectly Matched Layer}
        \begin{itemize}
        \item Augment the computational domain
        \item Particular anisotropic material parameters prevent
          outgoing waves from being reflected back into the domain
        \item Waves attenuate rapidly in the PML layer
        \item May now specify a homogeneous Dirichlet boundary
          condition in the $z$-direction
        \end{itemize}
      \end{block}
    } \column{.6\textwidth} \centering \only<1>{ \vspace{-0.62cm}
      \includegraphics[scale=.35]{images/pml2a.eps}
    }\only<2>{
      \includegraphics[scale=.35]{images/pml2.eps}
    }
  \end{columns}
\end{frame}



% ----------------------------------------------------------------------------
\begin{frame}{Perfectly Matched Layer}

  \begin{itemize}
  \item Within the PML,
    $\overline{\overline{\epsilon}}=\epsilon_r\overline{\overline{S}}$
    and $\overline{\overline{\mu}}=\mu_r\overline{\overline{S}}$,
    where
    \[ \overline{\overline{S}}=\left[ \begin{array}{ccc}
        1 + \frac{i\sigma}{\epsilon_r}&0&0\\
        0&1 + \frac{i\sigma}{\epsilon_r}&0\\
        0&0&\frac{1}{1 + \frac{i\sigma}{\epsilon_r}}
      \end{array} \right] 
    \]

    \vspace{0.5cm}
  \item The function $\sigma(z)$ is zero at the PML interface,
    increasing as $z$ approaches the boundary.

    \vspace{0.5cm}
  \item Transmitted fields acquire a factor of $e^{-\sigma\sqrt{\frac{\mu_r}{\epsilon_r}}\cos (\theta^i)z}$
\pause
\vspace{0.5cm}
\item Movie of exact solution...

\end{itemize}
\end{frame}

% ----------------------------------------------------------------------------

\section{Finite Difference Frequency Domain}
\subsection{Finite Difference Frequency Domain}
% \subsection{Motivation}



\begin{frame}{FDFD Discretization: Yee's Grid}

  \begin{block}{Time-harmonic Maxwell's Equations}
    \begin{align*}
      &\nabla \times E_{sca} = i \mu_r H_{sca}  + M_{ind} \\
      &\nabla \times H_{sca} = -i \epsilon_r E_{sca} + J_{ind}.
    \end{align*}
  \end{block}

  \begin{center}
    \includegraphics[scale=.37]{images/yeecell.jpg}
  \end{center}

\end{frame}

% --------------------------------------------------------------------------------------

\begin{frame}{Electric Field FDFD Equation}

  \begin{itemize}
  \item Solve for $H_{sca}$ and eliminate it to get
    \[
    A_e \; E_{sca} = f_e, \text{ where}
    \]
    \begin{align*}
      A_e \; E_{sca} = -i\nabla_h\times \left( \mu_r^{-1}
        \nabla_e\times E_{sca} \right) + i\epsilon_r E_{sca},
    \end{align*}
    \begin{align*}
      f_e = J_{ind} - i \nabla_h \times \left( \mu_r^{-1} M_{ind}
      \right).
    \end{align*}

    \vspace{0.5cm}
  \item Solve $A_e \; E_{sca} = f_e$ to obtain $E_{sca}$.

    \vspace{0.5cm}
  \item Calculate the total field $ E = E_{sca} + E_{bg} $.

    \vspace{0.5cm}
  \item Obtain the magnetic field:
    \begin{align*}
      H = -i \mu_r^{-1} \nabla_e \times E
    \end{align*}

    % We assumed that the magnetic current source of the original
    % problem $ M_{ori}$ is zero in the computation domain.
 
  \end{itemize}
\end{frame}

% --------------------------------------------------------------------------------------

\begin{frame}{Magnetic Field FDFD Equation}

  \begin{itemize}
  \item Solve for $E_{sca}$ and eliminate it to get
    \[
    A_h \; H_{sca} = f_h, \text{ where}
    \]
    \begin{align*}
      A_h \; H_{sca} = i \nabla_e \times \left( \epsilon_r^{-1}
        \nabla_h \times H_{sca} \right) - i \mu_r H_{sca},
    \end{align*}
    \begin{align*}
      f_h = M_{ind} +i \nabla_e \times \left( \epsilon^{-1} J_{ind}
      \right)
    \end{align*}

    \vspace{0.5cm}
  \item Obtain the magnetic field by solving $A_h \; H_{sca} = f_h$

    \vspace{0.5cm}
  \item Calculate the total field $ H = H_{sca} + H_{bg} $.

    \vspace{0.5cm}
  \item Obtain the magnetic field:
$$
E = i \epsilon_r^{-1} \nabla_h \times H
$$

\end{itemize}
\end{frame}

% --------------------------------------------------------------------------------------


\begin{frame} {First-Order FDFD Equation}

  \begin{itemize}
  \item Do not eliminate $E$ or $H$. Solve
    % This results in an equation that has only first order
    % derivatives
    \eq{ A \quad
      \begin{bmatrix}
        \ E\ \\ \ H\
      \end{bmatrix}
      =f }

    \vspace{0.5cm}
  \item The matrix-vector product is: \eq{ A \quad
      \begin{bmatrix}
        \ E\ \\ \ H\
      \end{bmatrix}
      =
      \begin{bmatrix}
        \nabla_e \times E & - i \mu_r H\\
        -i \epsilon_r E & \nabla_h \times H\\
      \end{bmatrix}
    }

\vspace{0.5cm}
  \item The forcing term is: \eq{ f=
      \begin{bmatrix}
        \ M_{ind}\ \\ \ J_{ind} \
      \end{bmatrix}
    }
  \end{itemize}

\end{frame}



\begin{frame}{PML Results}
  \begin{columns}
    \column{0.45\textwidth}
    \includegraphics[scale=.33]{images/pmlDirect_PML0_EH.eps}
    \column{0.45\textwidth}
    \includegraphics[scale=.33]{images/pmlDirect_PML1_EH.eps}
  \end{columns}

\end{frame}



% -------------------------------------------------



\begin{frame}{Effective Permittivity}
  \begin{columns}
    \column{.4\textwidth}
    \begin{block}{Boundary Conditions}
      \begin{itemize}
      \item Ambiguity in normal field component at boundary.
      
      \item Effective permittivity \begin{align*} \epsilon^* \equiv
          \frac{2 \epsilon_1 \epsilon_2} { \epsilon_1 + \epsilon_2}.
        \end{align*}
      
      \item Calculated from integral
        \begin{align*}
          \hspace{- 2cm}   &  \left( \int_{-\Delta z/2}^{\Delta z/2} \frac{dz}{\epsilon \Delta z} \right) \oint_{\partial \Omega}  H \cdot d\ell \\
          & \quad = \int_{\Omega} \left( \int_{\Delta z/2}^{\Delta
              z/2} \frac{E \ dz}{\Delta z} \right) \cdot ds,
        \end{align*}
      \end{itemize}
      $ \quad \quad $ across boundary.
    \end{block}
    
    \column{.6\textwidth} \centering \only<1>{ \vspace{-0.62cm}
      \includegraphics[scale=.28]{images/permittivity.pdf}}
  \end{columns}
\end{frame}

% ---------

\begin{frame}{Electrical Permittivity}

  \centering
  \includegraphics[scale=0.65]{images/epsilon.pdf}

\end{frame}





\begin{frame} {Coefficient matrix for a 3-D FDFD E-field equation}

  Below are the coefficient matrices for the 3-D FDFD $E$, $H$ and
  $[E,H]$ field equation

  % \begin{figure}
  %   \begin{subfigure}[b]{0.33\textwidth}
  %     \includegraphics[width=\textwidth]{images/spy_FDFD_E.pdf}
  %   \end{subfigure}\hspace{0.5cm}
  %   \begin{subfigure}[b]{0.33\textwidth}
  %     \includegraphics[width=\textwidth]{images/spy_FDFD_H.pdf}
  %   \end{subfigure}
  %   \begin{subfigure}[b]{0.33\textwidth}
  %     \includegraphics[width=\textwidth]{images/spy_FDFD_EH.pdf}
  %   \end{subfigure}
  % \end{figure}

  \vspace{0.3cm}
  \includegraphics[scale=0.25]{images/spy_FDFD_E.pdf}
  \
  \includegraphics[scale=0.25]{images/spy_FDFD_H.pdf}
  \
  \includegraphics[scale=0.25]{images/spy_FDFD_EH.pdf}



\end{frame}


% --------------------------------------------------------------------------------------

\begin{frame} {Condition Number of Equations}

  \begin{itemize}
  \item Coded the linear equations without storing the coefficient
    matrices.
  \item Evaluated the matrix-product by evaluating finite-difference
    curls according to Yee's grid.
  \item Found that GMRES reduced the residual slowly and stalled.
  \item Formed the sparse matrices for a small problem
  \item Solved the linear equations directly and estimated the
    condition numbers
  \end{itemize}

  \vspace{0.5cm}

  \begin{quote}
    \begin{tabular}{ | c | r | r | r | }
      \hline
      Unknown &  \# Unknowns & \qquad Condition &  Relative Residual after\\
      Vector  &              & Number    & 100 GMRES Iterations \\
      \hline
      $E$ & 10500 & 23400 &  0.29 \\
      $H$ & 10500 & 15500 &  0.13 \\
      $[E,H]$ & 21000 & 4400 & 0.87 \\
      \hline
    \end{tabular}
  \end{quote}


  % The first-order equation had the lowest condition number yet it
  % had the largest GMRES relative residual.  GMRES was run 100
  % iterations without restarting.  Restarting did not reduce the
  % residual.


\end{frame}





% -------------------------------------------------


\begin{frame} {Relative difference between some fields}

  The real parts of $E_y$ and $H_x$ field components computed by three
  different FDFD formulations.

  \begin{center}
    \includegraphics[scale=0.52]{images/smallProb_Ey.pdf}

    \includegraphics[scale=0.52]{images/smallProb_Hx.pdf}
  \end{center}

  The relative difference is on the order of 3\%. The mesh size is
  1/10 of the vacuum wavelength. Relative difference is the ratio of
  $\ell^2$ norms.


  % \begin{figure}[h!]
  %   \centering
  %   \includegraphics[width=0.5\textwidth]{images/smallProb_Ey.pdf}
  %   \\
  %   \includegraphics[width=0.5\textwidth]{images/smallProb_Hx.pdf}
  %   \caption{The real part of $E_y$ field component computed by
  %   three different FDFD formulations. The relative difference
  %   between E-field formulation and first-order $[E,H]$
  %   formulation
  %   is zero.  The relative difference between H-field formulation
  %   and first-order $[E,H]$ formulation is 2.9\%.}
  % \end{figure}


\end{frame}

% -------------------------------------------------


% -------------------------------------------------


% --------------------------------------------------------------------------------------


\begin{frame}{Iterative Domain Decomposition}
  \begin{itemize}
  \item Would like to solve, for domains $i=1,2$ with shared boundary
    $\Gamma$:
    \begin{align*}
      &\blu{i\mu_{r,i} H_i = \nabla \times E_i + f^h_i, \quad
        -i\epsilon_{r,i} E_i = \nabla \times H_i + f_i^e,} \quad
      \red{(\n \times E_i) = \lambda \text{ on } \Gamma }
    \end{align*}
    \pause
  \item $\red{\lambda}$ must guarantee
    \begin{align*}
      \red{(\n \times \curl E_1) = (\n \times \curl E_2), \; \text{on
        } \Gamma.}
    \end{align*}
    \pause
  \item Superposition: define $\blu{E_i^\lambda}$ as the response of
    the electric field to the interface data $\red{\lambda}$, and
    $\blu{E_i^f}$ as the reponse to the forcing terms.
  \item Define operators
    \begin{align*}
      \red{S\lambda} &\red{:= (\n \times \curl E_1^\lambda)|_\Gamma
        - (\n \times \curl E^\lambda_2)|_\Gamma,} \\
      \red{\chi} &\red{:= (\n \times \curl E_2^f)|_\Gamma - (\n \times
        \curl E_1^f)|_\Gamma.}
    \end{align*}
  \end{itemize}
  \begin{lemma}[Equivalence of \red{Steklov-Poincar\'{e}} system to TH
    Maxwell's]
    Suppose $\lambda$ satisifes $S\lambda = \chi$. Then the solution
    to the independent subdomain problems with Dirichlet data
    $\lambda$ at the interface solves the time-harmonic Maxwell's
    equations on $\Omega$.
  \end{lemma}
\end{frame}



\begin{frame}{Iterative Domain Decomposition}
  \begin{block}{Neumann-Neumann}
    Given $\red{\lambda^0}$, for each $k\geq 0$,
    \begin{enumerate}
    \item Find $\blu{[E^{n+1}, H^{n+1}]}$ by solving, for $i=1,2$,
      \begin{align*}
        \hspace{-.3cm}&\blu{i \mu_{r,i} H_i^{n+1} = \nabla \times
          E^{n+1}_i + f^h_i, \quad -i\epsilon_{r,i} E^{n+1}_i = \nabla
          \times H^{n+1}_i + f_i^e,} \quad \red{(\n \times E^{n+1}_i)
          = \lambda^n}.
      \end{align*}
    \item Then find $\blu{\Psi^{k+1}}$ by solving, for $i=1,2$,
      \begin{align*}
        &\blu{\nabla \times (\mu_{r,i}^{-1} \nabla \times
          \Psi_i^{n+1}) - \epsilon_{r,i} \Psi_i^{n+1} = 0,}
        \\
        &\red{(\n \times \curl \Psi_i^{n+1}) = [\n\times \curl
          E_i^{n+1}]|_\Gamma.}
      \end{align*}
    \item Update $\red{\lambda}$ by
      \begin{align*}
        \red{\lambda^{n+1} = \lambda^n - \theta[\Psi^{n+1}]_\Gamma.}
      \end{align*}
    \item Continue with (1) unless stopping criterium $\red{\|[\n
        \times \curl E]_\Gamma\|}$ holds.
    \end{enumerate}
  \end{block}
  \pause \vspace{-.1cm}
  \begin{block}{Preconditioned Richardson Scheme}
    \begin{align*}
      \red{\lambda^{n+1} = \lambda^n - \theta(S_1^{-1} +
        S_2^{-1})(S\lambda^n - \chi).}
    \end{align*}
    \vspace{-.3cm}
  \end{block}

\end{frame}

% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------

% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% ---------------------------------------------------------

\section{Conclusion}
\subsection{Conclusion}

\begin{frame}{Summary}
\begin{block}{Summary}
  \begin{itemize}
    \item Implemented 3d FDFD code for time-harmonic Maxwell's equations with a
      perfectly matched layer.
    \item Analyzed condition number and GMRES performance for 3 different FDFD
      formulations.
    \item Developed new domain decomposition algorithm. Analyzed performance.
  \end{itemize}
\end{block}

\begin{block}{Conclusions}
  \begin{itemize}
    \item EM scattering problem in optical lithography: computationally expensive.
    \item State-of-the-art iterative techniques, e.g. Krylov, Domain Decomposition,
      Multigrid, struggle to perform.
    \item Richardson scheme able to converge quickly for low frequency if high
      frequencies can be controlled.
    \item While memory intensive, the 1st order $E$-$H$ problem is significantly
      better conditioned.
  \end{itemize}
\end{block}

\begin{block}{Future Directions}
  \begin{itemize}
    \item Low frequency accuracy paramount, consider principal component analysis.
    \item Consider more advanced discretization techniques.
  \end{itemize}
\end{block}

\end{frame}


\end{document}
