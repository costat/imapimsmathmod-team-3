\beamer@endinputifotherversion {3.24pt}
\beamer@sectionintoc {1}{Motivation}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Motivation}{2}{0}{1}
\beamer@sectionintoc {2}{Model}{7}{0}{2}
\beamer@subsectionintoc {2}{1}{Time-Harmonic Maxwell's Equations}{7}{0}{2}
\beamer@sectionintoc {3}{Perfectly Matched Layer}{12}{0}{3}
\beamer@subsectionintoc {3}{1}{Perfectly Matched Layer}{12}{0}{3}
\beamer@sectionintoc {4}{Finite Difference Frequency Domain}{16}{0}{4}
\beamer@subsectionintoc {4}{1}{Finite Difference Frequency Domain}{16}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{31}{0}{5}
\beamer@subsectionintoc {5}{1}{Conclusion}{31}{0}{5}
