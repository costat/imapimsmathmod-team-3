\documentclass[11pt]{extarticle}
\usepackage{tcdef}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{framed,color}
\definecolor{shadecolor}{rgb}{.7,.7,.7}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}{Corollary}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{proposition}{Proposition}[section]

\begin{document}
\centerline{\Large{IMA \& PIMS Math Modeling Workshop}}
\vspace{.3cm}
\centerline{\large{Team 3 Final Report: Fast Calculation of Diffraction
  by Photomasks}}
\vspace{.3cm}
\centerline{Timothy Costa, John Cummings, Michael Jenkinson,}
\centerline{Yeon Eung Kim,
  Jose de Jesus Martinez, Nicole Olivares, Apo Sezginer}
\vspace{.5cm}
\hrule
\vspace{.5cm}

\tableofcontents

\section{Introduction}

This work is motivated by the problems
of manufacturing and quality control of integrated circuits. Integrated circuits
are manufactured by optical lithography.
This process involves several components,
first a photomask is designed, consisting of a layered glass plate
with a pattern etched into
the surface. Light is shown through this plate, and then focused to develop
a photoresist which sits on top of the circuit to be manufactured. The pattern
on the photoresist is then etched onto a silicon wafer by chemical
processising. In Figure \ref{fig:optlith1}
a schematic diagram of this process is presented.\\

\begin{figure}
  \includegraphics[width=\textwidth]{optlith.pdf}
  \caption{\label{fig:optlith1}Schematic diagram of optical lithography process.}
\end{figure}

Simulation of this process has three components. The propagation of a laser
pulse from the source to the photomask, the calculation of the near field
after passing through the photomask, and the propagation of the scattered field
from the photomask to the device. However, the first and third components of this
process are easily and efficiently modeled by Fourier Optics, while the scattering
problem, i.e. the calculation of the near-field after passing through the photomask,
is computationally intensive. Figure \ref{fig:optlith2} diagrams this procedure.\\

\begin{figure}
  \includegraphics[width=\textwidth]{optlith2.pdf}
  \caption{\label{fig:optlith2}Schematic diagram of of the components in simulating
    the optical lithography process.}
\end{figure}

We are interested in the efficient
calculation of the near-field as the light passes through the
photomask. Efficient simulation of the near-field is necessary for
quality control of the production of the photomask, as well as in the optimization
process of the design of photomasks for new chips. This computation is very intensive,
requiring on average 100,000 CPU hours for the design of a single integrated circuit.\\

The near-field in this problem is modeled by the time-harmonic Maxwell equations,
posed in a large 3-dimensional domain with heterogeneous coefficients varying
according to the layers and etching in the photomask. Simulation
of this problem is computationally intensive due to the size of the problem,
and additional difficulties arise in discontinuous material parameters at
layer interfaces.\\

\myplan{ finish intro when we know... }

\section{Model}
\subsection{Time-Harmonic Maxwell's Equations and Boundary Conditions}
The EM scattering problem in optical lithography
is modeled by the time-harmonic Maxwell's equations. We will
consider the model after normalization, which is detailed in the appendix.
These are given by,
\begin{align}
  \label{eq:hfield}
  i\mu_r H &= \nabla \times E + f^h, \\
  \label{eq:efield}
  -i\epsilon_r E &= \nabla \times H + f^e,
\end{align}
along with appropriate boundary conditions. Here $E$ denotes the electric field,
$H$ the magnetic field, $\epsilon_r$
is the relative electric permittivity, and $\mu_r$ is the relative
magnetic permittivity. We include forcing terms $f^h$ and $f^e$ to
model the incident field caused by a laser pulse before the scatterers (etched
regions) are introduced.\\

For the purposes of proof-of-concept simulations during this work,
we pose the problem of determining the solution of Equations \eqref{eq:hfield} and
\eqref{eq:efield} in a 3d computational domain that captures the essential features
of the photomask, with appropriate boundary conditions. The computational domain is
shown in Figure \ref{fig:pml}. The domain is layered
in the $z$-direction consisting of materials with different permittivities, and etching
creates changes in permittivity in the $x$- and $y$-directions. We assume the geometry
is such that the permittivity is constant on polygonal sections with edges in
coordinate directions. Thus the permittivities are given by diagonal matrices.
The photomask is assumed to be infinite and periodic in the $x$- and $y$-directions.
Thus, the computational domain consists of one period, and periodic boundary conditions
in the $x$- and $y$-directions. In the positive and negative $z$-directions,
we must simulate homogeneous media extending to infinity. When truncating the
domain, we require a treatment of these boundaries that does not introduce
nonphysical reflections of the outgoing scattered field back into the region
of interest. This may be achieved in one of a few ways. We accomplish
this by the use of a perfectly matched layer (PML) boundary condition.

\subsection{Perfectly Matched Layer Boundary Conditions}

A perfectly matched layer boundary condition is employed by padding
the computational domain along the non-reflecting boundary. For exmaple, in the
case of the unit square, we would simulate the problem on $[0-\eta,
1+\eta]^2$ for some
$\eta > 0$. For our geometry, the PML layer is described visually
in Figure \ref{fig:pml}.\\

The non-reflection condition is achieved by designing an
artificial material in the PML region that does not reflect waves. In
addition, the material is designed to very quickly damp the waves that enter into it such
that, by the time an outgoing wave reaches the exterior boundary of the PML layer,
it has decayed to be nearly $0$.
A homogeneous Dirichlet boundary condition may now
be used here since the only waves reaching it
(that is, the only waves that will be reflected back towards the region of interest)
are almost entirely attenuated, and cause very little error in the solution. \\

\begin{figure}
  \centering
  \includegraphics[width=.5\textwidth]{pml2.pdf}
  \caption{\label{fig:pml}Illustration of the Perfectly Matched Layer
    concept.}
\end{figure}

\section{Kirchhoff's Approximation}

In the late 1800s Gustav Kirchhoff studied light diffraction by an aperture
in an opaque screen. Kirchhoff devised an approximation of the near-field
that allows for a computation of $O(N)$ complexity. This efficiency
is obtained by ignoring the aperture next to the screen, and ignoring
the screen next to the aperture. Once the near-field is approximated
in a plane, it is easily propagated to the image plane using Green's Theorem.
Figure \ref{fig:kirchh} illustrates this idea.\\

\begin{figure}
  \centering
  \includegraphics[width=.5\textwidth]{kirchh.pdf}
  \caption{\label{fig:kirchh}Illustration of Kirchoff approximation.}
\end{figure}

Clearly this approximation ignores essential physics if one is greatly concerned
with accuracy. Thus various improvements have been made. However, these
improvements are still inaccurate, and the explicit computation
of the time-harmonic Maxwell's equations would provide a better solution.

\section{Finite Difference Frequency Domain}

\subsection{Forcing Term Expressed as Deviation from a Background}
The electromagnetic field $(E, H)$ is sourced by the electric and magnetic
current densites $J,M$. Frequently the original source of the EM
radiation is far from the computation domain. For example the laser
that illuminates a lithographic photomask is far from the photomask and
there are many optical components in between.
In such cases expressing the forcing function in terms of a
known incident wave is advantageous.  We introduce the concepts of background geometry,
materials and fields.  The background problem has three properties:
it is easy to solve; it has the same primary sources as in the original problem;
the background geometry and materials
agree with those in the original problem in most places.
The original problem is:
\begin{align}
&\nabla \times E = i \mu_r H  + M_{pri}\\
&\nabla \times H = -i \epsilon_r E + J_{pri} \\
\end{align}
where $\mu_r$ and $\epsilon_r$ are inhomogeneous and
possibly anisotropic. The background problem is:
\begin{align}
&\nabla \times E_{bg} = i \mu_{bg} H_{bg}  + M_{pri}\\
&\nabla \times H_{bg} = -i \epsilon_{bg} E_{bg} + J_{pri} \\
\end{align}
We difference the above equations to obtain:
\begin{align}
&\nabla \times(E- E_{bg}) = i \mu_r (H- H_{bg})  +  i ( \mu_r - \mu_{bg} ) H_{bg} \\
&\nabla \times (H- H_{bg}) = -i \epsilon_r (E-E_{bg})
  - i ( \epsilon_r - \epsilon_{bg} ) E_{bg} \\
\end{align}
We define the scattered field as the difference between the fields of the
original problem and the background fields:
\begin{align}
& E_{sca} = E - E_{bg}\\
& H_{sca} = H - H_{bg}\\
\end{align}
We define the induced magnetic and electric current densities:
\begin{align}
& M_{ind} =  i ( \mu_r - \mu_{bg} ) H_{bg}\\
& J_{ind} = - i ( \epsilon_r - \epsilon_{bg} ) E_{bg}\\
\end{align}
The induced currents exist only where the background materials differ from the materials
of the original problem.
Maxwell's equations for the scattered fields are:
\begin{align}
&\nabla \times E_{sca} = i \mu_r  H_{sca}  + M_{ind}\\
&\nabla \times H_{sca} = -i \epsilon_r E_{sca} + J_{ind} \\
\end{align}
The scattered field satisfies the original PDE but with a different forcing function.
The scattered fields are excited by the induced electric current density
$J_{ind}$ and the induced magnetic current density $M_{ind}$.

\subsection{Setting up FDFD Equations}
We discretize the time-harmonic Maxwell's equations:
\begin{align}
&\nabla \times E_{sca} = i \mu_r H_{sca}  + M_{ind}\\
&\nabla \times H_{sca} = -i \epsilon_r E_{sca} + J_{ind} \\
\end{align}
 using Yee's grid
[Kane S. Yee, "Numerical Solution of Initial Boundary Value
Problems Involving Maxwell's Equations in Isotropic Media," IEEE Trans.
Anntennas and Propagation, Vol. AP-14, No. 3, pp. 302-307, May 1966].
The discretized equations form a linear system of equations. There are at
least three ways to  set up the linear system of equations.  We can eliminate
the magnetic or the electric field from the vector of unknowns.  Then
we have $3 N_x N_y N_z$ complex, scalar unknowns where $N_x N_y N_z$ is the
number of Yee cells in the computation domain.  Alternatively, we can
have $6 N_x N_y N_z$ complex, scalar unknowns which are the electric and magnetic
vector field components; and solve a first-order difference equation.

\subsection{Electric Field FDFD Equation}

We eliminate the magnetic field and  set up a linear system of equations for
the electric field.  The action of the coefficient matrix on the unknown
vector E is calculated as follows:
$$
A_e \; E_{sca} = -i \nabla_h \times \left(  \mu_r^{-1}
\nabla_e \times E_{sca}  \right)+ i \epsilon_r E_{sca}
$$
The finite-difference curl operators $\nabla_e$ and $\nabla_h$ are different because
they act on staggered grids.  These operators particularly differ at the boundary
of the computation domain.
The forcing term for the electric field FDFD equations is:
$$
f_e = J_{ind} - i \nabla_h \times \left(  \mu_r^{-1} M_{ind}  \right)
$$
We obtain the electric field by solving the equation:
$$
A_e \; E_{sca} = f_e
$$
and calculate the total field: $  E =  E_{sca} + E_{bg} $.  The magnetic field
is obtained from the electric field:
$$
 H =   -i \mu_r^{-1}  \nabla_e \times E
$$
In the last equation we assumed that the magnetic current source of the
original problem $ M_{ori}$ is zero in the computation domain.

\subsection{Magnetic Field FDFD Equation}
We eliminate the electric field to set up a linear system of equations for the
magnetic field.
The action of the coefficient matrix on the unknown vector H is calculated as follows:
$$
A_h \; H_{sca} = i \nabla_e \times
\left( \epsilon_r^{-1} \nabla_h \times H_{sca} \right) - i \mu_r H_{sca}
$$
The forcing term for the magnetic field FDFD equations is:
$$
f_h = M_{ind} +i \nabla_e \times \left( \epsilon^{-1} J_{ind} \right)
$$
We obtain the magnetic field by solving the equation:
$$
A_h \; H_{sca} = f_h
$$
and calculate the total field: $  H =  H_{sca} + H_{bg}  $
 The electric field is obtained from the magnetic field:
$$
E =  i \epsilon_r^{-1} \nabla_h \times H
$$
In the last equation we assumed that the electric current source of the original
problem $J_{ori}$ is zero in the computation domain.

\subsection{First-Order FDFD Equation}
In this case we choose the unknown vector $[E,H]$. We don't eliminate either
$E$ or $H$.  This results in an equation that has only first order derivatives.
We solve the linear equation:
$$
A \quad \left[ \begin{array}{c} E \\ H \end{array} \right]  = f
$$
The matrix-vector product is calculated as:
$$
A \quad \left[ \begin{array}{c} E \\ H \end{array} \right] =
\begin{bmatrix}
\nabla_e \times E & -i \mu_r H \\
-i \epsilon_r E & \nabla_h  \times H \\
\end{bmatrix}
$$
The forcing term is:
$$
f =  \left[  \begin{array}{c}
 M_{ind} \\
 J_{ind}  \\
 \end{array} \right]
$$

\subsection{Condition Number of Equations}

We initially coded the linear equations without storing the coefficient matrices.  We
evaluated the matrix-product by evaluating finite-difference curls according to
Yee's grid.  We found that GMRES reduced the residual slowly and stalled.  To
investigate the cause, we formed the sparse matrices for a small problem (
1 wavelength x 1 wavelength x 3.5 wavelength ) and solved the linear
equations with MATLAB back slash operation and estimated the condition numbers. We
observed the following performance:

\begin{quote}
\begin{tabular}{  | c | r | r | r |  }
\hline
Unknown Vector&  \# Unknowns & Condition  &  Relative Residual after\\
                          &                        &  Number   & 100 GMRES Iterations \\
\hline
$E$ & 10500 & 23400 &  0.29 \\
$H$ & 10500 & 15500 &  0.13 \\
$[E,H]$ & 31500 & 4400 & 0.87 \\
\hline
\end{tabular}
\end{quote}
The first-order equation had the lowest condition number yet it had the
largest GMRES relative residual.  GMRES was run 100 iterations without restarting.
Restarting did not reduce the residual.

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{spy_FDFD_E.jpg}
  \caption{Coefficient matrix for a 3-D FDFD E-field equation}
\end{figure}

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{spy_FDFD_H.jpg}
  \caption{Coefficient matrix for a 3-D FDFD H-field equation}
\end{figure}

\begin{figure}[h!]
  \centering
    \includegraphics[width=0.5\textwidth]{spy_FDFD_EH.jpg}
  \caption{Coefficient matrix for a 3-D, first-order FDFD equation}
\end{figure}

The fields computed by the three formulations agree when the linear equations
are solved without using an iterative solver.  This can be seen in the figures below.
\begin{figure}[h!]
  \centering
\vspace*{-2 cm}
    \includegraphics[width=\textwidth]{smallProb_Ey.jpg}
    \includegraphics[width=\textwidth]{smallProb_Hx.jpg}
  \caption{The real parts of $E_y$ and $H_x$  field components computed by
three different FDFD formulations.  The relative difference is on the order
of 3\%.  The mesh size is 1/10 of the vacuum wavelength.  Relative difference is
the ratio of $\ell_2$ norms.}
\end{figure}

\section{Domain Decomposition}

Domain decomposition (DDM)
techniques for partial differential equations attempt to solve independent subdomain
problems, rather than the problem as originally posed on the full domain.
These methods have been designed with two primary purposes in mind. Originally,
domain decomposition was developed as a methodology of parallelism for large, out-of-core
problems. There exist a library of literature in this context from
mathematicians, computer scientists, and computational scientists over decades
that is too large to appropriately summarize here. \\

In more recent years DDM has received growing interest as a tool
for handing multi-physics problems where material parameters or physical models
vary between physical subdomains, as well as for problems with multi-scale
phenomena characterized by interfaces. In this context DDM has been successfully
applied to problems of fluid flow \cite{girault2005discontinuous},
the coupling of Darcy and Stokes flows \cite{discacciati2003analysis},
\cite{discacciati2007robin}, as well
as in problems of electrostatics \cite{haii}, \cite{haii2}, among other applications.
\\

The physical problem we are interested in computing is composed of layered materials
separated by interfaces. This combined with the size of the problem
suggests the use of domain decomposition techniques. \\

In recent years there has been a growing interest in domain decomposition
for the time-harmonic Maxwell's equations. Methods of iterative substructuring
type \cite{alonso1997domain}, \cite{alonso1999optimal} have been developed
using complicated Robin type transmission conditions, as well as
Schwarz
type preconditioners \cite{lietal}. In this work, we develop
an iterative substructuring algorithm of Neumann-Neumann type.
Neumann-Neumann methods
have the advantage of employing non-overlapping subdomains and obtaining convergence
independent of the resolution. That is, the Neumann-Neumann method
has $O(1)$ complexity. Thus the computational complexity of a computational paradigm
employing Neumann-Neumann methods is determined entirely by the complexity of
the subdomain solvers, while simultaneously reducing the size of $N$ for these
solvers and allowing parallelism. Addtionally,
due to their popularity, Neumann-Neumann type methods
have been extensively researched, and it is well-understood how to
obtain good scaling properties with many domains
through the use of some sort of global correction, e.g. a Balancing Neumann-Neuamnn
method. Neumann-Neumann type methods have also been developed
for unusual interface behavior in problems with multiscale phenomena
characterized by interfaces. We note, however, that the method developed here
demonstrated extraordinarily slow convergence in a 3d simulation
using finite-difference frequency domain discretizations for the
subdomain problems. A literature review
\myplan{ cite the iterative methods $=$ bad paper on fdfd maxwells paper }
suggests that, in general, iterative methods, e.g. Krylov methods, domain decomposition
methods, and multigrid, have tremendous difficulty with rate of convergence
on the time-harmonic Maxwell system. Given that
these methods encompass the state-of-the-art in numerical
partial differential equations, this suggests the need for more research
and the development of methods to accelerate convergence for domain decomposition
schemes and other iterative methods applied to the time-harmonid Maxwell system. \\

Iterative substructuring algorithms of Neumann-Neumann type work by first
defining a multi-domain formulation of a model partial differential equation problem.
An equation posed on the interface is then developed whose solution will guarantee
that the multidomain formulation is consistent with the original partial differential
equation. An iterative procedure is then performed to solve the interface equation
and the independent subdomain problems.

\subsection{Two-domain time harmonic Maxwell's equations}

In this section we develop the two-domain model. This is trivially extended
to the many subdomain model by requiring the transmission conditions obtained here
hold between all adjacent subdomains.\\

Rather than solving the problem on the domain $\Omega$, we subdivide the domain
into two components $\Omega_1$ and $\Omega_2$ such that $\cap_i \Omega_i = \emptyset$,
$\cup_i \overline{\Omega_i} = \overline{\Omega}$, and
$\cap_i \overline{\Omega_i} := \Gamma$ is an 2 dimensional manifold.\\

Then we consider the two-domain formulation of the time harmonic Maxwell
system given by,
\begin{align}
  \label{eq:multieq}
  \text{Subdomain problems:  }
  &\left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E_i + f^h, & \text{in } \Omega_i, i=1,2, \\
    -i\epsilon_{r,i} E_i = \nabla \times H_i + f^e, & \text{in }
      \Omega_i, i=1,2.
  \end{array}\right.\\
  \label{eq:trans}
  \text{Transmission conditions: }
  &\left\lbrace \begin{array}{l}
    (\n \times E_1)|_\Gamma = (\n \times E_2)|_\Gamma, \\
    (\n \times \curl E_1)|_\Gamma = (\n \times \curl E_2)|_\Gamma.
  \end{array}\right.
\end{align}
The first of these transmission conditions ensures that the tangential
component of the electric field is continuous across the interface $\Gamma$, while
the second of these transmission conditions ensures that the tangential
component of the magnetic field is continuous across the interface $\Gamma$.\\

It is a well-known result that these transmission conditions ensure that a solution to
the two-domain problem solves the single domain problem \cite{alonso1999optimal}.

\subsection{Independent Subdomain Problems \& Steklov-Poincar\'{e} Interface
Equation}

Clearly the problem given by \eqref{eq:multieq}-\eqref{eq:trans} does not
consist of independent subdomain problems, as the transmission conditions
\eqref{eq:trans} couple the solutions of the two subdomain problems. Thus
we would like to solve instead the decoupled problems,
\begin{align}
  \left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E_i + f^h & \text{in } \Omega_i, \\
    -i\epsilon_{r,i} E_i = \nabla \times H_i + f^e & \text{in }
      \Omega_i, \\
    (\n^i \times E_i)|_\Gamma = \lambda,
  \end{array}\right.
\end{align}
for some value $\lambda$ to be determined. For any value of $\lambda$, we satisfy the
first transmission condition
\begin{align}
  (\n \times E_1)|_\Gamma = (\n \times E_2)|_\Gamma,
\end{align}
however, we have no reason to expect that this problem ensures continuity
of the tangential component of the magnetic field. Thus we must develop
a tool for finding the correct $\lambda$ such that
\begin{align}
  (\n \times \curl E_1)|_\Gamma = (\n \times \curl E_2)|_\Gamma
\end{align}
is satisfied.\\

Applying superposition, we define $E_i^\lambda$ and $E_i^f$ to be the responses
to the interface data $\lambda$, or forcing term $f$. Thus, these terms solve
the equations,
\begin{align}
  &\left\lbrace
  \begin{array}{lr}
    \nabla \times(\mu_{r,i}^{-1} \nabla \times E_i^\lambda)
    - \epsilon_{r,i} E_i^\lambda = 0
    & \text{in } \Omega_i, \\
    (\n^i \times E_i^\lambda) = \lambda & \text{on } \Gamma.
  \end{array}
  \right.\\
  &\left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E^f_i + f^h & \text{in } \Omega_i, \\
    -i \epsilon_{r,i} E^f_i = \nabla \times H_i + f^e & \text{in }
      \Omega_i, \\
    (\n^i \times E_i)|_\Gamma = 0,
  \end{array}\right.
\end{align}
respectively.\\

Then we define the operators
\begin{align}
  S\lambda &:= (\n \times \curl E_2^\lambda)|_\Gamma
    - (\n \times \curl E_1^\lambda)|_\Gamma,\\
  \chi &:= (\n \times \curl E_1^f)|_\Gamma - (\n \times \curl E_2^f)|_\Gamma,
\end{align}
and seek to solve
\begin{align}
  S\lambda = \chi.
\end{align}

It is an exercise in algebra to see that if $\lambda$ satisfies this
Steklov-Poincar\'{e} equation, then a solution to the independent subdomain problems
will solve the two-domain formulation of the time harmonic Maxwell system, and thus
solves the original problem.

\subsection{Algorithm nnMaxwell}

An algorithm of the Neumann-Neumann type for this problem
has the following form:\\

Given $\lambda^0$,
\begin{enumerate}
  \item Solve
    \begin{align}
      \left\lbrace \begin{array}{lr}
        i\mu_{r,i} H^{n+1}_i = \nabla \times E^{n+1}_i
         + f^h & \text{in } \Omega_i, \\
        -i\epsilon_{r,i}E^{n+1}_i
        = \nabla \times H^{n+1}_i + f^e
        & \text{in } \Omega_i, \\
        (\n^i \times E^{n+1}_i) = \lambda^n & \text{on } \Gamma.
      \end{array}\right.
    \end{align}
  \item Then solve the auxiliary problem,
    \begin{align}
      \left\lbrace
      \begin{array}{lr}
        \nabla \times(\mu_{r,i}^{-1} \nabla \times \Psi^{n+1}_i) - \epsilon_{r,i}
          \Psi^{n+1}_i = 0 & \text{in } \Omega_i, \\
        (\n^i \times \curl \Psi^{n+1}_i) = [\n\times \curl E_i^{n+1}]|_\Gamma
        & \text{on } \Gamma.
      \end{array}
    \right.
    \end{align}
  \item Then update $\lambda$,
  \begin{align}
    \lambda^{n+1} = \lambda^n - \theta [\Psi^{n+1}]_\Gamma.
  \end{align}
  \item Check stopping criteria, e.g. $\|[(\n \times \curl E)]_\Gamma \|$,
    return to (1) if criteria is not met, else exit..
\end{enumerate}

\subsection{Richardson Scheme}

It is helpful to notice that
this algorithm is a pre-conditioned Richardson scheme for the equation
$S\lambda = 0$. To see this, notice,
\begin{align}
  \Psi_1^{n+1} = S_1^{-1}(S\lambda^n - \chi), \\
  \Psi_2^{n+1} = -S_2^{-1}(S\lambda^n - \chi),
\end{align}
so that we can write the $\lambda$ update step as,
\begin{align}
  \lambda^{n+1} = \lambda^n - \theta(S_1^{-1} + S_2^{-1}) (S\lambda^n - \chi).
\end{align}

The question of convergence of this scheme will depend on properties of the operator
$S$, and will be explored in the weak setting.

\section{Conclusion}

\appendix
\section{Normalizing Maxwell's Equations}

Time-harmonic form of Maxwell's Equatiosn are:
\begin{align}
  \label{eq:curle}
&\nabla \times E = i \omega \mu H  + M\\
  \label{eq:curlh}
&\nabla \times H = -i \omega \epsilon E + J \\
\end{align}
We obtain these equations from the time-dependent Maxwell's equations
by using the $e^{-i\omega t}$ time dependence, and factoring out the time dependence.
We will now normalize these equations and render them dimensionless.
To this end we first define
the relative electric permittivity $\epsilon_r$ and magnetic permeability $\mu_r$:
\begin{align}
  \label{eq:releps}
&\epsilon_r =\epsilon / \epsilon_0\\
  \label{eq:curlh}
&\mu_r = \mu / \mu_0\\
&\epsilon_0 = 8.85...\times 10^{-12} \quad \mbox{Farad/m}\\
&\mu_0 = 4\pi \times 10^{-7} \quad \mbox{Henry/m}\\
\end{align}
$\epsilon_0$ and $\mu_0$ are electric permittivity and
magnetic permeability of vacuum, respectively.
Next, we redefine the magnetic field as the
magnetic field times the wave impedance of free-space: $$
\tilde H = H \sqrt{\mu_0 / \epsilon_0 }
$$
The units of $\tilde H$ and $E$ are the same. We redefine space coordinates as:
\begin{align}
&\tilde x = k_0 x \\
&k_0 = \omega \sqrt{ \mu_0 \epsilon_0 }\\
\end{align}
Then Maxwell's equtions become:
\begin{align}
&\tilde\nabla \times E = i \mu_r \tilde H  + \tilde M\\
  \label{eq:curlh}
&\tilde\nabla \times \tilde H = -i \epsilon_r E + \tilde J \\
\end{align}
We redefined the magnetic and electric current densities as:
\begin{align}
&\tilde M = M/k_0\\
&\tilde J = J  \sqrt{\mu_0 / \epsilon_0 } / k_0 \\
\end{align}
We drop all tilde symbols and obtain:
\begin{align}
&\nabla \times E = i \mu_r H  + M\\
&\nabla \times H = -i \epsilon_r E + J \\
\end{align}
In the above equation, the units of $E,H,M$ and $J$ are the same (Volt/m).
Normalizing them by an arbitrary, constant electric field such as 1 V/m
renders them dimensionless. Therefore all quantities
in the above normalized Maxwell's equations, including distance, are dimensionless.

\nocite{*}
\bibliographystyle{plain}
\bibliography{team3_final_report}

\end{document}
