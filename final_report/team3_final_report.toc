\contentsline {section}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {2}Model}{4}
\contentsline {subsection}{\numberline {2.1}Time-Harmonic Maxwell's Equations and Boundary Conditions}{4}
\contentsline {subsection}{\numberline {2.2}Perfectly Matched Layer}{4}
\contentsline {section}{\numberline {3}Kirchhoff's Approximation}{6}
\contentsline {section}{\numberline {4}Finite Difference Frequency Domain}{6}
\contentsline {subsection}{\numberline {4.1}Forcing Term Expressed as Deviation from a Background}{6}
\contentsline {subsection}{\numberline {4.2}Setting up FDFD Equations}{7}
\contentsline {subsection}{\numberline {4.3}Electric Field FDFD Equation}{8}
\contentsline {subsection}{\numberline {4.4}Magnetic Field FDFD Equation}{8}
\contentsline {subsection}{\numberline {4.5}First-Order FDFD Equation}{9}
\contentsline {subsection}{\numberline {4.6}Performance of the Perfectly Matched Layer}{9}
\contentsline {subsection}{\numberline {4.7}Condition Number of Equations}{9}
\contentsline {section}{\numberline {5}Domain Decomposition}{11}
\contentsline {subsection}{\numberline {5.1}Two-domain time harmonic Maxwell's equations}{12}
\contentsline {subsection}{\numberline {5.2}Independent Subdomain Problems \& Steklov-Poincar\'{e} Interface Equation}{13}
\contentsline {subsection}{\numberline {5.3}Algorithm nnMaxwell}{14}
\contentsline {subsection}{\numberline {5.4}Richardson Scheme}{14}
\contentsline {subsubsection}{\numberline {5.4.1}Difficulties with Convergence}{14}
\contentsline {section}{\numberline {6}Conclusion}{16}
\contentsline {section}{\numberline {A}Normalizing Maxwell's Equations}{17}
