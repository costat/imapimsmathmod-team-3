\documentclass[10pt]{extarticle}
\usepackage{tcdef}
\usepackage{graphicx}
\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{framed,color}
\definecolor{shadecolor}{rgb}{.7,.7,.7}
\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}{Corollary}[section]
\newtheorem{definition}{Definition}[section]
\newtheorem{proposition}{Proposition}[section]

\begin{document}
\hfill{\Large{Timothy Costa}}\\
\mbox{ } \hfill{\large{\today}}\\
\vspace{1cm}\\
\centerline{\Large{IMA \& PIMS Math Modeling Workshop}} \\
\centerline{\large{Team 3: Notes on Domain Decomposition for Maxwell's Equations}}
\vspace{.5cm}\\
\hrule
\vspace{.5cm}

\section{Model and Multi-domain Statement}

We are interested in simulating the time-harmonic Maxwell's equations, given
by
\begin{align}
  \label{eq:hfield}
  &i\mu_r H = \nabla \times E + f^h, \\
  \label{eq:efield}
  &-i\epsilon_r E = \nabla \times H + f^e.
\end{align}

Boundary conditions will be provided later, as they become understood...
The terms $f^h$ and $f^e$ are included to allow for a forcing term, should
we decide to use that method to include the incident field.\\

We pose this problem in a domain $\Omega \subset \mathbb{R}^3$ that is 'nice enough'
(to be defined later).\\

The geometry of the problem is naturally divided into material subdomains. This
combined with the interest in computational efficiency suggests the use of
domain decomposition techniques. These notes will attempt to develop a
Steklov-Poincar\'{e} type operator for the this problem.\\

To begin we subdivide the domain $\Omega$ into two subdomains $\Omega_i$, $i=1,2$
such that $\Omega = \cup_i \Omega_i$, $\cap_i \Omega_i = \emptyset$,
and $\overline{\Omega}_1\cap \overline{\Omega}_2 := \Gamma$ is a 2 dimensional
manifold.
In \myplan{cite Alonso and Valli} it is shown that problem
\eqref{eq:hfield}-\eqref{eq:efield} has the equivalent 2-domain formulation,
\begin{align}
  \label{eq:multieq}
  \text{Subdomain problems:  }
  \left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E_i + f^h, & \text{in } \Omega_i, i=1,2, \\
    -i\epsilon_{r,i} E_i = \nabla \times H_i + f^e, & \text{in } \Omega_i,
    i=1,2.
  \end{array}\right.
\end{align}
\begin{align}
  \label{eq:trans}
  \text{Transmission conditions: }
  \left\lbrace \begin{array}{l}
    (n\times E_1)|_\Gamma = (n \times E_2)|_\Gamma, \\
    (n\times \curl E_1)|_\Gamma = (n \times \curl E_2)|_\Gamma.
  \end{array}\right.
\end{align}

Our goal will be to develop domain decomposition methods such that
we can solve the subdomain problems indepedently, in parallel,
and then extend the 2-domain setup
to arbitrary domains, possibly by employing coarse space correction.
Two possible approaches are: (1) develop a Steklov-Poincar\'{e} Interface
Equation and develop a Neumann-Neumann type iterative substructuring method,
and (2) employ a Schwarz type algorithm. These notes work on (1). (2) will
be explored time permitting.

\section{Steklov-Poincar\'{e} Interface Equation}

Rather than the problem with transmission conditions \eqref{eq:multieq}-\eqref{eq:trans}
we would like to solve independent boundary value problems on subdomains $\Omega_1$ and
$\Omega_2$. To this end we consider the problem,
\begin{align}
  \left\lbrace \begin{array}{lr}
    i\mu_{r,i} H_i = \nabla \times E_i + f^h & \text{in } \Omega_i, \\
    -i\epsilon_{r,i} E_i = \nabla \times H_i + f^e & \text{in } \Omega_i, \\
    (\n \times E_i)|_\Gamma = \lambda,
  \end{array}\right.
\end{align}
for some value $\lambda$ to be determined. These are two independent subdomain
problems.\\

Clearly for any choice of $\lambda$ we satisfy the first of the two transmission
conditions in \eqref{eq:trans}, however we have no reason to expect that
\begin{align}
  \label{eq:trans2}
  (n\times \curl E_1)|_\Gamma = (n\times \curl E_2)|_\Gamma.
\end{align}

The essence of iterative substructuring methods is to develop an equation posed
on the interface, called the Steklov-Poincar\'{e} Interface Equation whose
solution will provide the correct $\lambda$ to guarantee \eqref{eq:trans2}
holds. To develop the SP equation, we first apply superposition to the
solution of the independent subdomain problems.\\

We denote by $E^\lambda_i$ the continuous extension of interface data $\lambda$
into subdomain $i$ obtained by solving the 2nd order curl curl equation for $E$
with no forcing terms,
\begin{align}
  \left\lbrace
  \begin{array}{lr}
    \nabla \times(\mu_{r,i}^{-1} \nabla \times E_i^\lambda)
      - \epsilon_{r,i} E_i^\lambda = 0
    & \text{in } \Omega_i, \\
    (n \times E_i^\lambda) = \lambda & \text{on } \Gamma.
  \end{array}
  \right.
\end{align}
Then we define $E_i^f$ to be the response of the solution to the 2nd order
problem to the forcing terms $f^e$ and $f^h$.

We then define,
\begin{align}
  S\lambda &:= (\n \times \curl E_2^\lambda)|_\Gamma
    - (\n \times \curl E_1^\lambda)|_\Gamma,\\
  \chi &:= (\n \times \curl E_1^f)|_\Gamma - (\n \times \curl E_2^f)|_\Gamma,
\end{align}
and we seek a $\lambda$ such that
\begin{align}
  S\lambda = \chi.
\end{align}

\section{A Neumann-Neumann Method}

An algorithm of the Neumann-Neumann type for this problem
has the following form:\\

Given $\lambda^0$,
\begin{enumerate}
  \item Solve
    \begin{align}
      \left\lbrace \begin{array}{lr}
        i\mu_{r,i} H^{n+1}_i = \nabla \times E^{n+1}_i
         + f^h & \text{in } \Omega_i, \\
        -i\epsilon_{r,i} E^{n+1}_i
        = \nabla \times H^{n+1}_i + f^e
        & \text{in } \Omega_i, \\
        (\n \times E^{n+1}_i) = \lambda^n & \text{on } \Gamma.
      \end{array}\right.
    \end{align}
  \item Then solve the auxiliary problem,
    \begin{align}
      \left\lbrace
      \begin{array}{lr}
        \nabla \times(\mu_{r,i}^{-1} \nabla \times \Psi^{n+1}_i) - \epsilon_{r,i}
          \Psi^{n+1}_i = 0 & \text{in } \Omega_i, \\
        (\n \times \curl \Psi^{n+1}_i) = [\n\times \curl E_i^{n+1}]|_\Gamma
        & \text{on } \Gamma.
      \end{array}
    \right.
    \end{align}
  \item Then update $\lambda$,
  \begin{align}
    \lambda^{n+1} = \lambda^n - \theta [\Psi^{n+1}]_\Gamma.
  \end{align}
  \item Check stopping criteria, e.g. $\|[(\n \times \curl E)]_\Gamma \|$,
    return to (1) or exit.
\end{enumerate}

We note that this is a pre-conditioned Richardson scheme for the equation
$S\lambda = 0$. To see this, notice,
\begin{align}
  \Psi_1^{n+1} = S_1^{-1}(S\lambda^n - \chi), \\
  \Psi_2^{n+1} = -S_2^{-1}(S\lambda^n - \chi),
\end{align}
so that we can write the $\lambda$ update step as,
\begin{align}
  \lambda^{n+1} = \lambda^n - \theta(S_1^{-1} + S_2^{-1}) (S\lambda^n - \chi).
\end{align}

The question of convergence of this scheme will depend on properties of the operator
$S$, and will be explored in the weak setting.

%\section{A Schwarz Method}

%In \myplan{cite Li et al}, a Schwarz type domain decomposition method is applied
%to the time harmonic Maxwell system. They results given in this paper
%are promising, with large reductions in computational time
%with increase in number of subdomains. This method proceeds iteratively, where
%the $(n+1)$-th iterate is defined from the $n$-th iterate by solving the subdomain
%problems
%\begin{align}
%  \left\lbrace \begin{array}{lr}
%    H_1^{n+1} + \frac{i}{k_{0,1}} \nabla \times E_1^{n+1} = 0 & \text{in } \Omega_1, \\
%    E_1^{n+1} - \frac{i}{k_{0,1} \epsilon_{r,1}} \nabla \times H_1^{n+1} = 0 &
%      \text{in } \Omega_1, \\
%    \mathcal{B}_{\n^1}(E_1^{n+1}, H_1^{n+1}) = \mathcal{B}_{\n^2}(E_2^n, H_2^n), & \\
%    \text{+ appropriate boundary conditions }.
%  \end{array}\right.
%\end{align}
%\begin{align}
%  \left\lbrace \begin{array}{lr}
%    H_2^{n+1} + \frac{i}{k_{0,2}} \nabla \times E_2^{n+1} = 0 & \text{in } \Omega_2, \\
%    E_2^{n+1} - \frac{i}{k_{0,2} \epsilon_{r,2}} \nabla \times H_2^{n+1} = 0 &
%      \text{in } \Omega_2, \\
%    \mathcal{B}_{\n^2}(E_2^{n+1}, H_2^{n+1}) = \mathcal{B}_{\n^1}(E_1^n, H_1^n), &\\
%    \text{+ appropriate boundary conditions }.
%  \end{array}\right.
%\end{align}
%Here $\mathcal{B}_\n(E,H)$ denotes a tranmission operator, which for the classical
%Schwarz method for the Maxwell system reads,
%\begin{align}
%  \mathcal{B}_\n(E,H) = \n \times \frac{E}{Z_r} + \n \times (\n \times H) =
%    \n \times \frac{E}{Z_r} - H^t
%\end{align}
%where $Z_r$ is the impedence.
%In the paper, the authors also employ a Krylov acceleration technique to this
%iterative procedure.
%
%\section{The Many Subdomain Problem}
%
%We can easily extend the statement of the 2 domain problem to $N$ subdomains.
%\begin{align}
%  \label{eq:multieq}
%  \text{Subdomain problems:  }
%  \left\lbrace \begin{array}{lr}
%    H_i = \frac{-i}{k_{0,i}} \nabla \times E_i, & \text{in } \Omega_i, i\in
%      \lbrace 1,\hdots,N\rbrace \\
%    E_i = \frac{i}{k_{0,i} \epsilon_{r,i}} \nabla \times H_i, & \text{in } \Omega_i,
%      i\in \lbrace 1,\hdots,N\rbrace.
%  \end{array}\right.
%\end{align}
%\begin{align}
%  \label{eq:trans}
%  \text{Transmission conditions: }
%  \left\lbrace \begin{array}{l}
%    (n^i\times E_i)|_\Gamma = (n^i \times E_j)|_\Gamma, \\
%    (n^i\times \curl E_i)|_\Gamma = (n^i \times \curl E_j)|_\Gamma
%  \end{array}\right.
%\end{align}
%for each $i\not= j$ such that $\partial \Omega_i \cap \partial \Omega_j \not= \emptyset$.

\section{Neumann-Neumann Convergence Analysis}

In the following the space $H^s(U)$, $s\in \mathbb{R}$ defines the usual Sobolev space,
with norm given by $\|\cdot \|_{s,U}$. Here $U$ is an arbitrary subset
$U\in \mathbb{R}^d$ for $d\in \lbrace 2,3 \rbrace$. We denote the duality pairing
between the spaces $H^s(U)$ and $H^{-s}(U)$ by $\langle \cdot, \cdot \rangle_{s,U}$.
Additionally we denote by $H(\text{curl}; U)$ and $H(\text{div}; U)$ the
set of real or complex functions $u\in (L^2(U))^3$ such that $\curl u\in (L^2(U))^3$
and $\nabla \cdot u \in L^2(U)$, respectively.\\

We will
need to define the tangential divergence of a tangential vector field $\mu$. Let
$\mu\in (H^{-1/2}(\partial \Omega))^3$ with $(\mu \cdot \n)|_{\partial \Omega} = 0$.
We define the tangential divergence $\text{div}_\tau \mu \in H^{-3/2}(\partial \Omega)$
by
\begin{align}
  \langle \text{div}_\tau \mu, \eta \rangle_{3/2, \partial \Omega} =
    -\langle \mu, (\nabla \mathcal{R}_2\eta)|_{\partial \Omega}\rangle_{1/2,
    \partial \Omega}, \quad \forall \eta \in H^{3/2}(\partial \Omega),
\end{align}
where $\mathcal{R}_2$ denotes any continuous extension operator from $H^{3/2}
(\partial \Omega)$ into $H^2(\Omega)$.\\

Now we introduce the Hilbert space $X_{\partial \Omega}$ and $X_\Sigma$, where
$\Sigma \subset \partial \Omega$ denotes a nonempty subset of the boundary.
\begin{align}
  X_{\partial \Omega} &:= \lbrace \mu \in (H^{-1/2}(\partial \Omega))^3\, |\,
    (\mu \cdot \n)|_{\partial \Omega} = 0 \text{ and } \text{div}_\tau \mu
    \in H^{-1/2}(\partial \Omega)\rbrace,\\
  X_\Sigma &:= \lbrace \mu \in (H^{-1/2}(\Sigma))^3 \, | \, (\mu \cdot \n)|_{\Sigma}
  = 0 \text{ and } \text{div}_\tau \tilde{\mu} \in H^{-1/2}(\partial \Omega)
    \rbrace,
\end{align}
where $\tilde{\mu}$ denotes the extension by $0$ on $\partial \Omega \setminus \Sigma$.
We endow these spaces with norms,
\begin{align}
  \|\mu\|_{X_{\partial \Omega}} &:= \|\mu\|_{-1/2, \partial \Omega}
    + \|\text{div}_\tau \mu\|_{-1/2, \partial \Omega},\\
  \|\mu\|_{X_\Sigma} &:= \|\mu\|_{-1/2, \Sigma}
    + \|\text{div}_\tau \tilde{\mu}\|_{-1/2, \partial \Omega}.
\end{align}

In the work of \myplan{cite Alonso and Valli} it is proven that if $\Omega \in C^{1,1}$
or $\Omega$ is a convex polyhedron, the space $X_{\partial \Omega}$ is the space
of tangential traces of $H(\text{curl}; \Omega)$, and $X_\Sigma$ is the space
of tangential traces of
\begin{align}
  H_{\partial \Omega\setminus \Sigma}(\text{curl}; \Omega) := \lbrace v\in
    H(\text{curl}; \Omega)\, | \, (\n \times v)|_{\partial \Omega \setminus \Sigma} = 0
    \rbrace.
\end{align}
Additionally it was shown that there exist two linear continuous operators
\begin{align}
  \mathcal{R}_{\partial \Omega}&: X_{\partial \Omega} \to H(\text{curl}; \Omega),\\
  \mathcal{R}_\Sigma &: X_\Sigma \to H_{\partial \Omega \setminus \Sigma}
    (\text{curl}; \Omega)
\end{align}
satisfying
\begin{align}
  (\n \times \mathcal{R}_{\partial \Omega}\mu)|_{\partial \Omega} = \mu, \quad
  (\n \times \mathcal{R}_\Sigma \mu)|_\Sigma = \mu
\end{align}
for all $\mu \in X_{\partial \Omega}$ or $\mu \in X_\Sigma$.\\

Our first task will be to develop a weak form of $S\lambda = \chi$ such that
$\langle S\cdot, \cdot \rangle : X_\Gamma \to X_\Gamma'$, as well as to represent
$\chi \in X_\Gamma'$.\\

Define the bilinar forms
\begin{align}
  a_i(u,v) &:= \int_{\Omega_i} \curl u \cdot \curl v,\\
  (u,v)_i &:= \int_{\Omega_i} u \cdot v.
\end{align}

We denote by $\mathcal{R}_{\Gamma,i}: X_{\Gamma} \to H_{\partial \Omega \setminus \Gamma}
(\text{curl}; \Omega_i)$ a linear continuous extension operator acting
from the interface to subdomain $i$. Then we calculate,
\begin{align}
  \langle S\lambda, \mu \rangle &= -\sum_{i=1}^2 \int_\Gamma
    (\n^i \times \curl E_i^\lambda) \cdot \mu \\
    &= -\sum_{i=1}^2 \int_\Gamma (\n^i \times \curl E_i^\lambda)
      \cdot \mathcal{R}_{\Gamma,i}\mu\\
    &= -\sum_{i=1}^2 \int_{\partial \Omega_i} (\n^i \times \curl E_i^\lambda)
      \cdot \mathcal{R}_{\Gamma,i}\mu\\
    &= -\sum_{i=1}^2 \int_{\Omega_i} -\curl E_i^\lambda \cdot \curl
      \mathcal{R}_{\Gamma,i} \mu\\
    &= \sum_{i=1}^2 a_i(E_i^\lambda, \mathcal{R}_{\Gamma,i}\mu).
\end{align}
Similarly,
\begin{align}
  \langle \chi, \mu \rangle &= \sum_{i=1}^2 \int_\Gamma (\n^i\times \curl E_i^f)
    \cdot \mathcal{R}_{\Gamma,i} \mu \\
    &=\sum_{i=1}^2 \int_\Gamma \curl E_i^f\times \n^i)
      \cdot \mathcal{R}_{\Gamma,i} \mu \\
    &=-\sum_{i=1}^2 \left(\int_{\Omega,i} \curl E_i^f
      \cdot \curl \mathcal{R}_{\Gamma,i} \mu -
      \int_{\Omega,i} \curl \curl E_i^f \cdot \mathcal{R}_{\Gamma,i} \mu\right)\\
    &=\sum_{i=1}^2 \left[(f^e,\mathcal{R}_{\Gamma,i}\mu)_i
      - a_i(E_i^f,\mathcal{R}_{\Gamma,i}\mu)\right].
\end{align}
Then we have the weak form of the problem,
\begin{align}
  \label{eq:spweak}
  \text{find } \lambda\in X_\Gamma \, : \,
    \langle S\lambda, \mu \rangle = \langle \chi, \mu \rangle
    \quad \forall \mu\in X_\Gamma.
\end{align}

To prove that the algorithm converges we will apply the following lemma from
\myplan{ cite QV dd book }.
\begin{lemma}
  \label{lem:qv}
  Let $X$ be a Hilbert space and $\mathcal{A}:X\to X'$ be an operator.
  Suppose $\mathcal{A}$ can be split into $\mathcal{A} = \mathcal{A}_1 + \mathcal{A}_2$,
  and that $\mathcal{A}_i$ is continuous and coercive with continuity constant
  $C_i$ and coercivity constant $\alpha_i$, $i=1,2$. Define $\mathcal{N}
  = (\mathcal{A}_1^{-1} + \mathcal{A}_2^{-1})^{-1}$. Note that $\mathcal{N}$
  is continuous and coercive since $\mathcal{A}_i$ is continuous and coercive. Let
  $\alpha_\mathcal{N}$ denote the coercivity constant for $\mathcal{N}$ and
  $C_\mathcal{N}$ denote the continuity constant for $\mathcal{N}$. Further
  assume $\mathcal{N}$ satisfies the condition that there exists
  $k_\ast > 0$ s.t.
  \begin{align}
    \label{eq:symcoer}
    \langle \mathcal{N}\lambda, \mathcal{N}^{-1}\mathcal{A}\lambda \rangle
    + \langle \mathcal{A}\lambda, \lambda \rangle \geq
    k_\ast \|\lambda\|_{X}^2, \quad \forall \lambda\in X.
  \end{align}
  Then there exists $\theta_{max} > 0$ given by,
  \begin{align}
    \theta_{max} = \frac{k_\ast \alpha_\mathcal{N}}{C_\mathcal{N} \left(\sum_{i=1}^2
    \alpha_i^{-1}\right)\left(\sum_{i=1}^2 C_i\right)^2}
  \end{align}
  such that for any $0 < \theta < \theta_{max}$ the operator
  \begin{align}
    T_\theta = I - \theta \mathcal{N}^{-1} \mathcal{A}
  \end{align}
  is a contraction on the space $X$. If $\mathcal{A}_i$ is symmetric
  for $i=1,2$ then \eqref{eq:symcoer} is equivalent to the coercivity
  of the operator $\mathcal{A}$.
\end{lemma}

\begin{proposition}
  There exists a unique $\lambda \in X_\Gamma$ such that
  \begin{align}
    \langle S\lambda, \mu \rangle = \langle \chi, \mu \rangle \quad \forall
      \mu\in X_\Gamma.
  \end{align}
\end{proposition}
\begin{proof}
  To show that there exists a unique solution to \eqref{eq:spweak} we will
  show that the operator $\langle S\cdot, \cdot\rangle$ is continuous
  and coercive on $X_\Gamma$. Thus the Lax-Milgram theorem will gaurantee
  the existence of a unique solution.
  \begin{align}
    \left|\langle S\lambda, \mu \rangle\right| &=
      \left| \sum_{i=1}^2 a_i(E_i^\lambda, E_i^\mu) \right|\\
      \label{eq:holder}
      &\leq \sum_{i=1}^2 \|\curl E_i^\lambda\|_{0,\Omega_i}
        \|\curl E_i^\mu\|_{0,\Omega_i} \\
      \label{eq:normdef}
      &\leq \sum_{i=1}^2 \|E_i^\lambda\|_{\text{curl}, \Omega_i}
        \|E_i^\mu\|_{\text{curl}, \Omega_i},
    \end{align}
  where in \eqref{eq:holder} the H\"{o}lder inequality was applied,
  and in \eqref{eq:normdef} the definition of the norm on $H(\text{curl}; \Omega_i)$
  was applied.
  Then denoting $\beta_i$ the continuity constant for the map
  $\lambda \mapsto E^\lambda_i$, we have,
  \begin{align}
   \left|\langle S\lambda, \mu \rangle\right| &\leq
     (\beta_1^2 + \beta_2^2)\|\lambda\|_{X_\Gamma}\|\mu\|_{X_\Gamma}.
  \end{align}
  Thus $\langle S\cdot, \cdot \rangle$ is continuous.
  \begin{align}
    \langle S\lambda, \lambda \rangle &=
      \sum_{i=1}^2 \int_{\Omega_i} |\curl E_i^\lambda |^2 \\
      &= \sum_{i=1}^2 \|\curl E_i^\lambda \|^2_{0,\Omega_i} \\
      \label{eq:friedrichs}
      &\geq \sum_{i=1}^2 \eta^2_i \|E_i\lambda \|^2_{0,\Omega_i},\\
      \label{eq:tantrace}
      &\geq \left(\frac{1}{\tau_1^2} \eta^2_1
        + \frac{1}{\tau_2^2} \eta^2_2\right) \|\lambda\|^2_{X_\Gamma},
  \end{align}
  where in \eqref{eq:friedrichs} we have applied the Friedrichs Inequality
  and denoted by $\eta_i$ the appropriate constant in domain $i$, and
  in \eqref{eq:tantrace} we have recalled that the tangential trace
  is a bounded linear operator and denoted by $\tau_i$ its continuity
  constant in domain $i$.\\

  We have shown that the operator $\langle S\cdot, \cdot \rangle \in
  \mathcal{L}(X_\Gamma, X_\Gamma')$ is a coercive operator, and so the
  Lax-Milgram gaurantees the existence of a unique solution to \eqref{eq:spweak}.
\end{proof}

\begin{theorem}
  There exists $\theta_{max} > 0$ such that for any $0 < \theta < \theta_{max}$,
  and for any initial guess $\lambda^0$,
  algorithm nnMaxwell converges to the unique weak solution of the interface
  equation \eqref{eq:spweak}.
\end{theorem}
\begin{proof}
A proof of this theorem takes the following steps:
\begin{enumerate}
  \item We perform an operator splitting of $\langle S\cdot, \cdot \rangle$
    and show that algorithm nnMaxwell is equivalent to a preconditioned
    Richardson method for \eqref{eq:spweak}.
  \item We show that the splitting of $S$ satisfies the criteria
    from Lemma \ref{lem:qv}.
  \item We apply Lemma \ref{lem:qv} and the Banach Contraction Mapping Principle
    to obtain the desired result.
\end{enumerate}

We define the operator splitting $S = S_1 + S_2$ by
\begin{align}
  \langle S_1\lambda, \mu \rangle = a_1(E^\lambda_1, E^\mu_1), \\
  \langle S_2\lambda, \mu \rangle = a_2(E^\lambda_2, E^\mu_2).
\end{align}
As was pointed out in the section above, we notice that in the algorithm,
\begin{align}
  \Psi_1^{n+1} &= S_1^{-1}(S\lambda^n - \chi), \\
  \Psi_2^{n+1} &= -S_2^{-1}(S\lambda^n - \chi).
\end{align}
and so we have
\begin{align}
  \lambda^{n+1} = \lambda^n - \theta(S_1^{-1} + S_2^{-1}) (S\lambda^n - \chi),
\end{align}
which is a preconditioned Richardson scheme with the preconditioner
$\mathcal{N} = (S_1^{-1} + S_2^{-1})^{-1}$.\\

The calculations in the previous section for the coercivity and continuity
of $\langle S\cdot, \cdot\rangle$ can now be copied directly, with the removal
of the summation notation to show coercivity and continuity of the components
$\langle S_i\cdot, \cdot\rangle$. Since we have chosen $\mathcal{R}_{\Gamma,i}$
to be the map $\lambda \mapsto E_i^\lambda$, the components $S_i$ are
symmetric, and thus condition \eqref{eq:symcoer} is satisfied by
the coercivity of the operator $S$. Thus there exists a $\theta_{max} > 0$
such that for each $0 < \theta < \theta_{max}$ the operator
\begin{align}
  T_\theta = I - \theta \mathcal{N}^{-1}S
\end{align}
is a contraction on the Hilbert space $X_\Gamma$.\\

Then we notice that since $T_\theta$ is a contraction, $G_\theta$ defined by
\begin{align}
  G_\theta \lambda = \lambda - \theta \mathcal{N}^{-1}S\lambda
    + \theta\mathcal{N}^{-1}\chi
\end{align}
is also a contraction on $X_\Gamma$. Then the Banach Contraction Mapping Principle
states that $G_\theta$ has a unique fixed point $\lambda_\ast$
and that for any $\lambda^0$, $\lim_{n\to\infty} G_\theta^n \lambda^0 = \lambda_\ast$.
That is,
\begin{align}
  G_\theta \lambda_\ast &= \lambda_\ast,\\
  \lambda_\ast - \theta\mathcal{N}^{-1} S\lambda_\ast + \theta \mathcal{N}^{-1}\chi
    &= \lambda_\ast, \\
  \theta\mathcal{N}^{-1}(S\lambda - \chi) &= 0, \\
  S\lambda &= \chi.
\end{align}

\end{proof}

\end{document}
